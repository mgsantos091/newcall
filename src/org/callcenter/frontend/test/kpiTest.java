package org.callcenter.frontend.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import net.miginfocom.swing.MigLayout;

public class kpiTest {
	public static void main(String args[]) {
		JPanel temEncarteRedePanel = new JPanel();
		JPanel compraForaRedePanel = new JPanel();
		JPanel contatoEfetivoValidoPanel = new JPanel();
		JPanel representanteAusentePanel = new JPanel();
		JPanel representanteVisitaPanel = new JPanel();
		JPanel contatosRealizadosPanel = new JPanel();
		JPanel motivoNaoCompraPanel = new JPanel();
		temEncarteRedePanel.setPreferredSize(new Dimension(50,50));
		compraForaRedePanel.setPreferredSize(new Dimension(50,50));
		contatoEfetivoValidoPanel.setPreferredSize(new Dimension(50,50));
		representanteAusentePanel.setPreferredSize(new Dimension(50,50));
		representanteVisitaPanel.setPreferredSize(new Dimension(50,50));
		motivoNaoCompraPanel.setBackground(Color.RED);
		motivoNaoCompraPanel.setPreferredSize(new Dimension(50,50));
		contatosRealizadosPanel.setBackground(Color.GRAY);
		contatosRealizadosPanel.setPreferredSize(new Dimension(50,50));
		
		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setBackground(Color.WHITE);
		panelPrincipal.setLayout(new MigLayout("", "[100px][100px][100px][100px][100px][100px]", "[][][]"));
		
		
		panelPrincipal.add(motivoNaoCompraPanel,"cell 0 0 3 1,alignx center");
		//		IndicadorContatosRealizadosPanel contatosRealizadosPanel = new IndicadorContatosRealizadosPanel();
		//		IndicadorMotivoNaoCompraPanel motivoNaoCompraPanel = new IndicadorMotivoNaoCompraPanel();
		//		IndicadorRepresentanteAusentePanel representanteAusentePanel = new IndicadorRepresentanteAusentePanel();
		//		IndicadorRepresentanteVisitaPanel representanteVisitaPanel = new IndicadorRepresentanteVisitaPanel();
		//		IndicadorTemEncarteRedePanel temEncarteRedePanel = new IndicadorTemEncarteRedePanel();
		//		IndicadorCompForaRedePanel compraForaRedePanel = new IndicadorCompForaRedePanel();
		//		IndicadorContatoEfetivoValidoPanel contatoEfetivoValidoPanel = new IndicadorContatoEfetivoValidoPanel();
				
				
				panelPrincipal.add(contatosRealizadosPanel, "cell 3 0 3 1,alignx center");
//		contatoEfetivoValidoPanel.setPreferredSize(new Dimension(50,50));
		panelPrincipal.add(contatoEfetivoValidoPanel, "cell 0 1 2 1,alignx center");
		panelPrincipal.add(temEncarteRedePanel, "cell 2 1 2 1,alignx center");
		panelPrincipal.add(compraForaRedePanel,"cell 4 1 2 1,alignx center");
//		representanteAusentePanel.setPreferredSize(new Dimension(50,50));
		panelPrincipal.add(representanteAusentePanel, "cell 0 2 3 1,alignx center");
		
		JScrollPane scrollPane = new JScrollPane(panelPrincipal,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//		representanteVisitaPanel.setPreferredSize(new Dimension(50,50));
		panelPrincipal.add(representanteVisitaPanel, "cell 3 2 3 1,alignx center");
		
		ApplicationFrame frame = new ApplicationFrame("TESTE");
		frame.setBackground(Color.white);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(scrollPane,BorderLayout.CENTER);
		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
		
	}
}
