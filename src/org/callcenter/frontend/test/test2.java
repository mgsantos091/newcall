package org.callcenter.frontend.test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.JLabel;

import java.awt.BorderLayout;
import javax.swing.SwingConstants;

public class test2 {
	public static void main(String args[]) {
		ApplicationFrame frame = new ApplicationFrame("TESTE");
		frame.setBackground(Color.white);
		
		frame.setPreferredSize(new Dimension(500,500));
//		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		
		JLabel lblNomePainel = new JLabel("NOME PAINEL");
		lblNomePainel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomePainel.setFont(new Font("Arial Black", Font.PLAIN, 12));
		lblNomePainel.setForeground(new Color(85,136,155));
		frame.getContentPane().add(lblNomePainel, BorderLayout.NORTH);
		frame.setVisible(true);
	}
}
