package org.callcenter.frontend.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import net.miginfocom.swing.MigLayout;

import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.Font;

public class teste {
	private static JTextField textField;
	public static void main(String args[]) {
		ApplicationFrame frame = new ApplicationFrame("TESTE");
		
		frame.setBackground(Color.white);
		
		frame.setPreferredSize(new Dimension(500,500));
//		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(Color.WHITE);
		leftPanel.setPreferredSize(new Dimension(300,0));
		frame.getContentPane().add(leftPanel, BorderLayout.WEST);
		leftPanel.setLayout(new MigLayout("", "[grow]", "[100px:n,grow][grow][200px,grow]"));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		leftPanel.add(panel_2, "cell 0 0,grow");
		panel_2.setLayout(new MigLayout("", "[grow]0[grow]", "[100px,bottom]0[100px]"));
		
		JLabel lblNewCallEsq = new JLabel("NEW");
		lblNewCallEsq.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewCallEsq.setFont(new Font("Arial Black", Font.BOLD, 22));
		lblNewCallEsq.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewCallEsq.setForeground(new Color(85,136,155));
		panel_2.add(lblNewCallEsq, "cell 0 0,alignx right,aligny bottom");
		
		JLabel lblNewCallDir = new JLabel("CALL");
		lblNewCallDir.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewCallDir.setFont(new Font("Arial Black", Font.BOLD, 22));
		lblNewCallDir.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewCallDir.setForeground(new Color(239,148,15));
		panel_2.add(lblNewCallDir, "cell 1 0,alignx left,aligny bottom");
		
		JLabel lblTelaInicial = new JLabel("TELA INICIAL");
		lblTelaInicial.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblTelaInicial.setForeground(new Color(85,136,155));
		panel_2.add(lblTelaInicial, "cell 0 1 2 1,alignx center,aligny top");
				
		JPanel panelBotoes = new JPanel();
		panelBotoes.setBackground(Color.WHITE);
		leftPanel.add(panelBotoes, "cell 0 1,grow");
		panelBotoes.setLayout(new MigLayout("", "[][grow][]", "[100px,fill][100px,fill][100px,fill][100px,fill]"));
		
		JButton btnFrontEnd = new JButton("FRONT END");
		btnFrontEnd.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnFrontEnd.setForeground(new Color(85,136,155));
		btnFrontEnd.setBorder(null);
		btnFrontEnd.setBackground(Color.WHITE);
		panelBotoes.add(btnFrontEnd, "cell 1 0,growx");
		
		JButton btnIndicadores = new JButton("INDICADORES DE PERFORMANCE");
		btnIndicadores.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnIndicadores.setForeground(new Color(85,136,155));
		btnIndicadores.setBorder(null);
		btnIndicadores.setBackground(Color.WHITE);
		panelBotoes.add(btnIndicadores, "cell 1 1,growx");
		
		JButton btnAgenda = new JButton("AGENDA");
		btnAgenda.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnAgenda.setForeground(new Color(85,136,155));
		btnAgenda.setBorder(null);
		btnAgenda.setBackground(Color.WHITE);
		panelBotoes.add(btnAgenda, "cell 1 2,growx");
		
		JButton btnRelatorios = new JButton("RELAT\u00D3RIOS");
		btnRelatorios.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnRelatorios.setForeground(new Color(85,136,155));
		btnRelatorios.setBorder(null);
		btnRelatorios.setBackground(Color.WHITE);
		panelBotoes.add(btnRelatorios, "cell 1 3,growx");
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		leftPanel.add(panel, "cell 0 2,grow");
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSair = new JButton("SAIR");
		btnSair.setBorder(null);
		btnSair.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnSair.setForeground(new Color(85,136,155));
		btnSair.setPreferredSize(new Dimension(60,35));
		btnSair.setBackground(Color.WHITE);
		panel_1.add(btnSair);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.WHITE);
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		JLabel lblInfoGeral = new JLabel("INFORMA\u00C7\u00D5ES GERAIS DO ATENDIMENTO DI\u00C1RIO");
		lblInfoGeral.setFont(new Font("Arial Black", Font.PLAIN, 14));
		centerPanel.add(lblInfoGeral, "cell 0 0,alignx center");
		
		JPanel panelInformacoes = new JPanel();
		panelInformacoes.setBackground(Color.WHITE);
		centerPanel.add(panelInformacoes, "cell 0 1,grow");
		panelInformacoes.setLayout(new MigLayout("", "[]", "[]"));
		frame.setVisible(true);
	}
}
