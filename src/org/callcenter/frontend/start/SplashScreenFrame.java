package org.callcenter.frontend.start;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.callcenter.frontend.swing.BackgroundPanel;
import org.openswing.swing.client.ProgressBarControl;

import net.miginfocom.swing.MigLayout;

public class SplashScreenFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private JTextPane processoIniciacaoArea = new JTextPane( );
	private JScrollPane processoScrollPane = new JScrollPane( );
	private ProgressBarControl barraDeProgresso = new ProgressBarControl();

	private SimpleAttributeSet keyWord = new SimpleAttributeSet();;
	
	public static void main(String args[]) {
		SplashScreenFrame frame = new SplashScreenFrame();
//		JFrame frame = new JFrame();
//		JTextPane processoIniciacaoArea = new JTextPane( );
		
		frame.setPreferredSize(new Dimension(600,550));	
		frame.setUndecorated(true);
		frame.setLayout(new MigLayout("","[fill]0[]0[]","[]0"));
	}
	
	public SplashScreenFrame() {
		
		super.setUndecorated(true);
		
		/*super.setPreferredSize(new Dimension(600,359));*/
		super.setPreferredSize(new Dimension(600,550));
		
//		BackgroundPanel bkgPanel = new BackgroundPanel(Toolkit.getDefaultToolkit().createImage(SplashScreenFrame.class.getResource("/images/splash_screen-1920_1080.jpg")));
//		BackgroundPanel bkgPanel = new BackgroundPanel(Toolkit.getDefaultToolkit().createImage(SplashScreenFrame.class.getResource("/images/azul-claro.png")));
//		BackgroundPanel bkgPanel = new BackgroundPanel(Toolkit.getDefaultToolkit().createImage(SplashScreenFrame.class.getResource("/images/newcall_rgb_1920x1080.jpg")));
//		BackgroundPanel bkgPanel = new BackgroundPanel(Toolkit.getDefaultToolkit().createImage(SplashScreenFrame.class.getResource("/images/splash3_800x450_RGB.jpg")));
		BackgroundPanel bkgPanel = new BackgroundPanel(Toolkit.getDefaultToolkit().createImage(SplashScreenFrame.class.getResource("/images/newcall_padrao_600x338.jpg")));
		
		super.setLayout(new MigLayout("","[fill]0[]0[]","[]0"));
		
//		super.setLayout( new BorderLayout( ) );
//		super.add(bkgPanel,BorderLayout.CENTER);
		super.add(bkgPanel,"dock center , wrap");

		/*super.add(lblStatus, BorderLayout.SOUTH);*/
//		processoIniciacaoArea.setEnabled(true);
		processoIniciacaoArea.setEditable(false);
		processoIniciacaoArea.setPreferredSize(new Dimension(0,191));
		StyleConstants.setForeground(keyWord, Color.BLACK);
//		StyleConstants.setBackground(keyWord, Color.RED);
//		StyleConstants.setItalic(keyWord, true);
//		StyleConstants.setForeground(keyWord, Color.GREEN);
		StyleConstants.setBold(keyWord, true);
		
//		processoIniciacaoArea.setCaretColor(Color.BLACK);
		/*processoScrollPane.getViewport().setBackground(Color.WHITE);*/
		processoScrollPane.setViewportView(processoIniciacaoArea);
//		processoScrollPane.setForeground(Color.BLACK);
		/*super.add(processoScrollPane, BorderLayout.SOUTH);*/
		super.add(barraDeProgresso,"dock south");
		super.add(processoScrollPane,"dock south");
//		super.add(barraDeProgresso,BorderLayout.SOUTH);
//		super.add(processoScrollPane, BorderLayout.SOUTH);
		this.addText("Carregando...");
		/* setSize(new Dimension(300, 150)); */
		/*super.setLayout(new FlowLayout());*/
		super.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		super.setResizable(false);

		/* setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); */
	
		super.pack();
		super.setLocationRelativeTo(null);
		super.repaint();
		super.setVisible(true);

	}

	public void addText( String text ) {
		/*this.lblStatus.setText(text);
		this.lblStatus.repaint();*/
		StyledDocument doc = processoIniciacaoArea.getStyledDocument();
		synchronized(this) {
//			doc.insertString(0, "Start of text\n", null );
		    try {
//		    	doc.insertString(0, "Start of text\n", null );
				doc.insertString(doc.getLength(), text + "\n", keyWord );
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			
//			this.processoIniciacaoArea. .append(text+"\n");
		/*this.processoIniciacaoArea.repaint();*/
			/*this.processoScrollPane.repaint();*/
//			this.processoScrollPane.revalidate();
			
			/*revalidate();*/
			
		}
		/*super.pack();*/
		/*super.setLocationRelativeTo(null);
		super.repaint();*/
	}
	
	public void setBarraDeProgressoVal( int v ) {
		barraDeProgresso.setValue(v);
	}

}