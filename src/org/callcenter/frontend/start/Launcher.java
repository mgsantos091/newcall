package org.callcenter.frontend.start;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.callcenter.frontend.controller.MenuPrincipalController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.license.ControleLicenca;

public class Launcher {

	public static void main(String[] args) {

		SplashScreenFrame frameSplash = new SplashScreenFrame();
		frameSplash.setBarraDeProgressoVal(0);

		frameSplash.addText("Inicializando o sistema de logs do NewcALL...");
		
		LogFileMngr logMngr = LogFileMngr.getInstance();
		logMngr.iniciar();
		
		frameSplash.addText("Logs iniciados...");
		
		frameSplash
			.addText("Verificando licen�a NewCall...");
		
		try{
			if(ControleLicenca.validarLicenca()) {
				frameSplash
					.addText("Licen�a v�lida, seja bem vindo");
			} else {
				frameSplash
					.addText("Licen�a inv�lida, encerrando...");
				JOptionPane.showMessageDialog(null, 
						"Licen�a inv�lida. Por favor entre em contato com o suporte.", 
						"Licen�a n�o registrada", 
						JOptionPane.ERROR_MESSAGE);
				logMngr.error("Licen�a deste computador n�o foi encontrada");
				System.exit(0);
			}
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, 
					"N�o foi poss�vel validar a licen�a. Por favor entre em contato com o suporte.", 
					"Falha na valida��o da licen�a", 
					JOptionPane.ERROR_MESSAGE);
			logMngr.error(e);
			System.exit(0);
		}
		
		frameSplash
				.addText("Verificando a exist�ncia de outras inst�ncias do aplicativo em execu��o...");
		
		try {
			FileLockMngt.tryAcquireLock();
			frameSplash.addText("Verifica��o conclu�da...");
		} catch (Exception e) {
			frameSplash.addText( e.getMessage( ) );
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		frameSplash.setBarraDeProgressoVal(10);

		if (FileLockMngt.getLock() == null) {
			frameSplash
					.addText("J� existe uma inst�ncia do aplicativo em execu��o...");
			try {
				Thread.sleep(5000);
				frameSplash.dispose();
				System.exit(0);
			} catch (InterruptedException e) {
//				PIVLogSettings.getInstance().error(e.getMessage(), e);
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		} else {
			frameSplash
					.addText("Lock adquirido com sucesso, o aplicativo esta sendo iniciado...");
			/*EventQueue.invokeLater(new Runnable() {
				public void run() {*/
					try {
						
//						NativeInterface.open();
						
						UIManager.setLookAndFeel(UIManager
								.getSystemLookAndFeelClassName());
						
						frameSplash.setBarraDeProgressoVal(20);
						
						// UIManager.setLookAndFeel(UIManager.getLookAndFeel());
						MenuPrincipalController menuPrincipalController = new MenuPrincipalController( frameSplash );
						
//						MDIFrame mdi = new MDIFrame(menuPrincipalController);
//						mdi.setIconImage(Toolkit.getDefaultToolkit().getImage(
//								MenuPrincipalController.class.getResource("/images/icone_newcall_150x150.jpg")));
						
//						frameSplash.setBarraDeProgressoVal(100);
						
//						frameSplash.dispose();
						
//						AgendaCFView calendar = new AgendaCFView();
//						MDIFrame.add(calendar);
						
					} catch (Exception e) {
						frameSplash.addText("Foi encontrado um erro: " + e.getMessage() + ", favor entrar em contato com o administrador do sistema");
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, e1.getMessage());
						}
						frameSplash.dispose();
						e.printStackTrace();
//						PIVLogSettings.getInstance().error(e.getMessage(), e);
						JOptionPane.showMessageDialog(null, e.getMessage());
					} finally {
//						NativeInterface.runEventPump();
					}
				/*}
			});*/
		}
	}
	
//	private static Boolean isExpirado( ) {
//		GregorianCalendar dataExpiracao = new GregorianCalendar();
//		dataExpiracao.set(Calendar.YEAR, 2015);
//		dataExpiracao.set(Calendar.MONTH, 6);
//		dataExpiracao.set(Calendar.DATE, 17);
//		try {
//			Calendar dataAtual = DateUtils.getAtomicTime();
//			if(dataAtual.before(dataExpiracao)) return false;
//			else {
//				JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Esta vers�o de demonstra��o do PlayIV expirou.\n"
//						+ "Favor entrar em contato para obter uma vers�o atualizada para continuar trabalhando.", "Software Expirado", JOptionPane.ERROR_MESSAGE);
//				return true;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Houve um problema ao recuperar dados via internet. Por favor, verifique sua conex�o com a internet.\n"
//					+ "Para utilizar esta vers�o de demonstra��o, � necess�rio uma conex�o ativa com a internet.");
//			return true;
//		}
//	}
	
	static class FileLockMngt {

		private static File f;
		private static FileChannel channel;
		private static FileLock lock;

		public static FileLock getLock() {
			return lock;
		}

		public static void tryAcquireLock( ) {

			try {
				f = new File("frontend.lock");
				// Check if the lock exist
				if (f.exists()) {
					// if exist try to delete it
					f.delete();
				}
				// Try to get the lock
				channel = new RandomAccessFile(f, "rw").getChannel();
				lock = channel.tryLock();
				if (lock == null) {
					// File is lock by other application
					channel.close();
					throw new RuntimeException("Apenas uma inst�ncia deste aplicativo pode ser executado.");
				}
				// Add shutdown hook to release lock when application shutdown
				ShutdownHook shutdownHook = new ShutdownHook();
				Runtime.getRuntime().addShutdownHook(shutdownHook);

				// Your application tasks here..
				System.out.println("Running");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} catch (IOException e) {
				throw new RuntimeException("N�o foi poss�vel iniciar o processo.", e);
			}

		}

		public static void unlockFile() {
			// release and delete file lock
			try {
				if (lock != null) {
					lock.release();
					channel.close();
					f.delete();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		static class ShutdownHook extends Thread {

			public void run() {
				unlockFile();
			}
		}

	}

}