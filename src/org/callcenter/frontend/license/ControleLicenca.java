package org.callcenter.frontend.license;

import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.callcenter.frontend.lib.Util;

public class ControleLicenca {
	
//	public static void main(String args[]) {
////		String chave = gerarHashCombinacaoLocal();
////		try {
////			Connection conn = DriverManager.getConnection("jdbc:mysql://newcall.igbgroup.com.br/newcalllicensa","mateus","z9@#hj35");
////			PreparedStatement stmt = conn.prepareStatement("insert into chaves values (?)");
////			stmt.setString(1, chave);
////			stmt.executeUpdate();
////		} catch (SQLException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
//		
//		String pwd = gerarCombinacaoLocal();
//		Boolean validado = false;
//		try {
//			Connection conn = DriverManager.getConnection("jdbc:mysql://newcall.igbgroup.com.br/newcalllicensa","mateus","z9@#hj35");
//			PreparedStatement stmt = conn.prepareStatement("select chave from chaves");
//			ResultSet rs = stmt.executeQuery();
//			while(rs.next()) {
//				String chave = rs.getString(1);
//				validado = PasswordHash.validatePassword(pwd, chave);
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InvalidKeySpecException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		System.out.println(validado);
//		
//	}
	
	private static String gerarCombinacaoLocal( ) {
		String hostName = "";
		String serialNumber = "";
		String osName = "";
		String osArch = "";
		try {
			hostName = Util.getHostName();
			serialNumber = Util.getSerialNumber();
			osName = Util.getOSName();
			osArch = Util.getOsArch();
//			System.out.println(hostName+"|"+serialNumber+"|"+osName+"|"+osArch);
			return hostName + serialNumber + osName + osArch;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String gerarHashCombinacaoLocal( ) {
		try {
			return PasswordHash.createHash(gerarCombinacaoLocal());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Boolean validarLicenca( ) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		String password = gerarCombinacaoLocal();
		
		Boolean validado = false;
		Connection conn = DriverManager.getConnection("jdbc:mysql://newcall.igbgroup.com.br/newcalllicensa","mateus","z9@#hj35");
		PreparedStatement stmt = conn.prepareStatement("select chave from chaves");
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			String chave = rs.getString(1);
			validado = PasswordHash.validatePassword(password, chave);
			if(validado) break;
		}
		stmt.close();
		conn.close();
		
		return validado;
	}
	
}