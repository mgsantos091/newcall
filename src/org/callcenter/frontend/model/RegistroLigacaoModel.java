package org.callcenter.frontend.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.callcenter.frontend.lib.Util;
import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.openswing.swing.util.client.ClientSettings;

@Entity
@Table(name="\"RegistroLigacao\"")
public class RegistroLigacaoModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private ClienteModel cliente;
	private UsuarioModel usuario;
	private String ddd;
	private String numero;
	private Timestamp inicio;
	private Timestamp fim;
	private Timestamp dataregistro = Util.getCurrentTime();
	private boolean ativo;
	private Timestamp duracao;
	private String anotacao;
	
	private boolean target_fora; // caso o telefone seja inv�lido, i.e. n�o foi encontrado
	
	private Integer status_ligacao_nao_atendida = 0;
	private Integer status_ligacao_atendida_nao_efetivado = 0;
	private Integer status_ligacao_atendida_efetivado = 0;
	private Integer indicou_compra;
//	private RepresentanteModel representante_ausente; // qnd o item 21 for escolhido, representante ausente
	
	private String status_ligacao;
	private String motivo_ligacao;
	
	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Id
//	@SequenceGenerator(name = "\"RegistroLigacao_id_seq\"", sequenceName = "\"RegistroLigacao_id_seq\"", allocationSize = 1, initialValue = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"RegistroLigacao_id_seq\"")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	@OneToOne(fetch = FetchType.LAZY)
	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public ClienteModel getCliente() {
		return this.cliente;
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}
	
	public Timestamp getInicio() {
		return inicio;
	}

	public Timestamp getFim() {
		return fim;
	}

	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}

	public void setFim(Timestamp fim) {
		this.fim = fim;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public Timestamp getDuracao() {
		if(this.inicio != null && this.fim != null) {
			this.duracao = new Timestamp( this.fim.getTime() - this.inicio.getTime() );
			return this.duracao;
		} else return null;
	}

	public void setDuracao(Timestamp duracao) {
		this.duracao = duracao;
	}
	
	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	public boolean isTarget_fora() {
		return target_fora;
	}

	public void setTarget_fora(boolean target_fora) {
		this.target_fora = target_fora;
	}

	public Integer getStatus_ligacao_nao_atendida() {
		return status_ligacao_nao_atendida;
	}

	public Integer getStatus_ligacao_atendida_nao_efetivado() {
		return status_ligacao_atendida_nao_efetivado;
	}

	public Integer getStatus_ligacao_atendida_efetivado() {
		return status_ligacao_atendida_efetivado;
	}

	public void setStatus_ligacao_nao_atendida(Integer status_ligacao_nao_atendida) {
		if(status_ligacao_nao_atendida==null) status_ligacao_nao_atendida = 0;
		this.status_ligacao_nao_atendida = status_ligacao_nao_atendida;
	}

	public void setStatus_ligacao_atendida_nao_efetivado(
			Integer status_ligacao_atendida_nao_efetivado) {
		if(status_ligacao_atendida_nao_efetivado==null) status_ligacao_atendida_nao_efetivado = 0;
		this.status_ligacao_atendida_nao_efetivado = status_ligacao_atendida_nao_efetivado;
	}

	public void setStatus_ligacao_atendida_efetivado(
			Integer status_ligacao_atendida_efetivado) {
		if(status_ligacao_atendida_efetivado==null) status_ligacao_atendida_efetivado = 0;
		this.status_ligacao_atendida_efetivado = status_ligacao_atendida_efetivado;
	}

//	@ManyToOne
//	@JoinColumn(name = "id_representante")
//	public RepresentanteModel getRepresentante_ausente() {
//		return representante_ausente;
//	}
//
//	public void setRepresentante_ausente(RepresentanteModel representante_ausente) {
//		this.representante_ausente = representante_ausente;
//	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getStatus_ligacao() {
//		if(this.status_ligacao==null||this.status_ligacao.equals("")) {
		String status = "";
		if(isTarget_fora()) status = "TARGET FORA";
		else if(getStatus_ligacao_nao_atendida()>0) status = "LIGA��O N�O ATENDIDA";
		else if(getStatus_ligacao_atendida_nao_efetivado()>0) status = "LIGA��O N�O EFETIVADO";
		else if(getStatus_ligacao_atendida_efetivado()>0) status = "LIGA��O EFETIVADO";
		this.status_ligacao = status;
//		}
		return this.status_ligacao;
	}

	public String getMotivo_ligacao() {
//		if(this.motivo_ligacao==null||this.motivo_ligacao.equals("")) {
		String motivo = "";
		if(getStatus_ligacao_nao_atendida()>0) motivo = ClientSettings.getInstance().getDomain("LIGACAO_NAO_ATENDIDA").getDomainPair(getStatus_ligacao_nao_atendida()).getDescription();
		else if(getStatus_ligacao_atendida_nao_efetivado()>0) motivo = ClientSettings.getInstance().getDomain("LIGACAO_ATENDIDA_NAO_EFETIVADO").getDomainPair(getStatus_ligacao_atendida_nao_efetivado()).getDescription();
		else if(getStatus_ligacao_atendida_efetivado()>0) motivo = ClientSettings.getInstance().getDomain("LIGACAO_ATENDIDA_EFETIVADO").getDomainPair(getStatus_ligacao_atendida_efetivado()).getDescription();
		this.motivo_ligacao = motivo;
//		}
		return this.motivo_ligacao;
	}

	public void setStatus_ligacao(String status_ligacao) {
		this.status_ligacao = status_ligacao;
	}

	public void setMotivo_ligacao(String motivo_ligacao) {
		this.motivo_ligacao = motivo_ligacao;
	}

	public Integer getIndicou_compra() {
		return indicou_compra;
	}

	public void setIndicou_compra(Integer indicou_compra) {
		this.indicou_compra = indicou_compra;
	}

//	@OneToOne(fetch = FetchType.LAZY)
	@OneToOne
	@JoinColumn(name = "idusuario", referencedColumnName = "id")
	public UsuarioModel getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}
	
}