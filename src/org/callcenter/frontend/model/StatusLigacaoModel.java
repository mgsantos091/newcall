package org.callcenter.frontend.model;

import java.io.Serializable;

import javax.persistence.*;

import org.callcenter.frontend.lib.Util;
import org.openswing.swing.message.receive.java.ValueObjectImpl;

import java.sql.Timestamp;

@Entity
@Table(name="\"StatusLigacao\"")
public class StatusLigacaoModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer tipo_status = 0;
	private Integer cod_status;
	private String status_desc;
	
	private Timestamp dataregistro = Util.getCurrentTime();

	public StatusLigacaoModel() {
    }

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	public Integer getTipo_status() {
		return tipo_status;
	}

	public Integer getCod_status() {
		return cod_status;
	}

	public String getStatus_desc() {
		return status_desc;
	}

	public void setTipo_status(Integer tipo_status) {
		this.tipo_status = tipo_status;
	}

	public void setCod_status(Integer cod_status) {
		this.cod_status = cod_status;
	}

	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}

}