package org.callcenter.frontend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name = "\"DadosSessao\"")
public class DadosSessaoUsuario extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private UsuarioModel usuario;
	
	private ClienteModel ultimoClienteLigacao;
	
	public DadosSessaoUsuario() {
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@OneToOne
	@JoinColumn(name = "idusuario", referencedColumnName = "id")
	public UsuarioModel getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public ClienteModel getUltimoClienteLigacao() {
		return ultimoClienteLigacao;
	}

	public void setUltimoClienteLigacao(ClienteModel ultimoClienteLigacao) {
		this.ultimoClienteLigacao = ultimoClienteLigacao;
	}

}