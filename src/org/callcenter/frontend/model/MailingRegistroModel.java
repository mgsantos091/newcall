package org.callcenter.frontend.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name = "\"MailingImportacao\"")
public class MailingRegistroModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private String nome_arquivo;
	private String descricao;
	private UsuarioModel operador_importador;
	private int num_linhas;
	private int num_inserts;
	private int num_updates;
	private int num_reg_invalidos;
	private Timestamp dataregistro;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	public Integer getId( ) {
		return id;
	}

	public String getNome_arquivo() {
		return nome_arquivo;
	}

	public String getDescricao() {
		return descricao;
	}

	@OneToOne
	@JoinColumn(name = "idoperador", referencedColumnName = "id")
	public UsuarioModel getOperador_importador() {
		return operador_importador;
	}

	public int getNum_linhas() {
		return num_linhas;
	}

	public int getNum_inserts() {
		return num_inserts;
	}

	public int getNum_updates() {
		return num_updates;
	}

	public Timestamp getDataregistro() {
		return dataregistro;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome_arquivo(String nome_arquivo) {
		this.nome_arquivo = nome_arquivo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setOperador_importador(UsuarioModel operador_importador) {
		this.operador_importador = operador_importador;
	}

	public void setNum_linhas(int num_linhas) {
		this.num_linhas = num_linhas;
	}

	public void setNum_inserts(int num_inserts) {
		this.num_inserts = num_inserts;
	}

	public void setNum_updates(int num_updates) {
		this.num_updates = num_updates;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	public int getNum_reg_invalidos() {
		return num_reg_invalidos;
	}

	public void setNum_reg_invalidos(int num_reg_invalidos) {
		this.num_reg_invalidos = num_reg_invalidos;
	}
	
}
