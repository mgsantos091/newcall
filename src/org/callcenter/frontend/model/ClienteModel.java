package org.callcenter.frontend.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.callcenter.frontend.lib.Util;
import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name = "\"Cliente\"")
public class ClienteModel extends ValueObjectImpl implements Serializable, Comparable<ClienteModel> {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private boolean ativo;
	private boolean pjuridica;
	private String cnpjcpf;
	private String inscricao_estadual;
	private Timestamp dataregistro = Util.getCurrentTime();
	private String endreferencia;
	private String endcomplemento;
	private Integer endnumero;
	private String nomefantasia;
	private String razaosocial;
	private String nome;
	private String website;
	private String email;
	private EnderecoModel endereco;
	private String ddd1;
	private String telefone1;
	private String ddd2;
	private String telefone2;
	private String ddd3;
	private String telefone3;
	private String observacoes;
	private UsuarioModel operador;
	
	private String tab_email;
//	private String tab_tel_contato;
	private String tab_tel_contato_ddd;
	private String tab_tel_contato_telefone;
	
	private MailingRegistroModel importacao;
	
	public ClienteModel() {
	}

	@Id
//	@SequenceGenerator(name = "\"Cliente_id_seq\"", sequenceName = "\"Cliente_id_seq\"", allocationSize = 1, initialValue = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Cliente_id_seq\"")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Column(length = 14)
	public String getCnpjcpf() {
		return this.cnpjcpf;
	}

	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = Util.getCurrentTime();
	}

	@Column(length = 255)
	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public Integer getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(Integer endnumero) {
		this.endnumero = endnumero;
	}

	@Column(length = 255)
	public String getNomefantasia() {
		return this.nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	@Column(length = 255)
	public String getRazaosocial() {
		return this.razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	@Column(length = 255)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	// uni-directional many-to-one association to Endereco
	@ManyToOne
	@JoinColumn(name = "cep")
	public EnderecoModel getEndereco() {
		return this.endereco;
	}

	public void setEndereco(EnderecoModel endereco) {
		this.endereco = endereco;
	}

/*	// bi-directional many-to-one association to Contato
	@OneToMany(mappedBy = "cliente")
	public List<PIVContactModel> getContatos() {
		return this.contatos;
	}*/

/*	public void setContatos(List<PIVContactModel> contatos) {
		this.contatos = contatos;
	}*/

	public boolean isPjuridica() {
		return pjuridica;
	}

	public void setPjuridica(boolean pjuridica) {
		this.pjuridica = pjuridica;
	}

	@Column(length = 255)
	public String getEndreferencia() {
		return endreferencia;
	}

	public void setEndreferencia(String endreferencia) {
		this.endreferencia = endreferencia;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ClienteModel) {
			ClienteModel cliente = (ClienteModel) obj;
			if(this.id == cliente.getId())
				return true;
		}
		return false;
	}

	@Column(length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	
	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getTab_email() {
		return tab_email;
	}

	public void setTab_email(String tab_email) {
		this.tab_email = tab_email;
	}

	public String getDdd1() {
		return ddd1;
	}

	public String getDdd2() {
		return ddd2;
	}

	public void setDdd1(String ddd1) {
		this.ddd1 = ddd1;
	}

	public void setDdd2(String ddd2) {
		this.ddd2 = ddd2;
	}

	public String getTab_tel_contato_ddd() {
		return tab_tel_contato_ddd;
	}

	public String getTab_tel_contato_telefone() {
		return tab_tel_contato_telefone;
	}

	public void setTab_tel_contato_ddd(String tab_tel_contato_ddd) {
		this.tab_tel_contato_ddd = tab_tel_contato_ddd;
	}

	public void setTab_tel_contato_telefone(String tab_tel_contato_telefone) {
		this.tab_tel_contato_telefone = tab_tel_contato_telefone;
	}

	public String getInscricao_estadual() {
		return inscricao_estadual;
	}

	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}

	@Override
	public int compareTo(ClienteModel o) {
		int res = o.getId().compareTo(id);
		return res;
//		return res == 0 ? res : 1;
	}
	
	@OneToOne
	@JoinColumn(name = "idoperador", referencedColumnName = "id")
	public UsuarioModel getOperador() {
		return operador;
	}

	public void setOperador(UsuarioModel operador) {
		this.operador = operador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone3() {
		return telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public String getDdd3() {
		return ddd3;
	}

	public void setDdd3(String ddd3) {
		this.ddd3 = ddd3;
	}

	@OneToOne
	@JoinColumn(name = "idimportacao", referencedColumnName = "id")
	public MailingRegistroModel getImportacao() {
		return importacao;
	}

	public void setImportacao(MailingRegistroModel importacao) {
		this.importacao = importacao;
	}
	
}