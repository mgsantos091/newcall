package org.callcenter.frontend.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class ClienteFrontEndTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private RegistroLigacaoModel registro;
	
	private ClienteModel cliente;
	
	private AgendaFrontEndTempModel agenda;
	
	private Timestamp data_registro;
		
	public Timestamp getData_registro() {
		if(registro!=null&&registro.getId()!=null) data_registro = registro.getDataregistro();
		else data_registro = null;
		return data_registro;
	}

	public void setData_registro(Timestamp data_registro) {
		this.data_registro = data_registro;
	}

	public ClienteFrontEndTempModel( ) {
		
	}

	public RegistroLigacaoModel getRegistro() {
		return registro;
	}

	public void setRegistro(RegistroLigacaoModel registro) {
		this.registro = registro;
	}

	public ClienteModel getCliente() {
		return cliente;
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public AgendaFrontEndTempModel getAgenda() {
		return agenda;
	}

	public void setAgenda(AgendaFrontEndTempModel agenda) {
		this.agenda = agenda;
	}

}