package org.callcenter.frontend.model;

import java.io.Serializable;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class TelaInicialDadosTabTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String tab_prod_estoque_sim;
	private String tab_compra_atualmente_sim;
	private String tab_resistencia_marca_sim;
	private String tab_compra_atacado_sim;
	private String tab_compra_rede_sim;
	private String tab_extra_prod_encarte_sim;
	private String tab_extra_fora_rede_sim;
	private String tab_compraria_novamente_sim;
	private String tab_compraria_call_sim;
	private String tab_visita_representante_sim;
		
	private String tab_prod_estoque_nao;
	private String tab_compra_atualmente_nao;
	private String tab_resistencia_marca_nao;
	private String tab_compra_atacado_nao;
	private String tab_compra_rede_nao;
	private String tab_extra_prod_encarte_nao;
	private String tab_extra_fora_rede_nao;
	private String tab_compraria_novamente_nao;
	private String tab_compraria_call_nao;
	private String tab_visita_representante_nao;
	
	public TelaInicialDadosTabTempModel( ) {
		
	}

	public String getTab_prod_estoque_sim() {
		return tab_prod_estoque_sim;
	}

	public String getTab_compra_atualmente_sim() {
		return tab_compra_atualmente_sim;
	}

	public String getTab_resistencia_marca_sim() {
		return tab_resistencia_marca_sim;
	}

	public String getTab_compra_atacado_sim() {
		return tab_compra_atacado_sim;
	}

	public String getTab_compra_rede_sim() {
		return tab_compra_rede_sim;
	}

	public String getTab_extra_prod_encarte_sim() {
		return tab_extra_prod_encarte_sim;
	}

	public String getTab_extra_fora_rede_sim() {
		return tab_extra_fora_rede_sim;
	}

	public String getTab_compraria_novamente_sim() {
		return tab_compraria_novamente_sim;
	}

	public String getTab_compraria_call_sim() {
		return tab_compraria_call_sim;
	}

	public String getTab_visita_representante_sim() {
		return tab_visita_representante_sim;
	}

	public String getTab_prod_estoque_nao() {
		return tab_prod_estoque_nao;
	}

	public String getTab_compra_atualmente_nao() {
		return tab_compra_atualmente_nao;
	}

	public String getTab_resistencia_marca_nao() {
		return tab_resistencia_marca_nao;
	}

	public String getTab_compra_atacado_nao() {
		return tab_compra_atacado_nao;
	}

	public String getTab_compra_rede_nao() {
		return tab_compra_rede_nao;
	}

	public String getTab_extra_prod_encarte_nao() {
		return tab_extra_prod_encarte_nao;
	}

	public String getTab_extra_fora_rede_nao() {
		return tab_extra_fora_rede_nao;
	}

	public String getTab_compraria_novamente_nao() {
		return tab_compraria_novamente_nao;
	}

	public String getTab_compraria_call_nao() {
		return tab_compraria_call_nao;
	}

	public String getTab_visita_representante_nao() {
		return tab_visita_representante_nao;
	}

	public void setTab_prod_estoque_sim(String tab_prod_estoque_sim) {
		this.tab_prod_estoque_sim = tab_prod_estoque_sim;
	}

	public void setTab_compra_atualmente_sim(String tab_compra_atualmente_sim) {
		this.tab_compra_atualmente_sim = tab_compra_atualmente_sim;
	}

	public void setTab_resistencia_marca_sim(String tab_resistencia_marca_sim) {
		this.tab_resistencia_marca_sim = tab_resistencia_marca_sim;
	}

	public void setTab_compra_atacado_sim(String tab_compra_atacado_sim) {
		this.tab_compra_atacado_sim = tab_compra_atacado_sim;
	}

	public void setTab_compra_rede_sim(String tab_compra_rede_sim) {
		this.tab_compra_rede_sim = tab_compra_rede_sim;
	}

	public void setTab_extra_prod_encarte_sim(String tab_extra_prod_encarte_sim) {
		this.tab_extra_prod_encarte_sim = tab_extra_prod_encarte_sim;
	}

	public void setTab_extra_fora_rede_sim(String tab_extra_fora_rede_sim) {
		this.tab_extra_fora_rede_sim = tab_extra_fora_rede_sim;
	}

	public void setTab_compraria_novamente_sim(String tab_compraria_novamente_sim) {
		this.tab_compraria_novamente_sim = tab_compraria_novamente_sim;
	}

	public void setTab_compraria_call_sim(String tab_compraria_call_sim) {
		this.tab_compraria_call_sim = tab_compraria_call_sim;
	}

	public void setTab_visita_representante_sim(String tab_visita_representante_sim) {
		this.tab_visita_representante_sim = tab_visita_representante_sim;
	}

	public void setTab_prod_estoque_nao(String tab_prod_estoque_nao) {
		this.tab_prod_estoque_nao = tab_prod_estoque_nao;
	}

	public void setTab_compra_atualmente_nao(String tab_compra_atualmente_nao) {
		this.tab_compra_atualmente_nao = tab_compra_atualmente_nao;
	}

	public void setTab_resistencia_marca_nao(String tab_resistencia_marca_nao) {
		this.tab_resistencia_marca_nao = tab_resistencia_marca_nao;
	}

	public void setTab_compra_atacado_nao(String tab_compra_atacado_nao) {
		this.tab_compra_atacado_nao = tab_compra_atacado_nao;
	}

	public void setTab_compra_rede_nao(String tab_compra_rede_nao) {
		this.tab_compra_rede_nao = tab_compra_rede_nao;
	}

	public void setTab_extra_prod_encarte_nao(String tab_extra_prod_encarte_nao) {
		this.tab_extra_prod_encarte_nao = tab_extra_prod_encarte_nao;
	}

	public void setTab_extra_fora_rede_nao(String tab_extra_fora_rede_nao) {
		this.tab_extra_fora_rede_nao = tab_extra_fora_rede_nao;
	}

	public void setTab_compraria_novamente_nao(String tab_compraria_novamente_nao) {
		this.tab_compraria_novamente_nao = tab_compraria_novamente_nao;
	}

	public void setTab_compraria_call_nao(String tab_compraria_call_nao) {
		this.tab_compraria_call_nao = tab_compraria_call_nao;
	}

	public void setTab_visita_representante_nao(String tab_visita_representante_nao) {
		this.tab_visita_representante_nao = tab_visita_representante_nao;
	}

}