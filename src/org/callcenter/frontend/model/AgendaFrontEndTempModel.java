package org.callcenter.frontend.model;

import java.io.Serializable;
import java.util.Date;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class AgendaFrontEndTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date startDate;
	private String titulo;
	private String comentario;
	
	public Date getStartDate() {
		return startDate;
	}
	public String getTitulo() {
		return titulo;
	}
	public String getComentario() {
		return comentario;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
		
}