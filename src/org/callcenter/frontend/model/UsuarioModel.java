package org.callcenter.frontend.model;

import java.io.Serializable;

import javax.persistence.*;

import org.callcenter.frontend.lib.Util;
import org.openswing.swing.message.receive.java.ValueObjectImpl;

import java.sql.Timestamp;

@Entity
@Table(name="\"Usuario\"")
public class UsuarioModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Timestamp dataregistro = Util.getCurrentTime();
	private String nome;
	
	private String senha;
	private String login;
	
	private String usuario_email;
	private String senha_email;
	
	private boolean usuariogerente;

	public UsuarioModel() {
    }

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}


	@Column(length=255)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	@Column(length=35)
	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}


	@Column(length=25)
	public String getlogin() {
		return this.login;
	}

	public void setlogin(String login) {
		this.login = login;
	}

	public String getUsuario_email() {
		return usuario_email;
	}

	public void setUsuario_email(String usuario_email) {
		this.usuario_email = usuario_email;
	}

	public String getSenha_email() {
		return senha_email;
	}

	public void setSenha_email(String senha_email) {
		this.senha_email = senha_email;
	}

	public boolean isUsuariogerente() {
		return usuariogerente;
	}

	public void setUsuariogerente(boolean usuariogerente) {
		this.usuariogerente = usuariogerente;
	}

}