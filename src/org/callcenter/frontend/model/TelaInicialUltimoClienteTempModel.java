package org.callcenter.frontend.model;

import java.io.Serializable;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class TelaInicialUltimoClienteTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String ultimo_cliente_id;
	private String ultimo_cliente_nome;
		
	public TelaInicialUltimoClienteTempModel( ) {
		
	}

	public String getUltimo_cliente_id() {
		return ultimo_cliente_id;
	}

	public void setUltimo_cliente_id(String ultimo_cliente_id) {
		this.ultimo_cliente_id = ultimo_cliente_id;
	}

	public String getUltimo_cliente_nome() {
		return ultimo_cliente_nome;
	}

	public void setUltimo_cliente_nome(String ultimo_cliente_nome) {
		this.ultimo_cliente_nome = ultimo_cliente_nome;
	}

}