package org.callcenter.frontend.model;

import java.io.Serializable;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class TelaInicialDadosLigacoesTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String total;
	private String efetivadas;
	private String nao_efetivadas;
	private String nao_atendida;
	private String target_fora;
	private String selecao_trabalho;
	private String ligacoes_restantes;
	
	public TelaInicialDadosLigacoesTempModel( ) {
		
	}

	public String getTotal() {
		return total;
	}

	public String getEfetivadas() {
		return efetivadas;
	}

	public String getNao_efetivadas() {
		return nao_efetivadas;
	}

	public String getNao_atendida() {
		return nao_atendida;
	}

	public String getTarget_fora() {
		return target_fora;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public void setEfetivadas(String efetivadas) {
		this.efetivadas = efetivadas;
	}

	public void setNao_efetivadas(String nao_efetivadas) {
		this.nao_efetivadas = nao_efetivadas;
	}

	public void setNao_atendida(String nao_atendida) {
		this.nao_atendida = nao_atendida;
	}

	public void setTarget_fora(String target_fora) {
		this.target_fora = target_fora;
	}

	public String getSelecao_trabalho() {
		return selecao_trabalho;
	}

	public String getLigacoes_restantes() {
		return ligacoes_restantes;
	}

	public void setSelecao_trabalho(String selecao_trabalho) {
		this.selecao_trabalho = selecao_trabalho;
	}

	public void setLigacoes_restantes(String ligacoes_restantes) {
		this.ligacoes_restantes = ligacoes_restantes;
	}

}