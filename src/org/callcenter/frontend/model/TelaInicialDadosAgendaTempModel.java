package org.callcenter.frontend.model;

import java.io.Serializable;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class TelaInicialDadosAgendaTempModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String qtde_agenda_dia;
		
	public TelaInicialDadosAgendaTempModel( ) {
		
	}

	public String getQtde_agenda_dia() {
		return qtde_agenda_dia;
	}

	public void setQtde_agenda_dia(String qtde_agenda_dia) {
		this.qtde_agenda_dia = qtde_agenda_dia;
	}


}