package org.callcenter.frontend.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.OpeLigacoesDFController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.panel.DadosLigacaoPanel;
import org.callcenter.frontend.view.panel.RelogioLigacaoPanel;
import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;

public class OpeLigacoesDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private DadosLigacaoPanel jpDadosLigacaoPanel;
	private RelogioLigacaoPanel jpRelogioPanel; 
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	private OpeLigacoesDFController clienteDFController;
	
	public OpeLigacoesDFView(OpeLigacoesDFController controller) {

		setTitle("Liga��es - Vis�o Detalhada");
		setFrameIcon(new ImageIcon(OpeLigacoesDFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
		
		this.clienteDFController = controller;

//		this.gridFrame = controller.getGridFrame();

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					OpeLigacoesDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);

		setPreferredSize(new Dimension(800, 600));
		getContentPane().setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);
		
		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);
		
		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		jpPrincipal
				.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill][grow,fill]"));
		jpPrincipal.setVOClassName(RegistroLigacaoModel.class.getCanonicalName());
		jpPrincipal.setFormController(clienteDFController);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		try {
			jpDadosLigacaoPanel = new DadosLigacaoPanel( );
		} catch (ParseException e) {
			e.printStackTrace();
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}

		jpPrincipal.add(jpDadosLigacaoPanel, "cell 0 0 2 1,growx");
		
		try {
			jpRelogioPanel = new RelogioLigacaoPanel( );
		} catch (ParseException e) {
			e.printStackTrace();
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}

		jpPrincipal.add(jpRelogioPanel, "cell 0 1 2 1,growx");
		
		getContentPane().add(jpPrincipal, "dock center , grow");

//		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

	public Form getForm( ){
		return this.jpPrincipal;
	}

	public RelogioLigacaoPanel getJpRelogioPanel() {
		return jpRelogioPanel;
	}

}