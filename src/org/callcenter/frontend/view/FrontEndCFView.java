package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.FrontEndClienteGFController;
import org.callcenter.frontend.controller.FrontEndLigacaoFormController;
import org.callcenter.frontend.controller.FrontEndLigacaoGFController;
import org.callcenter.frontend.controller.FrontEndTabulacaoFormController;
import org.callcenter.frontend.view.panel.FrontEndClienteGridPanel;
import org.callcenter.frontend.view.panel.FrontEndLigacaoFormPanel;
import org.callcenter.frontend.view.panel.FrontEndLigacoesGridPanel;
import org.callcenter.frontend.view.panel.FrontEndTabulacaoFormPanel;
import org.openswing.swing.mdi.client.InternalFrame;

public class FrontEndCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	public FrontEndCFView( ) {
		
		setTitle("FRONT END - PRINCIPAL");
		
		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					FrontEndCFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);
		setSize(new Dimension(800, 600));

		setLayout(new BorderLayout());
		
		FrontEndLigacaoFormController ligacaoFormController = new FrontEndLigacaoFormController();
		FrontEndLigacaoFormPanel ligacaoFormPanel = new FrontEndLigacaoFormPanel(ligacaoFormController);
		
		FrontEndLigacaoGFController ligacoesGridController = new FrontEndLigacaoGFController(ligacaoFormController);
		FrontEndLigacoesGridPanel ligacoesGridPanel = new FrontEndLigacoesGridPanel(ligacoesGridController);
		
		FrontEndTabulacaoFormController tabulacaoController = new FrontEndTabulacaoFormController();
		FrontEndTabulacaoFormPanel tabulacaoPanel = new FrontEndTabulacaoFormPanel(tabulacaoController);
		
		FrontEndClienteGFController clientesController = new FrontEndClienteGFController(ligacoesGridController, tabulacaoController);
		FrontEndClienteGridPanel clientesPanel = new FrontEndClienteGridPanel(clientesController);
		
		JPanel panelLeft = new JPanel();
		panelLeft.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][grow]"));
		panelLeft.add(clientesPanel,"wrap");
		panelLeft.add(tabulacaoPanel,"");
		
		JPanel panelRight = new JPanel();
		panelRight.setLayout(new MigLayout("", "[grow,fill]", "[300,fill][grow,fill]"));
		panelRight.add(ligacoesGridPanel,"wrap");
		panelRight.add(ligacaoFormPanel,"");
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				panelLeft, panelRight);
		
		add(splitPane,BorderLayout.CENTER);
		
//		setVisible(true);
		setUniqueInstance(true);
	}
	
}
