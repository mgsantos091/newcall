package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.callcenter.frontend.controller.OpeLigacoesDFController;
import org.callcenter.frontend.controller.OpeLigacoesGFController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.internationalization.java.Resources;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.table.columns.client.TimeColumn;

public class OpeLigacoesGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private ExportButton exportButton1 = new ExportButton();

	private IntegerColumn colId = new IntegerColumn();
	private ComboColumn colStatusLigacaoNaoAtendida = new ComboColumn();
	private ComboColumn colStatusLigacaoAtendidaNaoEfetivado = new ComboColumn();
	private ComboColumn colStatusLigacaoAtendidaEfetivado = new ComboColumn();
	private TimeColumn colDuracao = new TimeColumn(TimeZone.getTimeZone("GMT"));
	private TextColumn colNumero = new TextColumn();
	private TextColumn colAnotacao = new TextColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();

	private OpeLigacoesGFController controller;
	
	public OpeLigacoesGFView() {
		try {
			super.setTitle("Liga��es - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(
					OpeLigacoesGFView.class
							.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}
	
	public void setController(OpeLigacoesGFController controller) {
		this.controller = controller;
		grid.setController(controller);
		grid.setGridDataLocator(controller);	
		controller.setGrid(grid);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		// grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName(RegistroLigacaoModel.class
				.getCanonicalName());
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
				this));
		exportButton1.setText("exportButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(editButton, null);
		buttonsPanel.add(exportButton1, null);

		colId.setColumnName("id");
		// colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(35);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		colNumero.setColumnName("numero");
		// colNumero.setHeaderColumnName("Site");
		colNumero.setMaxCharacters(30);
		colNumero.setPreferredWidth(90);
		grid.getColumnContainer().add(colNumero);

		colDuracao.setColumnName("duracao");
		colDuracao.setPreferredWidth(120);
		colDuracao.setTimeFormat(Resources.HH_MM_SS);
		grid.getColumnContainer().add(colDuracao);

		colStatusLigacaoNaoAtendida
				.setColumnName("status_ligacao_nao_atendida");
		colStatusLigacaoNaoAtendida.setPreferredWidth(150);
		colStatusLigacaoNaoAtendida.setColumnSortable(true);
		colStatusLigacaoNaoAtendida.setColumnFilterable(true);
		colStatusLigacaoNaoAtendida.setDomainId("LIGACAO_NAO_ATENDIDA");
		grid.getColumnContainer().add(colStatusLigacaoNaoAtendida);

		colStatusLigacaoAtendidaNaoEfetivado
				.setColumnName("status_ligacao_atendida_nao_efetivado");
		colStatusLigacaoAtendidaNaoEfetivado.setPreferredWidth(150);
		colStatusLigacaoAtendidaNaoEfetivado.setColumnSortable(true);
		colStatusLigacaoAtendidaNaoEfetivado.setColumnFilterable(true);
		colStatusLigacaoAtendidaNaoEfetivado
				.setDomainId("LIGACAO_ATENDIDA_NAO_EFETIVADO");
		grid.getColumnContainer().add(colStatusLigacaoAtendidaNaoEfetivado);

		colStatusLigacaoAtendidaEfetivado
				.setColumnName("status_ligacao_atendida_efetivado");
		colStatusLigacaoAtendidaEfetivado.setPreferredWidth(150);
		colStatusLigacaoAtendidaEfetivado.setColumnSortable(true);
		colStatusLigacaoAtendidaEfetivado.setColumnFilterable(true);
		colStatusLigacaoAtendidaEfetivado
				.setDomainId("LIGACAO_ATENDIDA_EFETIVADO");
		grid.getColumnContainer().add(colStatusLigacaoAtendidaEfetivado);

		colAnotacao.setColumnName("anotacao");
		colAnotacao.setMaxCharacters(255);
		colAnotacao.setPreferredWidth(220);
		grid.getColumnContainer().add(colAnotacao);

		colDataRegistro.setColumnName("dataregistro");
		colDataRegistro.setPreferredWidth(100);
		grid.getColumnContainer().add(colDataRegistro);

		setSize(800, 600);

		setUniqueInstance(true);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		OpeLigacoesDFController cadClienteController = new OpeLigacoesDFController(grid, OpeLigacoesGFView.this.controller.getCliente());
		OpeLigacoesDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	void editButton_actionPerformed(ActionEvent e) {
		RegistroLigacaoModel registro = (RegistroLigacaoModel) grid.getVOListTableModel()
				.getObjectForRow(grid.getSelectedRow());
		OpeLigacoesDFController cadClienteController = new OpeLigacoesDFController(
				grid, registro);
		OpeLigacoesDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		OpeLigacoesGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(OpeLigacoesGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

	class EmpGridFrame_editButton_actionAdapter implements
			java.awt.event.ActionListener {
		OpeLigacoesGFView adaptee;

		EmpGridFrame_editButton_actionAdapter(OpeLigacoesGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.editButton_actionPerformed(e);
		}
	}

}