package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import org.callcenter.frontend.controller.IndicadoresPerformanceCFController;
import org.callcenter.frontend.view.panel.IndicadorContatosRealizadosPanel;
import org.callcenter.frontend.view.panel.IndicadorMotivoNaoCompraPanel;
import org.openswing.swing.mdi.client.InternalFrame;

import net.miginfocom.swing.MigLayout;

public class IndicadoresPerformanceCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	public IndicadoresPerformanceCFView( ) {
		
		setTitle("NEWCALL - INDICADORES DE PERFORMANCE");
		
		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					IndicadoresPerformanceCFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);
		setSize(new Dimension(800, 600));
		
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);

		IndicadoresPerformanceCFController controller = new IndicadoresPerformanceCFController();
		
		IndicadorContatosRealizadosPanel contatosRealizadosPanel = controller.getContatosRealizadosPanel();
		IndicadorMotivoNaoCompraPanel motivoNaoCompraPanel = controller.getMotivoNaoCompraPanel();
		
		JPanel panelPrincipal = new JPanel(new MigLayout("", "[100px][100px][100px][100px][100px][100px]", "[][][]"));
		panelPrincipal.setBackground(Color.WHITE);
		
		panelPrincipal.add(contatosRealizadosPanel, "cell 3 0 3 1,alignx center");
		panelPrincipal.add(motivoNaoCompraPanel,"cell 0 0 3 1,alignx center");
		
		JScrollPane scrollPane = new JScrollPane(panelPrincipal,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		add(scrollPane, BorderLayout.CENTER);

//		setVisible(true);
		setUniqueInstance(true);
	}
	
}
