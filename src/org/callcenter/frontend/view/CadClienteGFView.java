package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.callcenter.frontend.controller.CadClienteDFController;
import org.callcenter.frontend.controller.CadClienteGFController;
import org.callcenter.frontend.controller.OpeLigacoesGFController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.model.ClienteModel;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GenericButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;

public class CadClienteGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private ExportButton exportButton1 = new ExportButton();
	private GenericButton operacaoLigacoesButton = new GenericButton();

	private final IntegerColumn colId = new IntegerColumn();

	private final TextColumn colNomeRed = new TextColumn();
	private final TextColumn colNomeComp = new TextColumn();
	private final TextColumn colSite = new TextColumn();
	private final TextColumn colEstado = new TextColumn();
	private final TextColumn colCidade = new TextColumn();

	public CadClienteGFView( ) {
		try {
			super.setTitle("Cadastro de Clientes - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(CadClienteGFView.class
					.getResource("/images/cadastros/cliente/agencia_16x16.png")));
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void setController(CadClienteGFController controller) {
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		controller.setGrid(grid);
	}
	
	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		// grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName(ClienteModel.class.getCanonicalName());
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
				this));
		operacaoLigacoesButton.setIcon(new ImageIcon(CadClienteGFView.class
					.getResource("/images/gerenciamento_relcomercial/tela-2/telefone_32x32.png")));
		operacaoLigacoesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClienteModel cliente = (ClienteModel) grid.getVOListTableModel()
						.getObjectForRow(grid.getSelectedRow());
				
				OpeLigacoesGFController opeLigacoesController = new OpeLigacoesGFController(cliente);
				OpeLigacoesGFView opeLigacoesView = new OpeLigacoesGFView();
				opeLigacoesView.setController(opeLigacoesController);
				MDIFrame.add(opeLigacoesView);
			}
		});
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(reloadButton);
		buttonsPanel.add(insertButton);
		buttonsPanel.add(editButton);
		buttonsPanel.add(exportButton1);
		buttonsPanel.add(operacaoLigacoesButton);

		colId.setColumnName("id");
		colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(25);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		colNomeRed.setColumnName("nomefantasia");
		colNomeRed.setMaxCharacters(160);
		colNomeRed.setPreferredWidth(150);
		colNomeRed.setColumnFilterable(true);
		colNomeRed.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeRed);

		colNomeComp.setColumnName("razaosocial");
		colNomeComp.setMaxCharacters(160);
		colNomeComp.setPreferredWidth(225);
		colNomeComp.setColumnFilterable(true);
		grid.getColumnContainer().add(colNomeComp);

		colSite.setColumnName("website");
		colSite.setMaxCharacters(80);
		colSite.setPreferredWidth(100);
		grid.getColumnContainer().add(colSite);

		colEstado.setColumnName("endereco.estado");
		colEstado.setMaxCharacters(80);
		colEstado.setPreferredWidth(45);
		grid.getColumnContainer().add(colEstado);

		colCidade.setColumnName("endereco.cidade");
		colCidade.setMaxCharacters(80);
		colCidade.setPreferredWidth(160);
		grid.getColumnContainer().add(colCidade);

		setSize(800, 600);
		
		setUniqueInstance(true);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		CadClienteDFController cadClienteController = new CadClienteDFController(
				grid);
		CadClienteDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	void editButton_actionPerformed(ActionEvent e) {
		ClienteModel cliente = (ClienteModel) grid.getVOListTableModel()
				.getObjectForRow(grid.getSelectedRow());
		CadClienteDFController cadClienteController = new CadClienteDFController(
				grid, cliente);
		CadClienteDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		CadClienteGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(CadClienteGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

	class EmpGridFrame_editButton_actionAdapter implements
			java.awt.event.ActionListener {
		CadClienteGFView adaptee;

		EmpGridFrame_editButton_actionAdapter(CadClienteGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.editButton_actionPerformed(e);
		}
	}

}