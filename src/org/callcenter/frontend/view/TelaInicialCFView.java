package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.swing.k5n.k5ncal.AgendaCFView;
import org.callcenter.frontend.view.panel.TelaInicialAgendaItem;
import org.callcenter.frontend.view.panel.TelaInicialDadosLigacoesItem;
import org.callcenter.frontend.view.panel.TelaInicialItemFormGenerico;
import org.callcenter.frontend.view.panel.TelaInicialUltimoClienteItem;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;

public class TelaInicialCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	private List<TelaInicialItemFormGenerico> itensForm = new ArrayList<TelaInicialItemFormGenerico>();
	
	public TelaInicialCFView( ) {
		setTitle("NEWCALL - FRONT END");
		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					TelaInicialCFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);
		setSize(new Dimension(800, 600));
		
		setBackground(Color.white);
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(Color.WHITE);
		leftPanel.setPreferredSize(new Dimension(300,0));
		getContentPane().add(leftPanel, BorderLayout.WEST);
		leftPanel.setLayout(new MigLayout("", "[grow]", "[100px:n,grow][grow][150px,grow]"));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		leftPanel.add(panel_2, "cell 0 0,grow");
		panel_2.setLayout(new MigLayout("", "[grow]0[grow]", "[100px,bottom]0[100px]"));
		
		JLabel lblNewCallEsq = new JLabel("NEW");
		lblNewCallEsq.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewCallEsq.setFont(new Font("Arial Black", Font.BOLD, 32));
		lblNewCallEsq.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewCallEsq.setForeground(new Color(85,136,155));
		panel_2.add(lblNewCallEsq, "cell 0 0,alignx right,aligny bottom");
		
		JLabel lblNewCallDir = new JLabel("CALL");
		lblNewCallDir.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewCallDir.setFont(new Font("Arial Black", Font.BOLD, 32));
		lblNewCallDir.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewCallDir.setForeground(new Color(239,148,15));
		panel_2.add(lblNewCallDir, "cell 1 0,alignx left,aligny bottom");
		
		JLabel lblTelaInicial = new JLabel("FRONT END");
		lblTelaInicial.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblTelaInicial.setForeground(new Color(85,136,155));
		panel_2.add(lblTelaInicial, "cell 0 1 2 1,alignx center,aligny top");
				
		JPanel panelBotoes = new JPanel();
		panelBotoes.setBackground(Color.WHITE);
		leftPanel.add(panelBotoes, "cell 0 1,grow");
		panelBotoes.setLayout(new MigLayout("", "[][grow][]", "[100px,fill]10[100px,fill]10[100px,fill]10[100px,fill]"));
		
		JButton btnFrontEnd = new JButton("FRONT END");
		btnFrontEnd.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnFrontEnd.setForeground(new Color(85,136,155));
		btnFrontEnd.setBorder(null);
		btnFrontEnd.setBackground(Color.WHITE);
		btnFrontEnd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					FrontEndCFView frontEndView = new FrontEndCFView();
					MDIFrame.add(frontEndView, true);
					LogFileMngr.getInstance().info("Tela Inicial - Front End inicalizado");
				} catch (Exception ex) {
					LogFileMngr.getInstance().error(ex.getMessage(), ex);
				}
			}
		});
		panelBotoes.add(btnFrontEnd, "cell 1 0,growx");
		
		JButton btnIndicadores = new JButton("INDICADORES DE PERFORMANCE");
		btnIndicadores.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnIndicadores.setForeground(new Color(85,136,155));
		btnIndicadores.setBorder(null);
		btnIndicadores.setBackground(Color.WHITE);
		btnIndicadores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					IndicadoresPerformanceCFView indicadoresPerformanceView = new IndicadoresPerformanceCFView();
					MDIFrame.add(indicadoresPerformanceView, true);
					LogFileMngr.getInstance().info("Tela Inicial - Indicadores de Performance inicializado");
				} catch (Exception ex) {
					LogFileMngr.getInstance().error(ex.getMessage(), ex);
				}
			}
		});
		panelBotoes.add(btnIndicadores, "cell 1 1,growx");
		
		JButton btnAgenda = new JButton("AGENDA");
		btnAgenda.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnAgenda.setForeground(new Color(85,136,155));
		btnAgenda.setBorder(null);
		btnAgenda.setBackground(Color.WHITE);
		btnAgenda.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					AgendaCFView calendarioView = new AgendaCFView();
					MDIFrame.add(calendarioView, true);
					LogFileMngr.getInstance().info("Tela Inicial - Calend�rio inicializado");
				} catch (Exception ex) {
					LogFileMngr.getInstance().error(ex.getMessage(), ex);
				}
			}
		});
		panelBotoes.add(btnAgenda, "cell 1 2,growx");
		
		JButton btnRelatorios = new JButton("RELAT\u00D3RIOS");
		btnRelatorios.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnRelatorios.setForeground(new Color(85,136,155));
		btnRelatorios.setBorder(null);
		btnRelatorios.setBackground(Color.WHITE);
		panelBotoes.add(btnRelatorios, "cell 1 3,growx");
		
		JButton btnAtualizar = new JButton("ATUALIZAR");
		btnAtualizar.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnAtualizar.setForeground(new Color(85,136,155));
		btnAtualizar.setBorder(null);
		btnAtualizar.setBackground(Color.WHITE);
		btnAtualizar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItens();
			}
		});
		panelBotoes.add(btnAtualizar, "cell 1 4,growx");
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		leftPanel.add(panel, "cell 0 2,grow");
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSair = new JButton("SAIR");
		btnSair.setBorder(null);
		btnSair.setFont(new Font("Arial Black", Font.PLAIN, 13));
		btnSair.setForeground(new Color(85,136,155));
		btnSair.setPreferredSize(new Dimension(60,35));
		btnSair.setBackground(Color.WHITE);
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					TelaInicialCFView.this.closeFrame();
					LogFileMngr.getInstance().info("Tela Inicial - sair");
				} catch (PropertyVetoException e1) {
				}
			}
		});
		panel_1.add(btnSair);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.WHITE);
		getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		JLabel lblInfoGeral = new JLabel("INFORMA\u00C7\u00D5ES GERAIS DO ATENDIMENTO DI\u00C1RIO");
		lblInfoGeral.setFont(new Font("Arial Black", Font.PLAIN, 14));
		centerPanel.add(lblInfoGeral, "cell 0 0,alignx center");
		
		JPanel panelInformacoes = new JPanel();
		panelInformacoes.setBackground(Color.WHITE);
		centerPanel.add(panelInformacoes, "cell 0 1,grow");
		panelInformacoes.setLayout(new MigLayout("", "", ""));
		
		TelaInicialDadosLigacoesItem dadosLigacaoItem = new TelaInicialDadosLigacoesItem();
		panelInformacoes.add(dadosLigacaoItem,"aligny top");
		itensForm.add(dadosLigacaoItem);
		LogFileMngr.getInstance().info("Tela Inicial - item dados de liga��es criado");
		
		TelaInicialAgendaItem agendaItem = new TelaInicialAgendaItem();
		panelInformacoes.add(agendaItem,"aligny top");
		itensForm.add(agendaItem);
		LogFileMngr.getInstance().info("Tela Inicial - item agenda criado");
		
		TelaInicialUltimoClienteItem ultimoClienteItem = new TelaInicialUltimoClienteItem();
		panelInformacoes.add(ultimoClienteItem,"aligny top,wrap");
		itensForm.add(ultimoClienteItem);
		LogFileMngr.getInstance().info("Tela Inicial - ultimo cliente criado");
		
		refreshItens();
		
//		setVisible(true);
		setUniqueInstance(true);
	}
	
	public synchronized void refreshItens( ) {
		LogFileMngr.getInstance().info("refresh iniciado");
		new Thread(new Runnable() {
			@Override
			public void run() {
				for(TelaInicialItemFormGenerico form : TelaInicialCFView.this.itensForm) {
					form.refresh();
					LogFileMngr.getInstance().info("Form " + form + " - refresh");
				}
			}
		}).start();
	}
}
