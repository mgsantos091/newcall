package org.callcenter.frontend.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.CadClienteDFController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.view.panel.EnderecoClientePanel;
import org.callcenter.frontend.view.panel.InformacoesClientePanel;
import org.callcenter.frontend.view.panel.TabulacaoClientePanel;
import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;

public class CadClienteDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private InformacoesClientePanel jpCliente_1;
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	private CadClienteDFController clienteDFController;
	
	private EnderecoClientePanel jpEndereco;

	private TabulacaoClientePanel jpTabCliente;

	public CadClienteDFView(CadClienteDFController controller) {

		setTitle("Cadastro de Clientes - Vis�o Detalhada");
		setFrameIcon(new ImageIcon(CadClienteDFView.class.getResource("/images/cadastros/cliente/agencia_16x16.png")));
		
		this.clienteDFController = controller;

//		this.gridFrame = controller.getGridFrame();

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					CadClienteDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);

		setPreferredSize(new Dimension(800, 600));
		getContentPane().setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);
		
		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);
		
		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		jpPrincipal
				.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill][grow,fill]"));
		jpPrincipal.setVOClassName(ClienteModel.class.getCanonicalName());
		jpPrincipal.setFormController(controller);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		jpCliente_1 = new InformacoesClientePanel(this.clienteDFController);
		if(controller.getCliente()!=null) jpCliente_1.setPJuridica(controller.getCliente().isPjuridica());
		jpPrincipal.add(jpCliente_1, "");
		
		jpEndereco = new EnderecoClientePanel();
		jpPrincipal.add(jpEndereco, "wrap");
		
		jpTabCliente = new TabulacaoClientePanel();
		jpPrincipal.add(jpTabCliente, "spanx");
		
		getContentPane().add(jpPrincipal, "dock center , grow");

		setSize(800, 600);
		
//		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public InformacoesClientePanel getClientePanel() {
		return this.jpCliente_1;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

	public Form getForm( ){
		return this.jpPrincipal;
	}

}