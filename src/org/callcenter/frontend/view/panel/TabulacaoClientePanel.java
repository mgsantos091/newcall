package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.text.ParseException;

import javax.swing.JPanel;

import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.swing.CadastroGenericoPanel;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.TextControl;

public class TabulacaoClientePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public TabulacaoClientePanel( ) {

		setLayout(new BorderLayout());
		
		CadastroGenericoPanel painel = new CadastroGenericoPanel("TABULA��O");
		add(painel,BorderLayout.CENTER);
		
		LabelControl lblTabResponsavelCompras = new LabelControl("Respons�vel por compras");
		painel.add(lblTabResponsavelCompras, "alignx right");
		
		TextControl txtResponsavelCompras = new TextControl();
		txtResponsavelCompras.setTrimText(true);
		txtResponsavelCompras.setAttributeName("tab_responsavel_compras");
		txtResponsavelCompras.setLinkLabel(lblTabResponsavelCompras);
		painel.add(txtResponsavelCompras, "span,grow");

		LabelControl lblEmail = new LabelControl("Email");
		painel.add(lblEmail, "");

		TextControl txtEmail = new TextControl();
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("tab_email");
		txtEmail.setLinkLabel(lblEmail);
		painel.add(txtEmail, "grow,span,wrap");
		
		LabelControl lblTelContato = new LabelControl("Outro tel. de contato:");
		painel.add(lblTelContato, "");
		
		LabelControl lblDdd = new LabelControl("DDD");
		painel.add(lblDdd, "alignx right");
		
		FormattedTextControl txtNumero;
		try {
			
		TextControl txtDdd = new TextControl(3);
		txtDdd.setAttributeName("tab_tel_contato_ddd");
		txtDdd.setMaxCharacters(3);
		txtDdd.setLinkLabel(lblDdd);
		painel.add(txtDdd, "");		
			
//		LabelControl lblNomeCliente = new LabelControl("Cliente (Nome Fantasia)");
//		painel.add(lblNomeCliente, "cell 2 0,alignx right");
//		
//		TextControl txtNomeCliente = new TextControl();
//		txtNomeCliente.setEnabledOnEdit(false);
//		txtNomeCliente.setEnabledOnInsert(false);
//		txtNomeCliente.setAttributeName("cliente.nomefantasia");
//		txtNomeCliente.setLinkLabel(lblNomeCliente);
//		painel.add(txtNomeCliente,"cell 3 0 3 1,growx");	
	
		LabelControl lblNumero = new LabelControl("N�mero");
		painel.add(lblNumero, "alignx right");
		
		txtNumero = TextControlFactory
				.criaFormattedTextControl(MaskResources.mask.TEL);
		txtNumero.setAttributeName("tab_tel_contato_telefone");
//		txtNumero.setRequired(true);
		txtNumero.setLinkLabel(lblNumero);
		//			lblTel.setLabelFor(txtNumero);
					painel.add(txtNumero,"");
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		
		

//		TextControl txtTelContato = new TextControl();
//		txtTelContato.setTrimText(true);
//		txtTelContato.setAttributeName("tab_tel_contato");
//		txtTelContato.setLinkLabel(lblTelContato);
//		form.add(txtTelContato, "cell 1 8 6 1,grow");

	}

}