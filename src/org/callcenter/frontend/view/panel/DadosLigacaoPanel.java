package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.text.ParseException;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.swing.CadastroGenericoPanel;
import org.openswing.swing.client.CheckBoxControl;
import org.openswing.swing.client.CodLookupControl;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;

public class DadosLigacaoPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public DadosLigacaoPanel( )
			throws ParseException {

		setLayout(new BorderLayout());
		
		CadastroGenericoPanel painel = new CadastroGenericoPanel("DADOS DA LIGA��O");
		painel.setCentroPainelLayout(new MigLayout("",
				"[right][][right]", ""));
		add(painel,BorderLayout.CENTER);

		LabelControl lblIdLigacao = new LabelControl("Codigo");
		painel.add(lblIdLigacao);

		NumericControl txtIdLigacao = new NumericControl();
		txtIdLigacao.setColumns(5);
		txtIdLigacao.setEnabledOnEdit(false);
		txtIdLigacao.setEnabledOnInsert(false);
		txtIdLigacao.setAttributeName("id");
		txtIdLigacao.setLinkLabel(lblIdLigacao);
//		lblIdEmp.setLabelFor(txtIdEmp);
		painel.add(txtIdLigacao,"wrap");
		
		LabelControl lblNomeCliente = new LabelControl("Cliente");
		painel.add(lblNomeCliente);
		
		TextControl txtNomeCliente = new TextControl();
		txtNomeCliente.setEnabledOnEdit(false);
		txtNomeCliente.setEnabledOnInsert(false);
		txtNomeCliente.setAttributeName("cliente.nomefantasia");
		txtNomeCliente.setLinkLabel(lblNomeCliente);
		painel.add(txtNomeCliente,"growx,wrap");		
	
		LabelControl lblNumero = new LabelControl("N�mero");
		painel.add(lblNumero);
		
		TextControl txtNumero = new TextControl();
		txtNumero.setAttributeName("numero");
		txtNumero.setRequired(true);
		txtNumero.setLinkLabel(lblNumero);
		painel.add(txtNumero,"");
		
		LabelControl lblTarget = new LabelControl("Fora de Target");
		painel.add(lblTarget);
		
		CheckBoxControl checkForaTarget = new CheckBoxControl();
		checkForaTarget.setAttributeName("target_fora");
		checkForaTarget.setLinkLabel(lblTarget);
		painel.add(checkForaTarget,"wrap");
				
//		LabelControl lbDuracao = new LabelControl("Dura��o");
//		painel.add(lbDuracao);
//		
//		DateControl txtDuracao = new DateControl(Consts.TYPE_TIME, Resources.DMY, '-', false, Resources.HH_MM_SS, TimeZone.getTimeZone("GMT"));
//		txtDuracao.setEnabledOnEdit(false);
//		txtDuracao.setEnabledOnInsert(false);
//		txtDuracao.setAttributeName("duracao");
//		txtDuracao.setLinkLabel(lblNumero);
//		painel.add(txtDuracao,"wrap");

		LabelControl lblStatusLigacaoNaoAtendida = new LabelControl("Status da Liga��o (n�o atendida)");
		painel.add(lblStatusLigacaoNaoAtendida,"");
		
		ComboBoxControl cboStatusLigacaoNaoAtendida = new ComboBoxControl();
		cboStatusLigacaoNaoAtendida.setAttributeName("status_ligacao_nao_atendida");
		cboStatusLigacaoNaoAtendida.setDomainId("LIGACAO_NAO_ATENDIDA");
		cboStatusLigacaoNaoAtendida.setRequired(false);
		cboStatusLigacaoNaoAtendida.setLinkLabel(lblStatusLigacaoNaoAtendida);
		painel.add(cboStatusLigacaoNaoAtendida,"growx,wrap");
		
		LabelControl lblStatusLigacaoAtendidaNaoEfetivado = new LabelControl("Status da Liga��o (n�o efetivado)");
		painel.add(lblStatusLigacaoAtendidaNaoEfetivado,"");
		
		ComboBoxControl cboStatusLigacaoAtendidaNaoEfetivado = new ComboBoxControl();
		cboStatusLigacaoAtendidaNaoEfetivado.setAttributeName("status_ligacao_atendida_nao_efetivado");
		cboStatusLigacaoAtendidaNaoEfetivado.setDomainId("LIGACAO_ATENDIDA_NAO_EFETIVADO");
		cboStatusLigacaoAtendidaNaoEfetivado.setRequired(false);
		cboStatusLigacaoAtendidaNaoEfetivado.setLinkLabel(lblStatusLigacaoAtendidaNaoEfetivado);
		painel.add(cboStatusLigacaoAtendidaNaoEfetivado,"growx,wrap");
		
		LabelControl lblStatusLigacaoAtendidaEfetivado = new LabelControl("Status da Liga��o (efetivado)");
		painel.add(lblStatusLigacaoAtendidaEfetivado,"");
		
		ComboBoxControl cboStatusLigacaoAtendidaEfetivado = new ComboBoxControl();
		cboStatusLigacaoAtendidaEfetivado.setAttributeName("status_ligacao_atendida_efetivado");
		cboStatusLigacaoAtendidaEfetivado.setDomainId("LIGACAO_ATENDIDA_EFETIVADO");
		cboStatusLigacaoAtendidaEfetivado.setRequired(false);
		cboStatusLigacaoAtendidaEfetivado.setLinkLabel(lblStatusLigacaoAtendidaEfetivado);
		painel.add(cboStatusLigacaoAtendidaEfetivado,"growx");
		
		LabelControl lblAnotacao = new LabelControl("Anota��o:");
		painel.add(lblAnotacao, "");

		TextAreaControl txtAnotacao = new TextAreaControl();
//		txtAnotacao.setMaxCharacters(10000);
		txtAnotacao.setAttributeName("anotacao");
		txtAnotacao.setRequired(false);
		txtAnotacao.setLinkLabel(lblAnotacao);

		painel.add(txtAnotacao, "h 250! , grow , span");
		
	}

}