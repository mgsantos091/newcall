package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.callcenter.frontend.controller.CadClienteDFController;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.swing.CadastroGenericoPanel;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;

public class InformacoesClientePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("rawtypes")
	private JComboBox cmbBoxCnpjCpf;

	private FormattedTextControl frmtdTxtCnpjCpf;

	public InformacoesClientePanel(CadClienteDFController controller) {

		setLayout(new BorderLayout());
		
		CadastroGenericoPanel painel = new CadastroGenericoPanel("EMPRESA");
		add(painel,BorderLayout.CENTER);
		
//		JLabel lblImgTopico = new JLabel();
//		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
//		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
//		lblImgTopico.setIcon(new ImageIcon(InformacoesClientePanel.class.getResource("/images/cadastros/cliente/agencia_64x64.png")));

		// lblImgTopico.setIcon(new ImageIcon(PIVClientePanel.class
		// .getResource("/resources/agency.png")));
//		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , left , span 2");

		LabelControl lblIdEmp = new LabelControl("Codigo");
		painel.add(lblIdEmp);

		NumericControl txtIdEmp = new NumericControl();
		txtIdEmp.setColumns(5);
		txtIdEmp.setEnabledOnEdit(false);
		txtIdEmp.setEnabledOnInsert(false);
		txtIdEmp.setAttributeName("id");
		txtIdEmp.setLinkLabel(lblIdEmp);
//		lblIdEmp.setLabelFor(txtIdEmp);
		painel.add(txtIdEmp);

		cmbBoxCnpjCpf = new JComboBox(new Object[] { "CNPJ", "CPF" });
		cmbBoxCnpjCpf.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
				try {
					if (event.getItem() == "CNPJ")
						TextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
								MaskResources.mask.CNPJ);
					else
						TextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
								MaskResources.mask.CPF);
				} catch (ParseException e) {
					e.printStackTrace();
					LogFileMngr.getInstance().error(e.getMessage(), e);
				}
			}
		});
		painel.add(cmbBoxCnpjCpf);

		frmtdTxtCnpjCpf = new FormattedTextControl();
//		frmtdTxtCnpjCpf.addValidFunction(new PIVValidTextContentFunc() {
//			@Override
//			public boolean validaConteudo(String text, Integer pk) {
//				
//				PIVClientModel cliente;
//
//				if (text.equals(""))
//					return false;
//
//				if(pk==null) pk = 0;
//				String sqlCode = "from org.playiv.com.mvc.model.PIVClientModel as Cliente where Cliente.cnpjcpf = '"
//						+ text + "' and Cliente.id <> " + pk;
//
//				Session session = getPdao().getSession();
//				cliente = (PIVClientModel) session.createQuery(sqlCode)
//						.uniqueResult();
//				session.clear();
//				// Cliente encontrado
//				if (cliente != null) {
//					// Cliente desativado
//					if (!cliente.isAtivo()) {
//						int option = JOptionPane
//								.showConfirmDialog(
//										null,
//										"Foi encontrado um cliente desativado"
//												+ " - "
//												+ (cliente.isPjuridica() ? " pessoa jur�dica "
//														: " pessoa f�sica ")
//												+ " - "
//												+ " com o mesmo "
//												+ (cliente.isPjuridica() ? "CNPJ"
//														: "CPF\n")
//												+ "Deseja ativa-lo e carregar seus dados?",
//										"Ativar cliente desativado",
//										JOptionPane.YES_NO_OPTION);
//						if (option == JOptionPane.YES_OPTION) {
//							cliente.setAtivo(true);
//							getPdao().update(cliente);
//							new PIVClientMngtDFController(null, cliente.getId());
//						}
//					} else {
//						int option = JOptionPane
//								.showConfirmDialog(
//										null,
//										"Foi encontrado um cliente"
//												+ " - "
//												+ (cliente.isPjuridica() ? " pessoa jur�dica "
//														: " pessoa f�sica ")
//												+ " - "
//												+ " com o mesmo "
//												+ (cliente.isPjuridica() ? "CNPJ"
//														: "CPF\n")
//												+ "Deseja carregar os dados do cliente?",
//										"Carregar dados de cliente encontrado",
//										JOptionPane.YES_NO_OPTION);
//						if (option == JOptionPane.YES_OPTION) {
//							new PIVClientMngtDFController(null, cliente.getId());
//						}
//					}
//					return false;
//				}
//				return true;
//			}
//		});
		try {
			TextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
					MaskResources.mask.CNPJ);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// frmtdTxtCnpjCpf =
		// PIVTextControlFactory.criaCpfCnpjTextControl(PIVMaskResources.mask.CNPJ);
		/*
		 * frmtdTxtCnpjCpf = PIVTextControlFactory
		 * .criaFormattedTextControl(PIVMaskResources.mask.CNPJ);
		 */
		// PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
		// PIVMaskResources.mask.CNPJ);
		frmtdTxtCnpjCpf.setAttributeName("cnpjcpf");
		painel.add(frmtdTxtCnpjCpf, "grow , wrap");
		
		LabelControl lblInscricao = new LabelControl("Inscri��o Estadual");
		painel.add(lblInscricao);

		TextControl txtInscricao = new TextControl();
		txtInscricao.setMaxCharacters(30);
		txtInscricao.setTrimText(true);
		txtInscricao.setAttributeName("inscricao_estadual");
		txtInscricao.setRequired(false);
		txtInscricao.setLinkLabel(lblInscricao);
//		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		painel.add(txtInscricao, "grow , span , wrap");
		
		LabelControl lblRazaoSocial = new LabelControl("Raz�o Social");
		painel.add(lblRazaoSocial);

		TextControl txtRazaoSocial = new TextControl();
		txtRazaoSocial.setMaxCharacters(120);
		txtRazaoSocial.setTrimText(true);
		txtRazaoSocial.setAttributeName("razaosocial");
		txtRazaoSocial.setRequired(true);
		txtRazaoSocial.setLinkLabel(lblRazaoSocial);
//		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		painel.add(txtRazaoSocial, "grow , span , wrap");

		LabelControl lblNomeFantasia = new LabelControl("Nome Fantasia");
		painel.add(lblNomeFantasia);

		TextControl txtNomeFantasia = new TextControl();
		txtNomeFantasia.setMaxCharacters(60);
		txtNomeFantasia.setTrimText(true);
		txtNomeFantasia.setAttributeName("nomefantasia");
		txtNomeFantasia.setRequired(true);
//		lblNomeFantasia.setLabelFor(txtNomeFantasia);
		txtNomeFantasia.setLinkLabel(lblNomeFantasia);
		painel.add(txtNomeFantasia, "grow , span , wrap");

		LabelControl lblWebsite = new LabelControl("Website");
		painel.add(lblWebsite);

		TextControl txtWebSite = new TextControl();
		txtWebSite.setMaxCharacters(120);
		txtWebSite.setTrimText(true);
		txtWebSite.setAttributeName("website");
//		lblWebsite.setLabelFor(txtWebSite);
		txtWebSite.setLinkLabel(lblWebsite);
		painel.add(txtWebSite, "grow");

		LabelControl lblEmail = new LabelControl("Email");
		painel.add(lblEmail);

		TextControl txtEmail = new TextControl();
		txtEmail.setMaxCharacters(120);
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("email");
//		lblWebsite.setLabelFor(txtWebSite);
		txtEmail.setLinkLabel(lblEmail);
		painel.add(txtEmail, "grow , wrap");
		
//		LabelControl lblLeadFonte = new LabelControl("Fonte do lead");
//		jpCentroPainel.add(lblLeadFonte,"");
//		
//		ComboBoxControl cboLeadFonte = new ComboBoxControl();
//		cboLeadFonte.setAttributeName("fonte");
//		cboLeadFonte.setDomainId("FONTE_LEAD");
//		cboLeadFonte.setRequired(true);
////		lblLeadFonte.setLabelFor(cboLeadFonte);
//		cboLeadFonte.setLinkLabel(lblLeadFonte);
//		jpCentroPainel.add(cboLeadFonte,"growx");
//		
//		LabelControl lblLeadAtividade = new LabelControl("Tipo de neg�cio");
//		jpCentroPainel.add(lblLeadAtividade,"");
//		
//		ComboBoxControl cboLeadNegocio = new ComboBoxControl();
//		cboLeadNegocio.setAttributeName("atividade");
//		cboLeadNegocio.setDomainId("ATIVIDADE_LEAD");
//		cboLeadNegocio.setRequired(true);
////		lblLeadAtividade.setLabelFor(cboLeadNegocio);
//		cboLeadNegocio.setLinkLabel(lblLeadAtividade);
//		jpCentroPainel.add(cboLeadNegocio,"growx,wrap");
		
		LabelControl lblDdd1 = new LabelControl("DDD 1");
		painel.add(lblDdd1);
		
		TextControl txtDdd1 = new TextControl(3);
		txtDdd1.setAttributeName("ddd1");
		txtDdd1.setMaxCharacters(3);
		txtDdd1.setLinkLabel(lblDdd1);
		painel.add(txtDdd1);
		
		LabelControl lblTel = new LabelControl("Telefone 1");
		painel.add(lblTel);

		FormattedTextControl txtTel;
		try {
			txtTel = TextControlFactory
					.criaFormattedTextControl(MaskResources.mask.TEL);
			txtTel.setAttributeName("telefone1");
			txtTel.setLinkLabel(lblTel);
//			lblTel.setLabelFor(txtTel);
			painel.add(txtTel, "");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		LabelControl lblDdd2 = new LabelControl("DDD 2");
		painel.add(lblDdd2);
		
		TextControl txtDdd2 = new TextControl(3);
		txtDdd2.setAttributeName("ddd2");
		txtDdd2.setMaxCharacters(3);
		txtDdd2.setLinkLabel(lblDdd2);
		painel.add(txtDdd2);
		
		LabelControl lblTel2 = new LabelControl("Telefone 2");
		painel.add(lblTel2);

		FormattedTextControl txtTel2;
		try {
			txtTel2 = TextControlFactory
					.criaFormattedTextControl(MaskResources.mask.TEL);
			txtTel2.setAttributeName("telefone2");
			txtTel2.setLinkLabel(lblTel2);
//			lblTel.setLabelFor(txtTel);
			painel.add(txtTel2, "wrap");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		LabelControl lblObs = new LabelControl("Observa��es:");
		painel.add(lblObs, "");

		TextAreaControl txtObs = new TextAreaControl();
		txtObs.setMaxCharacters(10000);
		txtObs.setAttributeName("observacoes");
		txtObs.setRequired(false);
		txtObs.setLinkLabel(lblObs);

		painel.add(txtObs, "grow,span");
		
	}

	public boolean isPJuridica() {
		return this.cmbBoxCnpjCpf.getSelectedItem() == "CNPJ";
	}

	public void setPJuridica(boolean isJuridica) {
		this.cmbBoxCnpjCpf.setSelectedItem(isJuridica ? "CNPJ" : "CPF");
	}

}