package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.lib.WebServiceCep;
import org.callcenter.frontend.model.EnderecoModel;
import org.callcenter.frontend.swing.CadastroGenericoPanel;
import org.hibernate.Session;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.mdi.client.MDIFrame;

public class EnderecoClientePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private FormattedTextControl frmtdTxtCep;
	private ComboBoxControl cboBoxPaises;
	private ComboBoxControl cboBoxEstados;
	private TextControl txtCidade;
	private TextControl txtBairro;
	private TextControl txtLogradouro;
	private NumericControl txtNumero;
	private TextControl txtComplemento;
	private TextControl txtReferencia;

	public EnderecoClientePanel ( ) {

//		setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		setLayout(new BorderLayout());
		
		CadastroGenericoPanel painel = new CadastroGenericoPanel("ENDERE�O");
		add(painel,BorderLayout.CENTER);

//		JPanel jpNomePainel = new JPanel();
//		jpNomePainel.setBackground(new Color(112, 128, 144));
//		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
//	
//		JLabel lblTituloPainel = new JLabel("ENDERECO");
//		lblTituloPainel.setForeground(Color.WHITE);
//		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
//		jpNomePainel.add(lblTituloPainel);
//		
//		super.add(jpNomePainel,"h 25! , dock north , growx");
//		
//		JPanel jpCentroPainel = new JPanel();
//		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
//		jpCentroPainel.setBackground(new Color(245, 245, 245));
//		jpCentroPainel.setLayout(new MigLayout("", "[right][left][right][grow][]", ""));
//
//		JLabel lblImgTopico = new JLabel();
//		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
//		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
//		lblImgTopico.setIcon(new ImageIcon(EnderecoClientePanel.class.getResource("/images/cadastros/endereco/endereco_64x64.png")));
//		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , spany 3 , right");

		LabelControl lblCep = new LabelControl("Cep");
		painel.add(lblCep,"");
		
		try {
			frmtdTxtCep = TextControlFactory.criaFormattedTextControl(MaskResources.mask.CEP);
			frmtdTxtCep.setAttributeName("endereco.cep");
//			lblCep.setLabelFor(frmtdTxtCep);
			frmtdTxtCep.setLinkLabel(lblCep);
			painel.add(frmtdTxtCep,"grow");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JButton btnAtualizaCep = new JButton();
		btnAtualizaCep.setIcon(new ImageIcon(EnderecoClientePanel.class.getResource("/images/cadastros/endereco/encontra-cep_32x32.png")));
		btnAtualizaCep.setBackground(new Color(245, 245, 245));
		btnAtualizaCep.setBorder(BorderFactory.createEmptyBorder());
		btnAtualizaCep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String cep = (String) frmtdTxtCep.getValue();
				if(cep.equals(""))
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Por Favor, preencha o cep antes.");
				else{
					String baseSQL = "from Endereco in class org.callcenter.frontend.model.EnderecoModel where Endereco.cep = '"
							+ cep + "'";
					Session session = PrincipalDAO.getInstance().getSession(); // obtain a JDBC connection and
															// instantiate a new Session
					EnderecoModel endereco = (EnderecoModel) session.createQuery(baseSQL)
							.uniqueResult();
					session.close();
					
					if(endereco==null) {
						JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o foi poss�vel encontrar este CEP na base de dados, uma nova tentativa de busca sera realizada via internet");
						WebServiceCep wscep = WebServiceCep.searchCep(cep);
						if (wscep.wasSuccessful()) {
							String estado = wscep.getUf();
							String cidade = wscep.getCidade();
							String bairro = wscep.getBairro();
							String logradouro = wscep.getLogradouroFull();
							atualizaEndereco(estado,cidade,bairro,logradouro);
							return;
						}
					}
					if(endereco!=null) {
						String estado = endereco.getEstado();
						String cidade = endereco.getCidade();
						String bairro = endereco.getBairro();
						String logradouro = endereco.getLogradouro();
						atualizaEndereco(estado,cidade,bairro,logradouro);
					} else
						JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o foi poss�vel encontrar este CEP, favor verificar o cep ou digitar os dados manualmente.");
				}
			}
		});
		painel.add(btnAtualizaCep,"wrap");
		
		LabelControl lblPais = new LabelControl("Pais");
		painel.add(lblPais,"");
		
		cboBoxPaises = new ComboBoxControl();
		cboBoxPaises.setAttributeName("endereco.pais");
		cboBoxPaises.setDomainId("PAIS");
		cboBoxPaises.setCanCopy(true);
		cboBoxPaises.setLinkLabel(lblPais);
		/*cboBoxPaises.setRequired(true);*/
		cboBoxPaises.setSelectedIndex(0);
		cboBoxPaises.setEnabled(false);
//		lblPais.setLabelFor(cboBoxPaises);
		cboBoxPaises.setLinkLabel(lblPais);
		painel.add(cboBoxPaises,"grow");
		
		LabelControl lblEstado = new LabelControl("Estado");
		painel.add(lblEstado,"");
		
		cboBoxEstados = new ComboBoxControl();
		cboBoxEstados.setAttributeName("endereco.estado");
		cboBoxEstados.setDomainId("ESTADO");
		cboBoxEstados.setCanCopy(true);
		cboBoxEstados.setLinkLabel(lblPais);
		/*cboBoxEstados.setRequired(true);*/
		cboBoxEstados.setSelectedIndex(0);
		cboBoxEstados.setEnabled(false);
//		lblEstado.setLabelFor(cboBoxEstados);
		cboBoxEstados.setLinkLabel(lblEstado);
		painel.add(cboBoxEstados,"grow");
		
		painel.add(new JPanel(),"wrap");
		
		LabelControl lblCidade = new LabelControl("Cidade");
		painel.add(lblCidade,"right , top");
		
		txtCidade = new TextControl();
		txtCidade.setMaxCharacters(120);
		txtCidade.setTrimText(true);
		txtCidade.setAttributeName("endereco.cidade");
		txtCidade.setEnabled(false);
//		lblCidade.setLabelFor(txtCidade);
		txtCidade.setLinkLabel(lblCidade);
		painel.add(txtCidade,"growx , top , span , wrap");
		
		LabelControl lblBairro = new LabelControl("Bairro");
		painel.add(lblBairro);
		
		txtBairro = new TextControl();
		txtBairro.setMaxCharacters(60);
		txtBairro.setTrimText(true);
		txtBairro.setAttributeName("endereco.bairro");
		txtBairro.setEnabled(false);
//		lblBairro.setLabelFor(txtBairro);
		txtBairro.setLinkLabel(lblBairro);
		painel.add(txtBairro,"grow , span 2 , wrap");
		
		LabelControl lblLogradouro = new LabelControl("Logradouro:");
		painel.add(lblLogradouro);
		
		txtLogradouro = new TextControl();
		txtLogradouro.setMaxCharacters(60);
		txtLogradouro.setTrimText(true);
		txtLogradouro.setAttributeName("endereco.logradouro");
		txtLogradouro.setEnabled(false);
//		lblLogradouro.setLabelFor(txtLogradouro);
		txtLogradouro.setLinkLabel(lblLogradouro);
		painel.add(txtLogradouro,"grow , span 3");
		
		LabelControl lblNumero = new LabelControl("Num");
		painel.add(lblNumero,"right");
		
		txtNumero = new NumericControl();
		txtNumero.setColumns(5);
		txtNumero.setMaxCharacters(6);
		txtNumero.setAttributeName("endnumero");
		txtNumero.setLinkLabel(lblNumero);
//		lblNumero.setLabelFor(txtNumero);
		painel.add(txtNumero,"align left , wrap");
		
		LabelControl lblComplemento = new LabelControl("Complemento");
		painel.add(lblComplemento);
		
		txtComplemento = new TextControl();
		txtComplemento.setMaxCharacters(60);
		txtComplemento.setTrimText(true);
		txtComplemento.setAttributeName("endcomplemento");
//		lblComplemento.setLabelFor(txtComplemento);
		txtComplemento.setLinkLabel(lblComplemento);
		painel.add(txtComplemento,"grow , span , wrap");
		
		LabelControl lblReferencia = new LabelControl("Refer�ncia");
		painel.add(lblReferencia);
		
		txtReferencia = new TextControl();
		txtReferencia.setMaxCharacters(60);
		txtReferencia.setTrimText(true);
		txtReferencia.setAttributeName("endreferencia");
//		lblReferencia.setLabelFor(txtReferencia);
		txtReferencia.setLinkLabel(lblReferencia);
		painel.add(txtReferencia,"grow , span ");

//		super.add(jpCentroPainel,"dock center , grow");
	
	}

	private void atualizaEndereco( String estado , String cidade , String bairro , String logradouro ) {
		try {
			this.cboBoxEstados.setValue(estado);
		} catch (Exception ignore) { }
		this.txtCidade.setText(cidade);
		this.txtBairro.setText(bairro);
		this.txtLogradouro.setText(logradouro);
	}

	public String getEstado() {
		return (String) this.cboBoxEstados.getValue();
	}
	
	public String getCidade( ) {
		return this.txtCidade.getText();
	}
	
	public String getBairro( ) {
		return this.txtBairro.getText();
	}
	
	public String getRua( ) {
		return this.txtLogradouro.getText();
	}

	public String getReferencia() {
		return txtReferencia.getText();
	}

	public String getComplemento() {
		return txtComplemento.getText();
	}

	public Integer getNumero() {
		return txtNumero.getValue() == null ? 0 :((java.math.BigDecimal) txtNumero.getValue()).intValue(); 
	}

	public String getCep() {
		return (String) frmtdTxtCep.getValue();
	}
	
	public String getPais( ) {
		return (String) this.cboBoxPaises.getValue(); 
	}

}