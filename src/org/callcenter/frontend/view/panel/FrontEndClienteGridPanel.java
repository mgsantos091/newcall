package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.callcenter.frontend.controller.CadClienteDFController;
import org.callcenter.frontend.controller.FrontEndClienteGFController;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.ClienteFrontEndTempModel;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.swing.k5n.k5ncal.EditEventWindow;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Calendar;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Repository;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Date;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Event;
import org.callcenter.frontend.view.CadClienteDFView;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GenericButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.send.java.FilterWhereClause;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;

import net.miginfocom.swing.MigLayout;

public class FrontEndClienteGridPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();
	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private ExportButton exportButton1 = new ExportButton();
	private GenericButton marcarAgenda = new GenericButton();
	private GenericButton filtrarAgendaAtual = new GenericButton();
	private GenericButton filtrarClienteSemLigacao = new GenericButton();
	private GenericButton filtrarClientesOperador = new GenericButton();
	private GenericButton buscarClienteSemOperador = new GenericButton();
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colRazaoSocial = new TextColumn();
	private TextColumn colNomeFantasia = new TextColumn();
	private TextColumn colDDD1 = new TextColumn();
	private FormattedTextColumn colTelefone1;
	private TextColumn colCep = new TextColumn();
	private TextColumn colEstado = new TextColumn();
	private TextColumn colCidade = new TextColumn();
	private TextColumn colLogradouro = new TextColumn();
	private TextColumn colBairro = new TextColumn();
	private IntegerColumn colNumero = new IntegerColumn();
	
	private DateTimeColumn colAgendaDtInicial = new DateTimeColumn();
	private TextColumn colAgendaTitulo = new TextColumn();
	private TextColumn colAgendaComentario = new TextColumn();
	
	private DateTimeColumn colDataMailing = new DateTimeColumn();
	private TextColumn colNomeMailing = new TextColumn();
	private TextColumn colDescMailing = new TextColumn();
	
	private DateTimeColumn colDataUltimaLigacao = new DateTimeColumn();
	private TextColumn statusUltimaLigacao = new TextColumn();
	private TextColumn motivoUltimaLigacao = new TextColumn();
	private TextColumn anotacaoUltimaLigacao = new TextColumn();

	private HashMap<String, FilterWhereClause[]> filtroAgendaMap = new HashMap<>();
	private Boolean temFiltroClienteSemLigacaoMap = false;
	private Boolean temFiltroOperadorMap = false;

	public FrontEndClienteGridPanel(final FrontEndClienteGFController controller) {

		grid.setFunctionId("GRID_FRONTEND_CLIENTE");
		
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		controller.setGrid(grid);
		TitledBorder title = BorderFactory
				.createTitledBorder("DADOS CLIENTES");
		title.setTitleFont(new Font("Arial Bold", Font.PLAIN, 18));
		setBorder(title);

		setLayout(new BorderLayout());

		JPanel painel = new JPanel();
		painel.setLayout(new MigLayout("", "[grow]", "[grow]"));
		add(painel, BorderLayout.CENTER);

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName(ClienteFrontEndTempModel.class
				.getCanonicalName());
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
				this));
		painel.add(grid, "span,grow");
		add(buttonsPanel, BorderLayout.NORTH);

//		marcarAgenda.setText("MARCAR EVENTO");
		marcarAgenda.setToolTipText("Clique para criar um evento na agenda.");
		marcarAgenda.setIcon(new ImageIcon(FrontEndClienteGridPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_mais_16x16.png")));
		marcarAgenda.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClienteFrontEndTempModel vo = (ClienteFrontEndTempModel) grid.getVOListTableModel()
						.getObjectForRow(grid.getSelectedRow());
				ClienteModel cliente = vo.getCliente();
				
				Repository repo = Util.getRepositorioPadrao();
				Calendar c = repo.getCalendars ().elementAt ( 0 );
				if(c==null) JOptionPane.showMessageDialog(MDIFrame.getInstance(), "N�o existem calend�rios criados, por favor inicie a Agenda e tenha certeza que\n"
						+ "exista ao menos um calend�rio criado.");
				Date now = Date.getCurrentDateTime ( "DTSTART" );
			    now.setMinute ( 0 );
			    new EditEventWindow ( repo, now, c, cliente );
			}
		});
		
//		filtrarAgendaAtual.setText("AGENDA HOJE");
		filtrarAgendaAtual.setToolTipText("Clique para visualizar os clientes com agendamento para hoje.");
		filtrarAgendaAtual.setIcon(new ImageIcon(FrontEndClienteGridPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_filtro_16x16.png")));
		filtrarAgendaAtual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Repository repo = Util.getRepositorioPadrao();
//				HashSet<ClienteModel> listaClientes = new HashSet<ClienteModel>();
				HashMap<String, FilterWhereClause[]> filtroMap = new HashMap<>();
				if(FrontEndClienteGridPanel.this.filtroAgendaMap.size()>0) {
					int dialogResult = JOptionPane.showConfirmDialog (null, "J� existe um filtro ativado, deseja remove-lo?","Filtro ativo",JOptionPane.YES_NO_OPTION);
					if(dialogResult == JOptionPane.YES_OPTION){
						FrontEndClienteGridPanel.this.filtroAgendaMap = new HashMap<>();
						controller.setAgendaFiltro(FrontEndClienteGridPanel.this.filtroAgendaMap);
						FrontEndClienteGridPanel.this.grid.reloadCurrentBlockOfData();
						return;
					}
				}
				ArrayList<Integer> ids = new ArrayList<Integer>();
				for (Event event : repo.getAllEntries()) {
					java.util.Calendar c1 = event.getStartDate().toCalendar();
					java.util.Calendar c2 = java.util.Calendar.getInstance();
					boolean mesmoDia = c1.get(java.util.Calendar.YEAR) == c2.get(java.util.Calendar.YEAR) &&
			                  c1.get(java.util.Calendar.DAY_OF_YEAR) == c2.get(java.util.Calendar.DAY_OF_YEAR);
					if(mesmoDia) { // mesmo dia
						String value = event.getNewCallData().getValue();
						Integer id;
						if(value.trim().equals("")) 
							continue;
						else
							id = Integer.parseInt(value);
						
						ClienteModel cliente = (ClienteModel) PrincipalDAO.getInstance().getObjectByID(ClienteModel.class, "id", id);
						if(cliente!=null) {
							ids.add(cliente.getId());
						}
					}
				}
				if(ids.size()>0) {
					FilterWhereClause fwc = new FilterWhereClause("cliente.id", "=", ids);
					FilterWhereClause[] fwcArray = new FilterWhereClause[2];
					fwcArray[0] = fwc;
					filtroMap.put("cliente.id", fwcArray);
					FrontEndClienteGridPanel.this.filtroAgendaMap = filtroMap;
					controller.setAgendaFiltro(FrontEndClienteGridPanel.this.filtroAgendaMap);
					FrontEndClienteGridPanel.this.grid.reloadCurrentBlockOfData();
				}
				
			}
		});
		
//		filtrarClienteSemLigacao.setText("SEM LIGAC�O");
		filtrarClienteSemLigacao.setToolTipText("Clique para visualizar os clientes sem nenhuma liga��o.");
		filtrarClienteSemLigacao.setIcon(new ImageIcon(FrontEndClienteGridPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cliente_sem_ligacao_16x16.png")));
		filtrarClienteSemLigacao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				Repository repo = Util.getRepositorioPadrao();
//				HashSet<ClienteModel> listaClientes = new HashSet<ClienteModel>();
//				HashMap<String, FilterWhereClause[]> filtroMap = new HashMap<>();
				if(FrontEndClienteGridPanel.this.temFiltroClienteSemLigacaoMap) {
					int dialogResult = JOptionPane.showConfirmDialog (null, "J� existe um filtro ativado, deseja remove-lo?","Filtro ativo",JOptionPane.YES_NO_OPTION);
					if(dialogResult == JOptionPane.YES_OPTION){
						FrontEndClienteGridPanel.this.temFiltroClienteSemLigacaoMap = false;
						controller.removeFiltroClienteSemLigacao();
						FrontEndClienteGridPanel.this.grid.reloadCurrentBlockOfData();
						return;
					}
				}

				FrontEndClienteGridPanel.this.temFiltroClienteSemLigacaoMap = true;
				controller.adicionaFiltroClienteSemLigacao();
				FrontEndClienteGridPanel.this.grid.reloadData();
				
			}
		});
		
		filtrarClientesOperador.setToolTipText("Clique para visualizar os seus clientes.");
		filtrarClientesOperador.setIcon(new ImageIcon(FrontEndClienteGridPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/meus_clientes_16x16.png")));
		filtrarClientesOperador.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				Repository repo = Util.getRepositorioPadrao();
//				HashSet<ClienteModel> listaClientes = new HashSet<ClienteModel>();
//				HashMap<String, FilterWhereClause[]> filtroMap = new HashMap<>();
				if(FrontEndClienteGridPanel.this.temFiltroOperadorMap) {
					int dialogResult = JOptionPane.showConfirmDialog (null, "J� existe um filtro ativado, deseja remove-lo?","Filtro ativo",JOptionPane.YES_NO_OPTION);
					if(dialogResult == JOptionPane.YES_OPTION){
						FrontEndClienteGridPanel.this.temFiltroOperadorMap = false;
						controller.removeFiltroClientesDoOperador();
						FrontEndClienteGridPanel.this.grid.reloadCurrentBlockOfData();
						return;
					}
				}

				FrontEndClienteGridPanel.this.temFiltroOperadorMap = true;
				controller.adicionaFiltroClientesDoOperador();
				FrontEndClienteGridPanel.this.grid.reloadData();
				
			}
		});
		
		buscarClienteSemOperador.setToolTipText("Clique para selecionar um cliente sem operador aleatoriamente.");
		buscarClienteSemOperador.setIcon(new ImageIcon(FrontEndClienteGridPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cliente_aleatorio.png")));
		buscarClienteSemOperador.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Session s = PrincipalDAO.getInstance().getSession();
				Criteria criteria = s.createCriteria(ClienteModel.class);
				criteria.add(Restrictions.isNull("operador.id"));
				criteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));
				criteria.setMaxResults(1);
				ClienteModel cliente = (ClienteModel) criteria.uniqueResult();
				if(cliente!=null) {
					controller.setTabulacoesCliente(cliente);
				} else
					JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Nenhum cliente encontrado");
				s.close();
			}
		});
		
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(editButton, null);
		buttonsPanel.add(exportButton1, null);
		buttonsPanel.add(marcarAgenda, null);
		buttonsPanel.add(filtrarAgendaAtual, null);
		buttonsPanel.add(filtrarClienteSemLigacao, null);
//		buttonsPanel.add(filtrarClientesOperador, null);
//		buttonsPanel.add(buscarClienteSemOperador, null);

		colId.setColumnName("cliente.id");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(35);
		colId.setColumnSortable(true);
		colId.setColumnFilterable(true);
		grid.getColumnContainer().add(colId);
		
		colRazaoSocial.setColumnName("cliente.razaosocial");
		colRazaoSocial.setMaxCharacters(160);
		colRazaoSocial.setPreferredWidth(225);
		colRazaoSocial.setColumnFilterable(true);
		colRazaoSocial.setColumnSortable(true);
		grid.getColumnContainer().add(colRazaoSocial);
		
		colNomeFantasia.setColumnName("cliente.nomefantasia");
		colNomeFantasia.setMaxCharacters(160);
		colNomeFantasia.setPreferredWidth(150);
		colNomeFantasia.setColumnFilterable(true);
		colNomeFantasia.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeFantasia);
		
		colDDD1.setColumnName("cliente.ddd1");
		colDDD1.setPreferredWidth(50);
		colDDD1.setColumnFilterable(true);
		colDDD1.setColumnSortable(true);
		grid.getColumnContainer().add(colDDD1);
		
		try {
			colTelefone1 = TextControlFactory.criaFormaattedTextColumn(MaskResources.mask.TEL);
			colTelefone1.setColumnName("cliente.telefone1");
			colTelefone1.setPreferredWidth(90);
			colTelefone1.setColumnFilterable(true);
			colTelefone1.setColumnSortable(true);
			grid.getColumnContainer().add(colTelefone1);
		} catch(Exception e) { }
		
		colCep.setColumnName("cliente.endereco.cep");
		colCep.setPreferredWidth(65);
		colCep.setColumnFilterable(true);
		colCep.setColumnSortable(true);
		grid.getColumnContainer().add(colCep);
		
		colEstado.setColumnName("cliente.endereco.estado");
		colEstado.setPreferredWidth(50);
		colEstado.setColumnFilterable(true);
		colEstado.setColumnSortable(true);
		grid.getColumnContainer().add(colEstado);
		
		colCidade.setColumnName("cliente.endereco.cidade");
		colCidade.setMaxCharacters(80);
		colCidade.setPreferredWidth(160);
		colCidade.setColumnFilterable(true);
		colCidade.setColumnSortable(true);
		grid.getColumnContainer().add(colCidade);
		
		colLogradouro.setColumnName("cliente.endereco.logradouro");
		colLogradouro.setPreferredWidth(70);
		colLogradouro.setColumnFilterable(true);
		colLogradouro.setColumnSortable(true);
		grid.getColumnContainer().add(colLogradouro);
		
		colBairro.setColumnName("cliente.endereco.bairro");
		colBairro.setPreferredWidth(70);
		colBairro.setColumnFilterable(true);
		colBairro.setColumnSortable(true);
		grid.getColumnContainer().add(colBairro);
		
		colNumero.setColumnName("cliente.endnumero");
		colNumero.setPreferredWidth(30);
		colNumero.setColumnFilterable(true);
		colNumero.setColumnSortable(true);
		grid.getColumnContainer().add(colNumero);

		colAgendaDtInicial.setColumnName("agenda.startDate");
		colAgendaDtInicial.setPreferredWidth(90);
		colAgendaDtInicial.setColumnFilterable(false);
		colAgendaDtInicial.setColumnSortable(false);
		grid.getColumnContainer().add(colAgendaDtInicial);
		
		colAgendaTitulo.setColumnName("agenda.titulo");
		colAgendaTitulo.setPreferredWidth(140);
		colAgendaTitulo.setColumnFilterable(false);
		colAgendaTitulo.setColumnSortable(false);
		grid.getColumnContainer().add(colAgendaTitulo);
		
		colAgendaComentario.setColumnName("agenda.comentario");
		colAgendaComentario.setPreferredWidth(200);
		colAgendaComentario.setColumnFilterable(false);
		colAgendaComentario.setColumnSortable(false);
		grid.getColumnContainer().add(colAgendaComentario);
		
		colNomeMailing.setColumnName("cliente.importacao.nome_arquivo");
		colNomeMailing.setPreferredWidth(150);
		colNomeMailing.setColumnFilterable(true);
		colNomeMailing.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeMailing);

		colDescMailing.setColumnName("cliente.importacao.descricao");
		colDescMailing.setPreferredWidth(200);
		colDescMailing.setColumnFilterable(true);
		colDescMailing.setColumnSortable(true);
		grid.getColumnContainer().add(colDescMailing);

		colDataMailing.setColumnName("cliente.importacao.dataregistro");
		colDataMailing.setPreferredWidth(90);
		colDataMailing.setColumnFilterable(true);
		colDataMailing.setColumnSortable(true);
		grid.getColumnContainer().add(colDataMailing);
		
//		colDataUltimaLigacao.setColumnName("data_ultima_ligacao");
		colDataUltimaLigacao.setColumnName("data_registro");
		colDataUltimaLigacao.setPreferredWidth(90);
		colDataUltimaLigacao.setColumnFilterable(true);
		colDataUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(colDataUltimaLigacao);

//		statusUltimaLigacao.setColumnName("status_ultima_ligacao");
		statusUltimaLigacao.setColumnName("registro.status_ligacao");
		statusUltimaLigacao.setPreferredWidth(200);
		statusUltimaLigacao.setColumnFilterable(true);
		statusUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(statusUltimaLigacao);
		
//		motivoUltimaLigacao.setColumnName("motivo_ultima_ligacao");
		motivoUltimaLigacao.setColumnName("registro.motivo_ligacao");
		motivoUltimaLigacao.setPreferredWidth(200);
		motivoUltimaLigacao.setColumnFilterable(true);
		motivoUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(motivoUltimaLigacao);
		
//		anotacaoUltimaLigacao.setColumnName("anotacao");
		anotacaoUltimaLigacao.setColumnName("registro.anotacao");
		anotacaoUltimaLigacao.setPreferredWidth(200);
		anotacaoUltimaLigacao.setColumnFilterable(true);
		anotacaoUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(anotacaoUltimaLigacao);
		
		grid.setShowPageNumber(false);
		grid.setLockedColumns(4);

	}

	void insertButton_actionPerformed(ActionEvent e) {
		CadClienteDFController cadClienteController = new CadClienteDFController( );
		CadClienteDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	void editButton_actionPerformed(ActionEvent e) {
		ClienteFrontEndTempModel vo = (ClienteFrontEndTempModel) grid.getVOListTableModel()
				.getObjectForRow(grid.getSelectedRow());

		Session session = PrincipalDAO.getInstance().getSession();
		ClienteModel cliente = (ClienteModel) session.createQuery("from org.callcenter.frontend.model.ClienteModel where id = '" + vo.getCliente().getId() + "'").uniqueResult();
		session.close();
		
		CadClienteDFController cadClienteController = new CadClienteDFController(cliente);
		CadClienteDFView cadClienteView = cadClienteController.getFrame();
		MDIFrame.add(cadClienteView);
	}
	
	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		FrontEndClienteGridPanel adaptee;

		EmpGridFrame_insertButton_actionAdapter(FrontEndClienteGridPanel adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

	class EmpGridFrame_editButton_actionAdapter implements
			java.awt.event.ActionListener {
		FrontEndClienteGridPanel adaptee;

		EmpGridFrame_editButton_actionAdapter(FrontEndClienteGridPanel adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.editButton_actionPerformed(e);
		}
	}

}