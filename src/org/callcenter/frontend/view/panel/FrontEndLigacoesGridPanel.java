package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.TimeZone;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.FrontEndLigacaoGFController;
import org.callcenter.frontend.lib.GridColumnFactory;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.internationalization.java.Resources;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.table.columns.client.TimeColumn;
import org.openswing.swing.util.java.Consts;

public class FrontEndLigacoesGridPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();
	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
//	private InsertButton insertButton = new InsertButton();
//	private EditButton editButton = new EditButton();
	private ExportButton exportButton1 = new ExportButton();

	private IntegerColumn colId = new IntegerColumn();
	private ComboColumn colStatusLigacaoNaoAtendida = new ComboColumn();
	private ComboColumn colStatusLigacaoAtendidaNaoEfetivado = new ComboColumn();
	private ComboColumn colStatusLigacaoAtendidaEfetivado = new ComboColumn();
	private TimeColumn colDuracao = new TimeColumn(TimeZone.getTimeZone("GMT"));
	private FormattedTextColumn colNumero;
	private TextColumn colAnotacao = new TextColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();

	private FrontEndLigacaoGFController controller;

	public FrontEndLigacoesGridPanel(FrontEndLigacaoGFController controller) {

		this.controller = controller;
		
		grid.setFunctionId("GRID_FRONTEND_LIGACAO");
		
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		this.controller.setGrid(grid);

		TitledBorder title = BorderFactory
				.createTitledBorder("DADOS LIGA��ES");
		title.setTitleFont(new Font("Arial Bold", Font.PLAIN, 18));
		setBorder(title);

		setLayout(new BorderLayout());

		JPanel painel = new JPanel();
		painel.setLayout(new MigLayout("", "[grow]", "[grow]"));
		add(painel, BorderLayout.CENTER);

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName(RegistroLigacaoModel.class
				.getCanonicalName());
//		insertButton
//				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
//						this));
//		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
//				this));
		painel.add(grid, "span,grow");
		add(buttonsPanel, BorderLayout.NORTH);

		buttonsPanel.add(reloadButton, null);
//		buttonsPanel.add(insertButton, null);
//		buttonsPanel.add(editButton, null);
		buttonsPanel.add(exportButton1, null);

		colId.setColumnName("id");
		// colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(35);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		try {
			colNumero = TextControlFactory.criaFormaattedTextColumn(MaskResources.mask.TEL);
			colNumero.setColumnName("numero");
			colNumero.setPreferredWidth(90);
			colNumero.setColumnFilterable(true);
			colNumero.setColumnSortable(true);
			grid.getColumnContainer().add(colNumero);
		} catch(Exception e) { }
		
		colDuracao.setColumnName("duracao");
		colDuracao.setPreferredWidth(120);
		colDuracao.setTimeFormat(Resources.HH_MM_SS);
		colDuracao.setColumnFilterable(true);
		colDuracao.setColumnSortable(true);
		grid.getColumnContainer().add(colDuracao);

		ComboColumn cbIndicouCompra = GridColumnFactory.getYesNoColumn("indicou_compra");
		grid.getColumnContainer().add(cbIndicouCompra);
		
		colStatusLigacaoNaoAtendida
				.setColumnName("status_ligacao_nao_atendida");
		colStatusLigacaoNaoAtendida.setPreferredWidth(150);
		colStatusLigacaoNaoAtendida.setColumnSortable(true);
		colStatusLigacaoNaoAtendida.setColumnFilterable(true);
		colStatusLigacaoNaoAtendida.setDomainId("LIGACAO_NAO_ATENDIDA");
		grid.getColumnContainer().add(colStatusLigacaoNaoAtendida);

		colStatusLigacaoAtendidaNaoEfetivado
				.setColumnName("status_ligacao_atendida_nao_efetivado");
		colStatusLigacaoAtendidaNaoEfetivado.setPreferredWidth(150);
		colStatusLigacaoAtendidaNaoEfetivado.setColumnSortable(true);
		colStatusLigacaoAtendidaNaoEfetivado.setColumnFilterable(true);
		colStatusLigacaoAtendidaNaoEfetivado
				.setDomainId("LIGACAO_ATENDIDA_NAO_EFETIVADO");
		grid.getColumnContainer().add(colStatusLigacaoAtendidaNaoEfetivado);

		colStatusLigacaoAtendidaEfetivado
				.setColumnName("status_ligacao_atendida_efetivado");
		colStatusLigacaoAtendidaEfetivado.setPreferredWidth(150);
		colStatusLigacaoAtendidaEfetivado.setColumnSortable(true);
		colStatusLigacaoAtendidaEfetivado.setColumnFilterable(true);
		colStatusLigacaoAtendidaEfetivado
				.setDomainId("LIGACAO_ATENDIDA_EFETIVADO");
		grid.getColumnContainer().add(colStatusLigacaoAtendidaEfetivado);

		colAnotacao.setColumnName("anotacao");
		colAnotacao.setMaxCharacters(255);
		colAnotacao.setPreferredWidth(220);
		grid.getColumnContainer().add(colAnotacao);

		colDataRegistro.setColumnName("dataregistro");
		colDataRegistro.setPreferredWidth(100);
		grid.getColumnContainer().add(colDataRegistro);
		
		grid.setShowPageNumber(false);

	}

	void insertButton_actionPerformed(ActionEvent e) {
		controller.setFormCliente();
		controller.setFormMode(Consts.INSERT);
//		OpeLigacoesDFController cadClienteController = new OpeLigacoesDFController(FrontEndLigacoesGridPanel.this.controller.getCliente());
//		OpeLigacoesDFView cadClienteView = cadClienteController.getFrame();
//		MDIFrame.add(cadClienteView);
	}

	void editButton_actionPerformed(ActionEvent e) {
		RegistroLigacaoModel registro = (RegistroLigacaoModel) grid.getVOListTableModel()
				.getObjectForRow(grid.getSelectedRow());
		controller.setFormRegistro(registro);
		controller.setFormMode(Consts.EDIT);
		controller.reloadForm();
//		OpeLigacoesDFController cadClienteController = new OpeLigacoesDFController(registro);
//		OpeLigacoesDFView cadClienteView = cadClienteController.getFrame();
//		MDIFrame.add(cadClienteView);
	}
	
//	class EmpGridFrame_insertButton_actionAdapter implements
//			java.awt.event.ActionListener {
//		FrontEndLigacoesGridPanel adaptee;
//
//		EmpGridFrame_insertButton_actionAdapter(FrontEndLigacoesGridPanel adaptee) {
//			this.adaptee = adaptee;
//		}
//
//		public void actionPerformed(ActionEvent e) {
//			adaptee.insertButton_actionPerformed(e);
//		}
//	}
//
//	class EmpGridFrame_editButton_actionAdapter implements
//			java.awt.event.ActionListener {
//		FrontEndLigacoesGridPanel adaptee;
//
//		EmpGridFrame_editButton_actionAdapter(FrontEndLigacoesGridPanel adaptee) {
//			this.adaptee = adaptee;
//		}
//
//		public void actionPerformed(ActionEvent e) {
//			adaptee.editButton_actionPerformed(e);
//		}
//	}
	
	public void reloadGrid( ) {
		this.grid.reloadData();
	}

	public GridControl getGrid() {
		return grid;
	}

}