package org.callcenter.frontend.view.panel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.model.TelaInicialDadosLigacoesTempModel;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.table.profiles.java.GridProfile;
import org.openswing.swing.util.server.HibernateUtils;

public class TelaInicialDadosLigacoesItem extends TelaInicialItemFormSQLGenerico {

	private static final long serialVersionUID = 1L;

	public TelaInicialDadosLigacoesItem( ) {
		addTextBox("total", "Total");
		addLinhaVazio();
		addTextBox("efetivadas", "Efetivados");
		addTextBox("nao_efetivadas", "N�o Efetivados");
		addTextBox("nao_atendida", "N�o Atendidos");
		addTextBox("target_fora", "Target Fora");
		addTextBox("selecao_trabalho", "Sele��o Trabalho");
		addTextBox("ligacoes_restantes", "Liga��es Restantes");
	}
	
	@Override
	public String getBaseSQL() {
		String baseSQL = "select registro as registro "
				+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro join fetch "
				+ "registro.cliente as cliente left join fetch "
				+ "cliente.endereco as endereco "
				+ "where registro.dataregistro = ( "
				  + "select max(dataregistro) "
				  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
				  + "where regmax.cliente = registro.cliente and regmax.ativo = true  "
				+ ") and cast(registro.dataregistro as date) = current_date and cliente.ativo = true ";
		return baseSQL;
	}

	@Override
	public String getNomeItem() {
		return "LIGA��ES";
	}

	@Override
	public Class getClassItem() {
		return TelaInicialDadosLigacoesTempModel.class;
	}

	@Override
	public Response processaDados(List list) {
		LogFileMngr.getInstance().info("Tela Inicial - item dados liga��es processaDados() inicializado");
		ArrayList<RegistroLigacaoModel> dadosLigacoes = (ArrayList<RegistroLigacaoModel>) list;
		
		Integer target_fora = 0;
		Integer naoAtendida = 0;
		Integer atendidaNaoEfetivado = 0;
		Integer atendidaEfetivado = 0;
		Integer total = 0;
		for(RegistroLigacaoModel reg : dadosLigacoes) {
			if(reg.isTarget_fora()) target_fora++;
			else if(reg.getStatus_ligacao_nao_atendida()>0) naoAtendida++;
			else if(reg.getStatus_ligacao_atendida_nao_efetivado()>0) atendidaNaoEfetivado++;
			else if(reg.getStatus_ligacao_atendida_efetivado()>0) atendidaEfetivado++;
			total ++;
		}
		
		Long ligacoesNaoRealizadas = 0l;
		Long ligacoesRealizadas = 0l;
		
		GridProfile profile = Util.getGridProfile("GRID_FRONTEND_CLIENTE");
		
		if(profile!=null&&profile.getQuickFilterValues().size()>0) {
					
			try {
				Session s = PrincipalDAO.getInstance().getSession();
				// liga��es feitas e restantes
				String baseSQL = "select case when registro.id is not null then 'sim' else 'nao' end , count(*) as total  "
						+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro right join fetch registro.cliente as cliente "
						+ "where ( registro.dataregistro = ( "
						  + "select max(dataregistro) "
						  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
						  + "where regmax.cliente = registro.cliente and regmax.ativo = true  "
						+ ") or registro.id is null ) and cliente.ativo = true "
						+ "group by case when registro.id is not null then 'sim' else 'nao' end";
				
				ArrayList<Object> values = new ArrayList<Object>();
				ArrayList<Type> types = new ArrayList<Type>();
				
				HashMap<String,String> mapeamentoDeCampos = new HashMap<String,String>();
				mapeamentoDeCampos.put("registro.cliente", "cliente");
				mapeamentoDeCampos.put("registro.cliente.endereco", "endereco");
				mapeamentoDeCampos.put("registro.cliente.representante", "representante");
				String baseSQLPlusFilter = HibernateUtils.applyFiltersAndSorter(
						mapeamentoDeCampos,
						profile.getQuickFilterValues(),
//						profile.getCurrentSortedColumns(),
						new ArrayList(),
//						profile.getCurrentSortedVersusColumns(),
						new ArrayList(),
				        RegistroLigacaoModel.class,
				        baseSQL,
				        values,
				        types,
				        null,
				        pdao.getSessionFactory()
				      );

//				List list = sess.createQuery(baseSQLPlusFilter).setParameters(values.toArray(),(Type[])types.toArray(new Type[types.size()])).list();

				List<Object[]> l = s.createQuery(baseSQLPlusFilter).setParameters(values.toArray(),(Type[])types.toArray(new Type[types.size()])).list();
				
				for(Object[] o : l) {
					if(o[0].equals("nao")) ligacoesNaoRealizadas = (Long) o[1];
					else if(o[0].equals("sim")) ligacoesRealizadas = (Long) o[1];
				}
				s.close();
				
			} catch (Exception e) {
				System.err.print(e);
			}
			
		}
		
		TelaInicialDadosLigacoesTempModel tempModel = new TelaInicialDadosLigacoesTempModel();
		tempModel.setEfetivadas(atendidaEfetivado.toString());
		tempModel.setNao_atendida(naoAtendida.toString());
		tempModel.setNao_efetivadas(atendidaNaoEfetivado.toString());
		tempModel.setTarget_fora(target_fora.toString());
		tempModel.setTotal(total.toString());
		tempModel.setLigacoes_restantes(ligacoesNaoRealizadas.toString());
		tempModel.setSelecao_trabalho(ligacoesRealizadas.toString());
		
		Response r = new VOResponse(tempModel);
		LogFileMngr.getInstance().info("Tela Inicial - item dados liga��es processaDados() finalizado");
		
		return r;
	}

	@Override
	protected String getSubtituloItem() {
		return null;
	}

}
