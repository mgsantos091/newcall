package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.TelInicialItemFormListController;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.form.client.Form;

public abstract class TelaInicialItemFormListGenerico extends TelaInicialItemFormGenerico {

	private static final long serialVersionUID = 1L;

	private JLabel lblSubtitle = new JLabel();
	
	public TelaInicialItemFormListGenerico() {
		super();

		TelInicialItemFormListController formController = new TelInicialItemFormListController(this);
		form = new Form();
		form.setFormController(formController);
		form.setLayout(new MigLayout("", "[right][grow]", "[][]"));
		form.setBackground(Color.WHITE);
		form.setVOClassName(getClassItem().getCanonicalName());
		add(form, BorderLayout.CENTER);
		
		lblSubtitle = new JLabel(getSubtituloItem());
		lblSubtitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubtitle.setFont(new Font("Arial Black", Font.PLAIN, 12));
		lblSubtitle.setForeground(new Color(85,136,155));
		form.add(lblSubtitle,"cell 1 0 2097051 1,growx,wrap");
	}
	
	public TelaInicialItemFormListGenerico(MigLayout lmngr) {
		this( );
		form.setLayout(lmngr);
	}
	
	public void addTextBox(String attributeName , String labelName) {
		LabelControl l = new LabelControl(labelName);
		l.setFont(new Font("Arial", Font.BOLD, 12));
		
		TextControl t = new TextControl();
		t.setEnabledOnEdit(false);
		t.setEnabledOnInsert(false);
		t.setAttributeName(attributeName);
		t.setLinkLabel(l);
		t.getBindingComponent().setOpaque(false);
		t.getBindingComponent().setBorder(null);
//		
//		t.setOpaque(false);
//		t.setBackground(Color.WHITE);
//		t.setBorder(null);
		
		form.add(l);
		form.add(t,"wrap");
	}
	
	public void addTextBox(String attributeName1, String attributeName2 , String labelName) {
		LabelControl l = new LabelControl(labelName);
		l.setFont(new Font("Arial", Font.BOLD, 12));
		
		TextControl t1 = new TextControl(5);
		t1.setEnabledOnEdit(false);
		t1.setEnabledOnInsert(false);
		t1.setAttributeName(attributeName1);
		t1.setLinkLabel(l);
		t1.getBindingComponent().setOpaque(false);
		t1.getBindingComponent().setBorder(null);
		t1.setTextAlignment(SwingConstants.CENTER);

		TextControl t2 = new TextControl(5);
		t2.setEnabledOnEdit(false);
		t2.setEnabledOnInsert(false);
		t2.setAttributeName(attributeName2);
		t2.setLinkLabel(l);
		t2.getBindingComponent().setOpaque(false);
		t2.getBindingComponent().setBorder(null);
		t2.setTextAlignment(SwingConstants.CENTER);
		
		form.add(l);
		form.add(t1);
		form.add(t2,"wrap");
		
	}

	public abstract List getDados();
	
	protected abstract String getSubtituloItem();
	
}