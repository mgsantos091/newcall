package org.callcenter.frontend.view.panel;

import java.util.ArrayList;
import java.util.List;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.SessaoUsuario;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.DadosSessaoUsuario;
import org.callcenter.frontend.model.TelaInicialUltimoClienteTempModel;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;

public class TelaInicialUltimoClienteItem extends TelaInicialItemFormListGenerico {

	private static final long serialVersionUID = 1L;

	public TelaInicialUltimoClienteItem( ) {
		addTextBox("ultimo_cliente_id", "Cliente ID");
		addTextBox("ultimo_cliente_nome", "Cliente Nome");
	}
	
	@Override
	public String getNomeItem() {
		return "�LTIMO CLIENTE";
	}

	@Override
	public Class getClassItem() {
		return TelaInicialUltimoClienteTempModel.class;
	}

	@Override
	public Response processaDados(List list) {
		
		LogFileMngr.getInstance().info("Tela Inicial - item ultimo cliente processaDados() iniciado");
		
		DadosSessaoUsuario dadosSessaoUsuario = (DadosSessaoUsuario) list.get(0);
		TelaInicialUltimoClienteTempModel vo = new TelaInicialUltimoClienteTempModel();
		String id = "-";
		String nome = "-";
		ClienteModel cliente = dadosSessaoUsuario.getUltimoClienteLigacao();
		if(cliente!=null) {
			id = cliente.getId().toString();
			nome = cliente.getNomefantasia() != null ? cliente.getNomefantasia() : cliente.getRazaosocial();
			if(nome!=null&&nome.length()>15) nome = nome.substring(0, 15) + "...";
		}
		vo.setUltimo_cliente_id(id);
		vo.setUltimo_cliente_nome(nome);
		
		LogFileMngr.getInstance().info("Tela Inicial - item ultimo cliente processaDados() finalizado");
		
		return new VOResponse(vo);
	}

	@Override
	protected String getSubtituloItem() {
		return null;
	}

	@Override
	public List getDados() {
		LogFileMngr.getInstance().info("Tela Inicial - ultimo cliente getDados()");
		DadosSessaoUsuario dadosSessaoUsuario = SessaoUsuario.getInstance().getDadosSessaoUsuario();
		List list = new ArrayList<DadosSessaoUsuario>();
		list.add(dadosSessaoUsuario);
		return list;
	}

}
