package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.hibernate.Session;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.AbstractCategoryItemLabelGenerator;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class IndicadorContatosRealizadosPanel extends IndicadorGenericoPanel {

	public IndicadorContatosRealizadosPanel(Dimension tamanhoPainel, List<RegistroLigacaoModel> dadosLigacoes) {
		super(tamanhoPainel, dadosLigacoes);
	}

	private static final long serialVersionUID = 1L;
	
/*	public static void main(String args[]) {
//		BarRenderer.setDefaultBarPainter(new StandardBarPainter());
		ApplicationFrame frame = new ApplicationFrame("TESTE");
		IndicadorContatosRealizadosPanel panelChart = new IndicadorContatosRealizadosPanel();
//		panelChart.setPreferredSize(new Dimension(500, 270));
		frame.setPreferredSize(new Dimension(450,350));
		frame.setContentPane(panelChart);
		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
	}*/
	
	class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;
		
		class LabelGenerator extends AbstractCategoryItemLabelGenerator
		implements CategoryItemLabelGenerator {
			
			private Integer category;
			
			public LabelGenerator(int category) {
				this(new Integer(category));
			}
			
			public LabelGenerator(Integer category) {
				super("", NumberFormat.getInstance());
				this.category = category;
			}
			
			private static final long serialVersionUID = 1L;
			private NumberFormat formatter = NumberFormat.getPercentInstance();
			
			private double calculateSeriesTotal(CategoryDataset dataset, int series) {
				double result = 0.0;
				for (int i = 0; i < dataset.getColumnCount(); i++) {
					Number value = dataset.getValue(series, i);
					if (value != null) {
						result = result + value.doubleValue();
					}
				}
				return result;
			}
			
			public String generateLabel(CategoryDataset dataset, int series,
					int category) {
				String result = null;
				double base = 0.0;
				if (this.category != null) {
					final Number b = dataset.getValue(series,
							this.category.intValue());
					base = b.doubleValue();
				} else {
					base = calculateSeriesTotal(dataset, series);
				}
				Number value = dataset.getValue(series, category).intValue();
				if (value != null) {
//					final double v = value.doubleValue();
					final int v = value.intValue();
					// you could apply some formatting here
					result = value.toString() + " ("
							+ this.formatter.format(v / base) + ")";
				}
				return result;
			}
		}
		
		

		private JFreeChart createChart(CategoryDataset categorydataset) {

			JFreeChart chart = ChartFactory.createBarChart("", // chart
					// title
					"", // domain axis label
					"", // range axis label
					categorydataset, // data
					PlotOrientation.VERTICAL, // orientation
					false, // include legend
					true, // tooltips?
					false // URLs?
					);
			chart.setBackgroundPaint(Color.white); // fundo panel
//			chart.removeLegend();

			CategoryPlot plot = chart.getCategoryPlot();
//			plot.setBackgroundPaint(Color.lightGray); // fundo grafico
			plot.setDomainGridlinePaint(Color.white);
			plot.setRangeGridlinePaint(Color.white);
			plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
			plot.setBackgroundPaint(Color.WHITE);
			((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
			
			CategoryAxis domainAxis = plot.getDomainAxis();  
			domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
			
			NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.25);
//			rangeAxis.setLabelAngle(Math.PI / 1.25);
			
//			CategoryItemRenderer renderer = plot.getRenderer();
			BarRenderer renderer = (BarRenderer) plot.getRenderer();

			renderer.setItemLabelsVisible(true);
			// use one or the other of the following lines to see the
			// different modes for the label generator...
			renderer.setItemLabelGenerator(new LabelGenerator(null));
			renderer.setSeriesPaint(0,new Color(85,136,155));
			renderer.setItemMargin(0.0);
			renderer.setDrawBarOutline(false);

//			GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
//			renderer.setSeriesPaint(0, gradientpaint);
			// renderer.setLabelGenerator(new LabelGenerator(0));
			return chart;
			
//			JFreeChart jfreechart = ChartFactory.createBarChart("", "", "",
//					categorydataset, PlotOrientation.VERTICAL, false, true,
//					false);
//
//			CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
//
//			BarRenderer custombarrenderer = new BarRenderer();
//			custombarrenderer
//					.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
//			custombarrenderer.setBaseItemLabelsVisible(true);
//			custombarrenderer.setItemLabelAnchorOffset(10D);
//			custombarrenderer
//					.setBasePositiveItemLabelPosition(new ItemLabelPosition(
//							ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
//			custombarrenderer.setBaseItemLabelsVisible(true);
//			custombarrenderer.setMaximumBarWidth(0.050000000000000003D);
//			custombarrenderer.setBaseItemLabelGenerator(
//	                new StandardCategoryItemLabelGenerator("{3}", 
//	                NumberFormat.getIntegerInstance(), new DecimalFormat("0.00%")));
//			
//			GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
//			custombarrenderer.setSeriesPaint(0, gradientpaint);
//			
//			categoryplot.setRenderer(custombarrenderer);
//
//			DecimalFormat decimalformat = new DecimalFormat("####");
//			decimalformat.setNegativePrefix("(");
//			decimalformat.setNegativeSuffix(")");
//			
//			/*TickUnits tickunits = new TickUnits();
//			tickunits.add(new NumberTickUnit(5, decimalformat));
//			tickunits.add(new NumberTickUnit(10, decimalformat));
//			tickunits.add(new NumberTickUnit(20, decimalformat));
//			tickunits.add(new NumberTickUnit(50, decimalformat));
//			tickunits.add(new NumberTickUnit(100, decimalformat));
//			tickunits.add(new NumberTickUnit(200, decimalformat));
//			tickunits.add(new NumberTickUnit(500, decimalformat));
//			tickunits.add(new NumberTickUnit(1000, decimalformat));
//			tickunits.add(new NumberTickUnit(2000, decimalformat));*/
//			
//			/*ValueAxis valueaxis = categoryplot.getRangeAxis();
//			valueaxis.setStandardTickUnits(tickunits);*/
//			
//			NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
//			numberaxis.setNumberFormatOverride(NumberFormat.getPercentInstance());
//			numberaxis.setUpperMargin(0.10000000000000001D);
////			numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
//			numberaxis.setNumberFormatOverride(new DecimalFormat("0.00%"));
//			/*numberaxis.setRange(0, 200);*/
//			/*numberaxis.setStandardTickUnits(tickunits);*/
//
//			ChartUtilities.applyCurrentTheme(jfreechart);
//
//			return jfreechart;

		}
		
		public Conteudo( )
		{
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			CategoryDataset categorydataset = createDataset();
			
			JFreeChart jfreechart = createChart(categorydataset);
			
			ChartPanel chartpanel = new ChartPanel(jfreechart);
			/*chartpanel.setPreferredSize(new Dimension(500, 270));*/
			add(chartpanel,BorderLayout.CENTER);
			
		}
		
		private CategoryDataset createDataset()
		{
			
			DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();
			
//			if(dadosLigacoes.isEmpty()) {
//				List list = getListBasedOnFilter();
//				if(list!=null&&list.size()>0)
//					dadosLigacoes = list;
//				else {
//					Session session = pdao.getSession();
//					dadosLigacoes = session.createQuery(getBaseSQL()).list();
//					session.close();	
//				}			
//			}
			
			List list = getListBasedOnFilter();
			if(list!=null&&list.size()>0)
				dadosLigacoes = list;
			else {
				if(dadosLigacoes.isEmpty()) {
					Session session = pdao.getSession();
					dadosLigacoes = session.createQuery(getBaseSQL()).list();
					session.close();	
				}
			}
						
//			String baseSQL = ""
//					+ "with tab_dados(ligacao_nao_atendida,ligacao_atendida_nao_efetivado,ligacao_atendida_efetivado,target_fora) as ( "
//					+ "  select "
//					+ "    case when status_ligacao_nao_atendida > 0 then true else false end as ligacao_nao_atendida , "
//					+ "    case when status_ligacao_atendida_nao_efetivado > 0 then true else false end as ligacao_atendida_nao_efetivado , "
//					+ "    case when status_ligacao_atendida_efetivado > 0 then true else false end as ligacao_atendida_efetivado , "
//					+ "    target_fora "
//					+ "  from \"RegistroLigacao\" as reg join ( "
//					+ "    select idcliente , max(id) as idligacao "
//					+ "    from \"RegistroLigacao\" "
//							+ "    where ativo = true "
//							+ "    group by idcliente ) as ult on reg.id = ult.idligacao ) "
//					+ "select 'ligacao_nao_atendida' as serie , count(*) as total from tab_dados where ligacao_nao_atendida = true "
//					+ "union all "
//					+ "select 'ligacao_atendida_nao_efetivado' , count(*) as total from tab_dados where ligacao_atendida_nao_efetivado = true "
//					+ "union all "
//					+ "select 'ligacao_atendida_efetivado' , count(*) as total from tab_dados where ligacao_atendida_efetivado = true "
//					+ "union all "
//					+ "select 'target_fora' , count(*) as total from tab_dados where target_fora = true";
			

			int target_fora = 0;
			int naoAtendida = 0;
			int atendidaNaoEfetivado = 0;
			int atendidaEfetivado = 0;
			for(RegistroLigacaoModel reg : dadosLigacoes) {
				if(reg.isTarget_fora()) target_fora++;
				else if(reg.getStatus_ligacao_nao_atendida()>0) naoAtendida++;
				else if(reg.getStatus_ligacao_atendida_nao_efetivado()>0) atendidaNaoEfetivado++;
				else if(reg.getStatus_ligacao_atendida_efetivado()>0) atendidaEfetivado++;
			}
			
//			Query query = session.createSQLQuery(baseSQL);
//			
//			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//			List results = query.list();
//			
//			Integer valLigacaoNaoAtendida = 0;
//			Integer valLigacaoAtendidaNaoEfetivado = 0;
//			Integer valLigacaoAtendidaEfetivado = 0;
//			Integer valTargetFora = 0;
//			
//			for(Object o : results) {
//				HashMap<String, Object> m = ((HashMap) o);
//				String serie = ((String)m.get("SERIE")).trim();
//				Integer porc = ((BigInteger)m.get("TOTAL")).intValue();
//				if(serie.equals("ligacao_nao_atendida")) valLigacaoNaoAtendida = porc;
//			    else if(serie.equals("ligacao_atendida_nao_efetivado")) valLigacaoAtendidaNaoEfetivado = porc;
//			    else if(serie.equals("ligacao_atendida_efetivado")) valLigacaoAtendidaEfetivado = porc;
//			    else if(serie.equals("target_fora")) valTargetFora = porc;
//			}
			
//			categorydataset.addValue(valTargetFora, "", "Target Fora");
//			categorydataset.addValue(valLigacaoNaoAtendida, "", "Liga��o N�o Atendida");
//			categorydataset.addValue(valLigacaoAtendidaNaoEfetivado,"","Liga��o N�o Efetivado");
//			categorydataset.addValue(valLigacaoAtendidaEfetivado, "", "Liga��o Efetivado");
			categorydataset.addValue(target_fora, "", "Target Fora");
			categorydataset.addValue(naoAtendida, "", "Liga��o N�o Atendida");
			categorydataset.addValue(atendidaNaoEfetivado,"","Liga��o N�o Efetivado");
			categorydataset.addValue(atendidaEfetivado, "", "Liga��o Efetivado");
			
			return categorydataset;
			
		}
		
	}

	@Override
	public void addConteudo() {
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
	}

	@Override
	public String getBaseSQL() {
		String baseSQL = "select registro as registro "
				+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro join fetch "
				+ "registro.cliente as cliente join fetch "
				+ "cliente.endereco as endereco "
				+ "where registro.dataregistro = ( "
				  + "select max(dataregistro) "
				  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
				  + "where regmax.cliente = registro.cliente and regmax.ativo = true "
				+ ")";
		return baseSQL;
	}

	@Override
	public String getNomeKPI() {
		return "CONTATOS REALIZADOS";
	}

}