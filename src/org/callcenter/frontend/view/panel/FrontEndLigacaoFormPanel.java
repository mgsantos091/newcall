package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.FrontEndLigacaoFormController;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.openswing.swing.client.CheckBoxControl;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.form.client.Form;

public class FrontEndLigacaoFormPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Form form = new Form();
	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private SaveButton saveButton = new SaveButton();
	
	MiniCronometro cronometro = new MiniCronometro();
	
	public FrontEndLigacaoFormPanel(FrontEndLigacaoFormController controller) {
	
		controller.setForm(form);
		controller.setCronometroPanel(cronometro);
	
		TitledBorder title = BorderFactory
				.createTitledBorder("LIGA��O");
		title.setTitleFont(new Font("Arial Bold", Font.PLAIN, 18));
		setBorder(title);

		setLayout(new BorderLayout());

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		form.setVOClassName(RegistroLigacaoModel.class.getCanonicalName());
		form.setFormController(controller);
		form.setSaveButton(saveButton);
		form.setInsertButton(insertButton);
		form.setEditButton(editButton);
		form.setReloadButton(reloadButton);
		
		form.setLayout(new MigLayout("", "[][grow][][][][][][][][grow]", "[][][][][][][grow]"));
		add(form, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.NORTH);

		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(insertButton,null);
		buttonsPanel.add(editButton,null);
		buttonsPanel.add(saveButton, null);
		
		LabelControl lblIdLigacao = new LabelControl("C�d Liga��o");
		form.add(lblIdLigacao, "cell 0 0");
		
		NumericControl txtIdLigacao = new NumericControl();
		txtIdLigacao.setColumns(5);
		txtIdLigacao.setEnabledOnEdit(false);
		txtIdLigacao.setEnabledOnInsert(false);
		txtIdLigacao.setAttributeName("id");
		txtIdLigacao.setLinkLabel(lblIdLigacao);
		//		lblIdEmp.setLabelFor(txtIdEmp);
		form.add(txtIdLigacao,"cell 1 0");

		LabelControl lblDdd = new LabelControl("DDD");
		form.add(lblDdd, "cell 2 0,alignx right");

		TextControl txtDdd = new TextControl(3);
		txtDdd.setAttributeName("ddd");
		txtDdd.setMaxCharacters(3);
		txtDdd.setLinkLabel(lblDdd);
		form.add(txtDdd, "cell 3 0,alignx right");		
		
//		LabelControl lblNomeCliente = new LabelControl("Cliente (Nome Fantasia)");
//		form.add(lblNomeCliente, "cell 2 0,alignx right");
//		
//		TextControl txtNomeCliente = new TextControl();
//		txtNomeCliente.setEnabledOnEdit(false);
//		txtNomeCliente.setEnabledOnInsert(false);
//		txtNomeCliente.setAttributeName("cliente.nomefantasia");
//		txtNomeCliente.setLinkLabel(lblNomeCliente);
//		form.add(txtNomeCliente,"cell 3 0 3 1,growx");	
	
		LabelControl lblNumero = new LabelControl("N�mero");
		form.add(lblNumero, "cell 4 0");
		
		FormattedTextControl txtNumero;
		try {
			txtNumero = TextControlFactory
					.criaFormattedTextControl(MaskResources.mask.TEL);
			txtNumero.setAttributeName("numero");
			txtNumero.setLinkLabel(lblNumero);
			txtNumero.setRequired(true);
//			lblTel.setLabelFor(txtNumero);
			form.add(txtNumero,"cell 5 0");
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		
		LabelControl lblTarget = new LabelControl("Fora de Target");
		form.add(lblTarget, "cell 6 0,alignx right");
		
		CheckBoxControl checkForaTarget = new CheckBoxControl();
		checkForaTarget.setAttributeName("target_fora");
		checkForaTarget.setLinkLabel(lblTarget);
		form.add(checkForaTarget,"cell 7 0");

		LabelControl lblStatusLigacaoNaoAtendida = new LabelControl("Status da Liga��o (n�o atendida)");
		form.add(lblStatusLigacaoNaoAtendida,"cell 0 1 6 1");
		
		LabelControl lblStatusLigacaoAtendidaNaoEfetivado = new LabelControl("Status da Liga��o (n�o efetivado)");
		form.add(lblStatusLigacaoAtendidaNaoEfetivado,"cell 6 1 3 1");
		
		
		ComboBoxControl cboStatusLigacaoNaoAtendida = new ComboBoxControl();
		cboStatusLigacaoNaoAtendida.setAttributeName("status_ligacao_nao_atendida");
		cboStatusLigacaoNaoAtendida.setDomainId("LIGACAO_NAO_ATENDIDA");
		cboStatusLigacaoNaoAtendida.setRequired(false);
		cboStatusLigacaoNaoAtendida.setLinkLabel(lblStatusLigacaoNaoAtendida);
		form.add(cboStatusLigacaoNaoAtendida,"cell 0 2 6 1,growx");
		
		ComboBoxControl cboStatusLigacaoAtendidaNaoEfetivado = new ComboBoxControl();
		cboStatusLigacaoAtendidaNaoEfetivado.setAttributeName("status_ligacao_atendida_nao_efetivado");
		cboStatusLigacaoAtendidaNaoEfetivado.setDomainId("LIGACAO_ATENDIDA_NAO_EFETIVADO");
		cboStatusLigacaoAtendidaNaoEfetivado.setRequired(false);
		cboStatusLigacaoAtendidaNaoEfetivado.setLinkLabel(lblStatusLigacaoAtendidaNaoEfetivado);
		form.add(cboStatusLigacaoAtendidaNaoEfetivado,"cell 6 2 3 1,growx");
		
		LabelControl lblStatusLigacaoAtendidaEfetivado = new LabelControl("Status da Liga��o (efetivado)");
		form.add(lblStatusLigacaoAtendidaEfetivado,"cell 0 3 6 1");
		
		JLabel lblTimer = new JLabel("TIMER");
		lblTimer.setFont(new Font("Tahoma", Font.BOLD, 11));
		form.add(lblTimer, "cell 6 3 3 1,alignx center");
		
//		LabelControl lblRepresentante = new LabelControl("Representante (se efetivado = 21)");
//		form.add(lblRepresentante, "cell 6 3 3 1,alignx center");
		
		ComboBoxControl cboStatusLigacaoAtendidaEfetivado = new ComboBoxControl();
		cboStatusLigacaoAtendidaEfetivado.setAttributeName("status_ligacao_atendida_efetivado");
		cboStatusLigacaoAtendidaEfetivado.setDomainId("LIGACAO_ATENDIDA_EFETIVADO");
		cboStatusLigacaoAtendidaEfetivado.setRequired(false);
		cboStatusLigacaoAtendidaEfetivado.setLinkLabel(lblStatusLigacaoAtendidaEfetivado);
		//		cboStatusLigacaoAtendidaEfetivado.addItemListener(new ItemListener() {
		//			public void itemStateChanged(ItemEvent event) {
		//				if (event.getItem().equals("21 - Representante Ausente")) {
		//					FrontEndLigacaoFormPanel.this.txtCboStatusEfetiadoExtra.setEnabled(true);
		//				} else {
		//					FrontEndLigacaoFormPanel.this.txtCboStatusEfetiadoExtra.setValue(null);
		//					FrontEndLigacaoFormPanel.this.txtCboStatusEfetiadoExtra.setEnabled(false);
		//				}
		//			}
		//		});
		//		cboStatusLigacaoAtendidaEfetivado.addItemListener(new ItemListener() {
		//			public void itemStateChanged(ItemEvent event) {
		//				if (event.getItem().equals("21 - Representante Ausente")) {
		//					RepresentanteLookupController controller = new RepresentanteLookupController();
		//					controller.openLookupFrame(MDIFrame.getSelectedFrame(),
		//							FrontEndLigacaoFormPanel.this);
		//
		//					cliente = (PIVClientModel) controller.getLookupVO();
		//					
		//					form.getVOModel().getValueObject();
		//				}
		//			}
		//		});
		form.add(cboStatusLigacaoAtendidaEfetivado,"cell 0 4 6 1,,growx");
		
		form.add(cronometro, "cell 6 4 3 1,alignx center");
		
		LabelControl lblIndicouCompra = new LabelControl("Indicou Compra");
		form.add(lblIndicouCompra, "cell 0 5,alignx trailing");
		
		ComboBoxControl cboIndicouCompra = new ComboBoxControl();
		cboIndicouCompra.setAttributeName("indicou_compra");
		cboIndicouCompra.setDomainId("SIM_OU_NAO");
		cboIndicouCompra.setRequired(false);
		cboIndicouCompra.setLinkLabel(lblIndicouCompra);
		form.add(cboIndicouCompra,"cell 1 5");
				
//		txtCboStatusEfetiadoExtra = new CodLookupControl();
//		txtCboStatusEfetiadoExtra.setLookupController(new RepresentanteLookupController());
//		txtCboStatusEfetiadoExtra.setLookupButtonVisible(true);
//		txtCboStatusEfetiadoExtra.setAttributeName("representante_ausente.nome");
//		txtCboStatusEfetiadoExtra.setEnabledOnEdit(false);
//		txtCboStatusEfetiadoExtra.setEnabledOnInsert(true);
//		txtCboStatusEfetiadoExtra.setRequired(false);
//		form.add(txtCboStatusEfetiadoExtra, "cell 6 4 3 1");
		
//		TextControl txtCboStatusEfetiadoExtra = new TextControl();
//		txtCboStatusEfetiadoExtra.setAttributeName("representante_ausente.nome");
//		txtCboStatusEfetiadoExtra.setRequired(false);
//		txtCboStatusEfetiadoExtra.setEnabledOnEdit(false);
//		txtCboStatusEfetiadoExtra.setEnabledOnInsert(false);
		
		LabelControl lblAnotacao = new LabelControl("Anota��o:");
		form.add(lblAnotacao, "cell 0 6");
//
		TextAreaControl txtAnotacao = new TextAreaControl();
		txtAnotacao.setMaxCharacters(10000);
		txtAnotacao.setAttributeName("anotacao");
		txtAnotacao.setRequired(false);
		txtAnotacao.setLinkLabel(lblAnotacao);
		form.add(txtAnotacao, "cell 1 6 9 1,grow");
		
	}
	
	public class MiniCronometro extends JPanel {
		
		private static final long serialVersionUID = -35363535177961727L;
		
		// GUI Components
	    private JPanel panel;
	    private JLabel timeLabel;

	    // Properties of Program.
	    private byte seconds = 0;
	    private byte minutes = 0;
	    private short hours = 0;

	    private DecimalFormat timeFormatter;

	    private Timer timer;
	    
	    private Timestamp inicio;
	    private Timestamp fim;

	    private ActionListener start = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	if(inicio == null) inicio = new Timestamp((new Date()).getTime());
            	fim = null;
                timer.start();
            }
        };
	    
	    public MiniCronometro() {
	        panel = new JPanel();
	        panel.setLayout(new BorderLayout());

	        timeLabel = new JLabel();
	        timeLabel.setFont(new Font("Consolas", Font.PLAIN, 18));
	        timeLabel.setHorizontalAlignment(JLabel.CENTER);
	        panel.add(timeLabel);

	        timeFormatter = new DecimalFormat("00");

	        timer = new Timer(1000, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	if( seconds < 59 ) {
	            		seconds++;
	            	} else {
	            		if( minutes > 59 ) {
	            			seconds = 0;
	            			minutes = 0;
	            			hours++;
	            		} else {
	            			seconds = 0;
	            			minutes++;
	            		}
	            	}
	            	atualizaLabel();
	            }
	        });

	        atualizaLabel();

	        add(panel);
	    }

		public Timestamp getInicio() {
			return inicio;
		}

		public Timestamp getFim() {
			return fim;
		}
		
		public void reset() {
			stop();
			seconds = 0;
			minutes = 0;
			hours = 0;
			inicio = null;
			fim = null;
			atualizaLabel();
		}
		
		public void atualizaLabel() {
	        timeLabel.setText(timeFormatter.format(hours) + ":"
	                + timeFormatter.format(minutes) + ":"
	                + timeFormatter.format(seconds));
		}
		
		public void setTime(Long time) {
			if(time == null) return;
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			cal.setTimeInMillis(time);
			hours = (short) cal.get(Calendar.HOUR);
			minutes = (byte) cal.get(Calendar.MINUTE);
			seconds = (byte) cal.get(Calendar.SECOND);
			atualizaLabel();
		}

		public void setInicio(Timestamp inicio) {
			this.inicio = inicio;
		}

		public void setFim(Timestamp fim) {
			this.fim = fim;
		}
		
		public void start() {
			start.actionPerformed(null);
		}
		
		public void stop() {
        	fim = new Timestamp((new Date()).getTime());
            timer.stop();
		}
		
	}

	public void reloadForm() {
		this.form.reload();
	}

}