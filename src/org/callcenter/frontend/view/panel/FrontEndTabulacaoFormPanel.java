package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.callcenter.frontend.controller.FrontEndTabulacaoFormController;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.model.ClienteModel;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.form.client.Form;

import net.miginfocom.swing.MigLayout;

public class FrontEndTabulacaoFormPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Form form = new Form();
	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private EditButton editButton = new EditButton();
	private SaveButton saveButton = new SaveButton();
	
	public FrontEndTabulacaoFormPanel(FrontEndTabulacaoFormController controller) {
	
		controller.setForm(form);

		TitledBorder title = BorderFactory
				.createTitledBorder("TABULA��O - CLIENTE SELECIONADO");
		title.setTitleFont(new Font("Arial Bold", Font.PLAIN, 18));
		setBorder(title);

		setLayout(new BorderLayout());

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		form.setVOClassName(ClienteModel.class.getCanonicalName());
		form.setFormController(controller);
		form.setSaveButton(saveButton);
		form.setEditButton(editButton);
		form.setReloadButton(reloadButton);
		
		form.setLayout(new MigLayout("", "[right][50.00][79.00][79.00][79.00][79.00][79.00,grow][][79.00][79.00]", "[][][][][]"));
		add(form, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.NORTH);

		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(editButton,null);
		buttonsPanel.add(saveButton, null);
		
		LabelControl lblIdEmp = new LabelControl("Codigo");
		form.add(lblIdEmp, "cell 0 0");
		
		NumericControl txtIdEmp = new NumericControl();
		txtIdEmp.setColumns(5);
		txtIdEmp.setEnabledOnEdit(false);
		txtIdEmp.setEnabledOnInsert(false);
		txtIdEmp.setAttributeName("id");
		txtIdEmp.setLinkLabel(lblIdEmp);
//		lblIdEmp.setLabelFor(txtIdEmp);
		form.add(txtIdEmp, "cell 1 0");
		
		LabelControl lblRazaoSocial = new LabelControl("Raz�o Social");
		form.add(lblRazaoSocial, "cell 2 0 2 1,alignx right");

		TextControl txtRazaoSocial = new TextControl();
		GridBagLayout gridBagLayout = (GridBagLayout) txtRazaoSocial.getLayout();
		gridBagLayout.columnWeights = new double[]{1.0};
		txtRazaoSocial.setMaxCharacters(120);
		txtRazaoSocial.setTrimText(true);
		txtRazaoSocial.setAttributeName("razaosocial");
		txtRazaoSocial.setEnabledOnEdit(false);
		txtRazaoSocial.setLinkLabel(lblRazaoSocial);
//		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		form.add(txtRazaoSocial, "cell 4 0 6 1,grow");

		LabelControl lblNomeFantasia = new LabelControl("Nome Fantasia");
		form.add(lblNomeFantasia, "cell 0 1");

		TextControl txtNomeFantasia = new TextControl();
		txtNomeFantasia.setMaxCharacters(60);
		txtNomeFantasia.setTrimText(true);
		txtNomeFantasia.setAttributeName("nomefantasia");
		txtNomeFantasia.setEnabledOnEdit(false);
//		lblNomeFantasia.setLabelFor(txtNomeFantasia);
		txtNomeFantasia.setLinkLabel(lblNomeFantasia);
		form.add(txtNomeFantasia, "cell 1 1 9 1,grow");
				
		LabelControl lblTabMelhorDiaDeCompra = new LabelControl("Melhor dia de compra?");
		form.add(lblTabMelhorDiaDeCompra, "cell 0 2,alignx right");
		
		TextControl txtMelhorDiaDeCompra = new TextControl();
		txtMelhorDiaDeCompra.setTrimText(true);
		txtMelhorDiaDeCompra.setAttributeName("tab_dia_compra");
		txtMelhorDiaDeCompra.setLinkLabel(lblTabMelhorDiaDeCompra);
		form.add(txtMelhorDiaDeCompra, "cell 1 2 3 1,grow");
		
		LabelControl lblTabResponsavelCompras = new LabelControl("Respons�vel por compras");
		form.add(lblTabResponsavelCompras, "cell 4 2 2 1,alignx right");
		
		TextControl txtResponsavelCompras = new TextControl();
		txtResponsavelCompras.setTrimText(true);
		txtResponsavelCompras.setAttributeName("tab_responsavel_compras");
		txtResponsavelCompras.setLinkLabel(lblTabResponsavelCompras);
		form.add(txtResponsavelCompras, "cell 6 2 3 1");

		LabelControl lblEmail = new LabelControl("Email");
		form.add(lblEmail, "cell 0 3");

		TextControl txtEmail = new TextControl();
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("tab_email");
		txtEmail.setLinkLabel(lblEmail);
		form.add(txtEmail, "cell 1 3 3 1,grow");
		
		LabelControl lblTelContato = new LabelControl("Outro tel. de contato:");
		form.add(lblTelContato, "cell 0 4");
		
		LabelControl lblDdd = new LabelControl("DDD");
		form.add(lblDdd, "cell 1 4,alignx right");
		
		FormattedTextControl txtNumero;
		try {
			
		TextControl txtDdd = new TextControl(3);
		txtDdd.setAttributeName("tab_tel_contato_ddd");
		txtDdd.setMaxCharacters(3);
		txtDdd.setLinkLabel(lblDdd);
		form.add(txtDdd, "cell 2 4,alignx left");		
			
//		LabelControl lblNomeCliente = new LabelControl("Cliente (Nome Fantasia)");
//		form.add(lblNomeCliente, "cell 2 0,alignx right");
//		
//		TextControl txtNomeCliente = new TextControl();
//		txtNomeCliente.setEnabledOnEdit(false);
//		txtNomeCliente.setEnabledOnInsert(false);
//		txtNomeCliente.setAttributeName("cliente.nomefantasia");
//		txtNomeCliente.setLinkLabel(lblNomeCliente);
//		form.add(txtNomeCliente,"cell 3 0 3 1,growx");	
	
		LabelControl lblNumero = new LabelControl("N�mero");
		form.add(lblNumero, "cell 3 4,alignx right");
		
		txtNumero = TextControlFactory
				.criaFormattedTextControl(MaskResources.mask.TEL);
		txtNumero.setAttributeName("tab_tel_contato_telefone");
//		txtNumero.setRequired(true);
		txtNumero.setLinkLabel(lblNumero);
		//			lblTel.setLabelFor(txtNumero);
					form.add(txtNumero,"cell 4 4 2 1");
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		
		

//		TextControl txtTelContato = new TextControl();
//		txtTelContato.setTrimText(true);
//		txtTelContato.setAttributeName("tab_tel_contato");
//		txtTelContato.setLinkLabel(lblTelContato);
//		form.add(txtTelContato, "cell 1 8 6 1,grow");

	}

	public void reloadForm() {
		this.form.reload();
	}

}