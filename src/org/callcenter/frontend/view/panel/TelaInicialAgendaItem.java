package org.callcenter.frontend.view.panel;

import java.util.ArrayList;
import java.util.List;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.TelaInicialDadosAgendaTempModel;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Event;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;

public class TelaInicialAgendaItem extends TelaInicialItemFormListGenerico {

	private static final long serialVersionUID = 1L;

	public TelaInicialAgendaItem( ) {
		addTextBox("qtde_agenda_dia", "Agendas no dia");
		addLinhaVazio();
	}
	
	@Override
	public String getNomeItem() {
		return "AGENDA";
	}

	@Override
	public Class getClassItem() {
		return TelaInicialDadosAgendaTempModel.class;
	}

	@Override
	public Response processaDados(List list) {
		
		LogFileMngr.getInstance().info("Tela Inicial - item dados agenda processaDados() iniciado");
		
		Integer qtd = 0;
		for (Event event : (ArrayList<Event>) list) {
			java.util.Calendar c1 = event.getStartDate().toCalendar();
			java.util.Calendar c2 = java.util.Calendar.getInstance();
			boolean mesmoDia = c1.get(java.util.Calendar.YEAR) == c2.get(java.util.Calendar.YEAR) &&
	                  c1.get(java.util.Calendar.DAY_OF_YEAR) == c2.get(java.util.Calendar.DAY_OF_YEAR);
			if(mesmoDia) // mesmo dia
				qtd++;
		}
		
		TelaInicialDadosAgendaTempModel vo = new TelaInicialDadosAgendaTempModel();
		vo.setQtde_agenda_dia( String.valueOf(qtd) );
		
		LogFileMngr.getInstance().info("Tela Inicial - item dados agenda processaDados() finalizado");
		
		return new VOResponse(vo);
	}

	@Override
	protected String getSubtituloItem() {
		return null;
	}

	@Override
	public List getDados() {
		LogFileMngr.getInstance().info("Tela Inicial - item dados agenda getDados()");
		return new ArrayList<>( Util.getRepositorioPadrao().getAllEntries() );
	}

}
