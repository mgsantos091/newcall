package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.callcenter.frontend.lib.PrincipalDAO;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.util.java.Consts;

public abstract class TelaInicialItemFormGenerico extends JPanel {

	private static final long serialVersionUID = 1L;

	protected JLabel lbItemName;
	
	protected PrincipalDAO pdao = PrincipalDAO.getInstance();

	protected Form form;
	
	public TelaInicialItemFormGenerico() {
		setBackground(Color.WHITE);
//		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		/* setPreferredSize(); */
		setLayout(new BorderLayout());

		lbItemName = new JLabel(getNomeItem());
		lbItemName.setHorizontalAlignment(SwingConstants.CENTER);
		lbItemName.setFont(new Font("Arial Black", Font.PLAIN, 13));
		lbItemName.setForeground(new Color(85,136,155));
		add(lbItemName, BorderLayout.NORTH);
	}

	public void addLinhaVazio( ) {
		form.add(new JLabel(""),"wrap");
	}
	
	public void refresh() {
		form.reload();
		form.setMode(Consts.READONLY);
	}

	protected abstract String getNomeItem();
	
	protected abstract Class getClassItem();
	
	public abstract Response processaDados(List list);
	
}