package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.callcenter.frontend.controller.IndicadoresFiltroLigacaoGFController;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.IndicadoresFiltroLigacaoGFView;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.profiles.java.GridProfile;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.server.HibernateUtils;

public abstract class IndicadorGenericoPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Dimension size = new Dimension(350, 200);

	private JPanel jpConteudo;

//	private JButton btnEdita;
	private JButton btnAtualiza;
	private JButton btnParametros;

	private JLabel lbKPIName;
	
	protected IndicadoresFiltroLigacaoGFController filtroController;
	protected IndicadoresFiltroLigacaoGFView filtroView;
	
	protected PrincipalDAO pdao = PrincipalDAO.getInstance();
	
	protected List<RegistroLigacaoModel> dadosLigacoes = new ArrayList<RegistroLigacaoModel>();

	private String filtroGridFunctionID = "GRID_FILTRO_INDICADORES_" + getNomeKPI();
	
	private Boolean flagFiltro = false;
	
	public IndicadorGenericoPanel(Dimension tamanhoPainel) {
		this( );
		setPreferredSize(tamanhoPainel);
		addConteudo();
	}
	
	public IndicadorGenericoPanel(Dimension tamanhoPainel , List<RegistroLigacaoModel> dadosLigacoes) {
		this( );
		setPreferredSize(tamanhoPainel);
		this.dadosLigacoes = dadosLigacoes;
		addConteudo();
	}
	
	private IndicadorGenericoPanel() {

		filtroController = new IndicadoresFiltroLigacaoGFController(getBaseSQL(),this);
		
		setBackground(Color.WHITE);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		/* setPreferredSize(); */
		setLayout(new MigLayout("", "[grow][]", "[][grow]"));

		lbKPIName = new JLabel(getNomeKPI());
		lbKPIName.setFont(new Font("Arial Bold", Font.PLAIN, 14));
		add(lbKPIName, "cell 0 0");

		JPanel jpBotoes = new JPanel();
		jpBotoes.setBackground(Color.WHITE);
		add(jpBotoes, "cell 1 0,grow");

//		btnEdita = new JButton(/* "1" */);
//		btnEdita.setPreferredSize(new Dimension(16,16));
//		btnEdita.setBorder(BorderFactory.createEmptyBorder());
//		btnEdita.setBackground(Color.WHITE);
//		btnEdita.setIcon(new ImageIcon(
//				IndicadorGenericoPanel.class
//						.getResource("/images/gerenciamento_relcomercial/outros/edit_16x16.png")));
//		btnEdita.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				edita();
//			}
//		});
//		jpBotoes.add(btnEdita);

		btnAtualiza = new JButton(/* "2" */);
		btnAtualiza.setPreferredSize(new Dimension(16,16));
		btnAtualiza.setBorder(BorderFactory.createEmptyBorder());
		btnAtualiza.setBackground(Color.WHITE);
		btnAtualiza
				.setIcon(new ImageIcon(
						IndicadorGenericoPanel.class
								.getResource("/images/gerenciamento_relcomercial/outros/refresh_16x16.png")));
		btnAtualiza.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
		jpBotoes.add(btnAtualiza);

		btnParametros = new JButton(/* "3" */);
		btnParametros.setPreferredSize(new Dimension(16,16));
		btnParametros.setBorder(BorderFactory.createEmptyBorder());
		btnParametros.setBackground(Color.WHITE);
		btnParametros
				.setIcon(new ImageIcon(
						IndicadorGenericoPanel.class
								.getResource("/images/gerenciamento_relcomercial/outros/configure_16x15.png")));
		btnParametros.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				abrirParametros();
			}
		});
		jpBotoes.add(btnParametros);

		jpConteudo = new JPanel();
		jpConteudo.setLayout(new BorderLayout());
		jpConteudo.setBackground(Color.WHITE);
		add(jpConteudo, "cell 0 1 2 1,grow");

	}

	public Component addConteudo(Component comp) {
		if (jpConteudo.getComponents().length == 0)
			jpConteudo.add(comp, BorderLayout.CENTER);
		return null;
	}
	
	public void removeConteudo( ) {
		jpConteudo.removeAll();
		jpConteudo.revalidate();
	}
	
	public abstract void addConteudo( );

	public void refresh() {
		removeConteudo();
		addConteudo();
		repaint();
		revalidate();
	}

	public void abrirParametros() {
		filtroView = new IndicadoresFiltroLigacaoGFView(getNomeKPI(),filtroGridFunctionID);
		filtroView.setController(filtroController);
		MDIFrame.add(filtroView);
	}
	
	public abstract String getBaseSQL();
	
	public abstract String getNomeKPI();

//	public void edita() {
//
//	}

	@Override
	public Dimension getSize() {
		return size;
	}

	@Override
	public Dimension getMinimumSize() {
		return size;
	}

	@Override
	public Dimension getMaximumSize() {
		return size;
	}

	public void setSize(Dimension size) {
		this.size = size;
		super.setSize(size);
		super.setMinimumSize(size);
		super.setMaximumSize(size);
	}
	
	public void atualizaDadosLigacoes(List<RegistroLigacaoModel> dadosLigacoes) {
		this.dadosLigacoes = dadosLigacoes;
	}
	
	public List getListBasedOnFilter( ) {	
		// busca sql do componente
		String baseSQL = getBaseSQL();
		
		// ---
        GridProfile profile = Util.getGridProfile(filtroGridFunctionID);
        if(profile==null||profile.getQuickFilterValues().size()==0) return null;
        
        enableFlagFiltro();
        
		try {
			ArrayList<Object> values = new ArrayList<Object>();
			ArrayList<Type> types = new ArrayList<Type>();
			String baseSQLPlusFilter = HibernateUtils.applyFiltersAndSorter(
					profile.getQuickFilterValues(),
//					profile.getCurrentSortedColumns(),
					new ArrayList(),
//					profile.getCurrentSortedVersusColumns(),
					new ArrayList(),
			        RegistroLigacaoModel.class,
			        baseSQL,
			        values,
			        types,
			        "registro",
			        pdao.getSessionFactory()
			      );
			Session sess = pdao.getSession();
			List list = sess.createQuery(baseSQLPlusFilter).setParameters(values.toArray(),(Type[])types.toArray(new Type[types.size()])).list();
			return list;
		} catch (Exception e) { }
		
		return null;
		
	}
	
	public void enableFlagFiltro( ) {
		if(flagFiltro==true) return;
		else {
			flagFiltro = true;
			lbKPIName.setText(getNomeKPI() + " [FILTRO]");
			lbKPIName.repaint();
		}
	}
	
	public void disableFlagFiltro( ) {
		if(flagFiltro==false) return;
		else {
			flagFiltro = false;
			lbKPIName.setText(getNomeKPI());
			lbKPIName.repaint();
		}
	}
	
}