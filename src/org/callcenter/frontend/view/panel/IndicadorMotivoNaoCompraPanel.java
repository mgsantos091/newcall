package org.callcenter.frontend.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.hibernate.Session;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.AbstractCategoryItemLabelGenerator;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class IndicadorMotivoNaoCompraPanel extends IndicadorGenericoPanel {

	public IndicadorMotivoNaoCompraPanel(Dimension tamanhoPainel, List<RegistroLigacaoModel> dadosLigacoes) {
		super(tamanhoPainel, dadosLigacoes);
	}

	private static final long serialVersionUID = 1L;

	/*public static void main(String args[]) {
//		BarRenderer.setDefaultBarPainter(new StandardBarPainter());
		ApplicationFrame frame = new ApplicationFrame("TESTE");
		IndicadorMotivoNaoCompraPanel panelChart = new IndicadorMotivoNaoCompraPanel();
//		panelChart.setPreferredSize(new Dimension(500, 270));
		frame.setPreferredSize(new Dimension(450, 350));
		frame.setContentPane(panelChart);
		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
	}*/
	
	class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;
		
		class LabelGenerator extends AbstractCategoryItemLabelGenerator
		implements CategoryItemLabelGenerator {
			
			private Integer category;
			
			public LabelGenerator(int category) {
				this(new Integer(category));
			}
			
			public LabelGenerator(Integer category) {
				super("", NumberFormat.getInstance());
				this.category = category;
			}
			
			private static final long serialVersionUID = 1L;
			private NumberFormat formatter = NumberFormat.getPercentInstance();
			
			private double calculateSeriesTotal(CategoryDataset dataset, int series) {
				double result = 0.0;
				for (int i = 0; i < dataset.getColumnCount(); i++) {
					Number value = dataset.getValue(series, i);
					if (value != null) {
						result = result + value.doubleValue();
					}
				}
				return result;
			}
			
			public String generateLabel(CategoryDataset dataset, int series,
					int category) {
				String result = null;
				double base = 0.0;
				if (this.category != null) {
					final Number b = dataset.getValue(series,
							this.category.intValue());
					base = b.doubleValue();
				} else {
					base = calculateSeriesTotal(dataset, series);
				}
				Number value = dataset.getValue(series, category).intValue();
				if (value != null) {
//					final double v = value.doubleValue();
					final int v = value.intValue();
					// you could apply some formatting here
					result = value.toString() + " ("
							+ this.formatter.format(v / base) + ")";
				}
				return result;
			}
		}
		
		

		private JFreeChart createChart(CategoryDataset categorydataset) {

			JFreeChart chart = ChartFactory.createBarChart("", // chart
					// title
					"", // domain axis label
					"", // range axis label
					categorydataset, // data
					PlotOrientation.VERTICAL, // orientation
					false, // include legend
					true, // tooltips?
					false // URLs?
					);
			chart.setBackgroundPaint(Color.white); // fundo panel
//			chart.removeLegend();

			CategoryPlot plot = chart.getCategoryPlot();
//			plot.setBackgroundPaint(Color.lightGray); // fundo grafico
			plot.setDomainGridlinePaint(Color.white);
			plot.setRangeGridlinePaint(Color.white);
			plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
			plot.setBackgroundPaint(Color.WHITE);
			((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
			
			CategoryAxis domainAxis = plot.getDomainAxis();  
			domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
			
			NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
			rangeAxis.setUpperMargin(0.25);
//			rangeAxis.setLabelAngle(Math.PI / 1.25);
			
//			CategoryItemRenderer renderer = plot.getRenderer();
			BarRenderer renderer = (BarRenderer) plot.getRenderer();

			renderer.setItemLabelsVisible(true);
			// use one or the other of the following lines to see the
			// different modes for the label generator...
			renderer.setItemLabelGenerator(new LabelGenerator(null));
			renderer.setSeriesPaint(0,new Color(85,136,155));
			renderer.setItemMargin(0.0);
			renderer.setDrawBarOutline(false);

			return chart;
		
		}
		
		public Conteudo( )
		{
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			CategoryDataset categorydataset = createDataset();
			
			JFreeChart jfreechart = createChart(categorydataset);
			
			ChartPanel chartpanel = new ChartPanel(jfreechart);
			/*chartpanel.setPreferredSize(new Dimension(500, 270));*/
			add(chartpanel,BorderLayout.CENTER);
			
		}
		
		private CategoryDataset createDataset()
		{
			DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();
			
//			if(dadosLigacoes.isEmpty()) {
//				List list = getListBasedOnFilter();
//				if(list!=null&&list.size()>0)
//					dadosLigacoes = list;
//				else {
//					Session session = pdao.getSession();
//					dadosLigacoes = session.createQuery(getBaseSQL()).list();
//					session.close();	
//				}			
//			}
			
			List list = getListBasedOnFilter();
			if(list!=null&&list.size()>0)
				dadosLigacoes = list;
			else {
				if(dadosLigacoes.isEmpty()) {
					Session session = pdao.getSession();
					dadosLigacoes = session.createQuery(getBaseSQL()).list();
					session.close();	
				}
			}
			
			int barra1 = 0;
			int barra2 = 0;
			int barra3 = 0;
			int barra4 = 0;
			int outros = 0;
			
			TreeMap<String, Integer> mapValores = new TreeMap<String, Integer>();
			
			for(RegistroLigacaoModel reg : dadosLigacoes) {
				if(reg.getStatus_ligacao_atendida_efetivado() > 0) {
					String motivo = reg.getMotivo_ligacao().substring(reg.getMotivo_ligacao().indexOf("-") + 2,reg.getMotivo_ligacao().length());
					Integer qtd = mapValores.get(motivo) == null ? 0 : mapValores.get(motivo);
					mapValores.put(motivo, qtd+1);
				}
			}
			
			SortedSet<Entry<String, Integer>> mapValoresOrdernado = entriesSortedByValues(mapValores);
			Iterator<Entry<String, Integer>> i = mapValoresOrdernado.iterator();
			while(i.hasNext()) {
				Entry<String, Integer> entry = i.next();
				String motivo = entry.getKey();
				Integer qtd = entry.getValue();
				if(barra1==0) {
					barra1 = qtd;
					categorydataset.addValue(barra1, "", motivo);
				} else if(barra2==0) {
					barra2 = qtd;
					categorydataset.addValue(barra2, "", motivo);
				} else if(barra3==0) {
					barra3 = qtd;
					categorydataset.addValue(barra3, "", motivo);
				} else if(barra4==0) {
					barra4 = qtd;
					categorydataset.addValue(barra4, "", motivo);
				} else {
					outros++;
				}
			}
			
			if(outros>0) categorydataset.addValue(outros, "", "Outros");
			
			return categorydataset;
		}
		
	}
	
	static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(
			Map<K, V> map) {
		SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(
				new Comparator<Map.Entry<K, V>>() {
					@Override
					public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
						int res = e2.getValue().compareTo(e1.getValue());
						return res != 0 ? res : 1;
					}
				});
		sortedEntries.addAll(map.entrySet());
		return sortedEntries;
	}

	@Override
	public void addConteudo() {
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
	}

	@Override
	public String getBaseSQL() {
		String baseSQL = "select registro as registro "
				+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro join fetch "
				+ "registro.cliente as cliente join fetch "
				+ "cliente.endereco as endereco "
				+ "where registro.dataregistro = ( "
				  + "select max(dataregistro) "
				  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
				  + "where regmax.cliente = registro.cliente and regmax.ativo = true  "
				+ ") and registro.status_ligacao_atendida_efetivado > 0 and cliente.ativo = true ";
		return baseSQL;
	}

	@Override
	public String getNomeKPI() {
		return "MOTIVOS DA N�O COMPRA";
	}

}