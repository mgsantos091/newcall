package org.callcenter.frontend.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.callcenter.frontend.controller.IndicadoresFiltroLigacaoGFController;
import org.callcenter.frontend.lib.GridColumnFactory;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.MaskResources;
import org.callcenter.frontend.lib.TextControlFactory;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;

public class IndicadoresFiltroLigacaoGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private ExportButton exportButton1 = new ExportButton();

	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colCodMicrosiga = new TextColumn();
	private TextColumn colLojaMicrosiga = new TextColumn();
	private TextColumn colRazaoSocial = new TextColumn();
	private TextColumn colNomeFantasia = new TextColumn();
	private TextColumn colDDD1 = new TextColumn();
	private FormattedTextColumn colTelefone1;
	private TextColumn colCodRepresentante = new TextColumn();
	private TextColumn colNomeRepresentante = new TextColumn();
	private CurrencyColumn colTicketMedio = new CurrencyColumn();
	private DateColumn colPrimeiraCompra = new DateColumn();
	private DateColumn colUltimaCompra = new DateColumn();
	private TextColumn colEstado = new TextColumn();
	private TextColumn colCidade = new TextColumn();
	private DateTimeColumn colDataUltimaLigacao = new DateTimeColumn();
	private TextColumn statusUltimaLigacao = new TextColumn();
	private TextColumn motivoUltimaLigacao = new TextColumn();
	private TextColumn anotacaoUltimaLigacao = new TextColumn();

	public IndicadoresFiltroLigacaoGFView(String nomeFiltro, String filtroGridFunctionID) {
		try {
			super.setTitle("Filtro [" + nomeFiltro + "] - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(IndicadoresFiltroLigacaoGFView.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
			jbInit();
			grid.setFunctionId(filtroGridFunctionID);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void setController(IndicadoresFiltroLigacaoGFController controller) {
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		controller.setGrid(grid);
	}
	
	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		// grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setExportButton(exportButton1);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName(RegistroLigacaoModel.class.getCanonicalName());
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(reloadButton);
		buttonsPanel.add(exportButton1);

		colId.setColumnName("cliente.id");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(35);
		colId.setColumnSortable(true);
		colId.setColumnFilterable(true);
		grid.getColumnContainer().add(colId);
		
		colCodMicrosiga.setColumnName("cliente.cod_microsiga");
		colCodMicrosiga.setPreferredWidth(80);
		colCodMicrosiga.setColumnSortable(true);
		colCodMicrosiga.setColumnFilterable(true);
		grid.getColumnContainer().add(colCodMicrosiga);
		
		colLojaMicrosiga.setColumnName("cliente.loja_microsiga");
		colLojaMicrosiga.setPreferredWidth(35);
		colLojaMicrosiga.setColumnSortable(true);
		colLojaMicrosiga.setColumnFilterable(true);
		grid.getColumnContainer().add(colLojaMicrosiga);

		colRazaoSocial.setColumnName("cliente.razaosocial");
		colRazaoSocial.setMaxCharacters(160);
		colRazaoSocial.setPreferredWidth(225);
		colRazaoSocial.setColumnFilterable(true);
		colRazaoSocial.setColumnSortable(true);
		grid.getColumnContainer().add(colRazaoSocial);
		
		colNomeFantasia.setColumnName("cliente.nomefantasia");
		colNomeFantasia.setMaxCharacters(160);
		colNomeFantasia.setPreferredWidth(150);
		colNomeFantasia.setColumnFilterable(true);
		colNomeFantasia.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeFantasia);
		
		colDDD1.setColumnName("cliente.ddd1");
		colDDD1.setPreferredWidth(50);
		colDDD1.setColumnFilterable(true);
		colDDD1.setColumnSortable(true);
		grid.getColumnContainer().add(colDDD1);
		
		try {
			colTelefone1 = TextControlFactory.criaFormaattedTextColumn(MaskResources.mask.TEL);
			colTelefone1.setColumnName("cliente.telefone1");
			colTelefone1.setPreferredWidth(90);
			colTelefone1.setColumnFilterable(true);
			colTelefone1.setColumnSortable(true);
			grid.getColumnContainer().add(colTelefone1);
		} catch(Exception e) { }
		
		colCodRepresentante.setColumnName("cliente.representante.codigo");
		colCodRepresentante.setPreferredWidth(70);
		colCodRepresentante.setColumnFilterable(true);
		colCodRepresentante.setColumnSortable(true);
		grid.getColumnContainer().add(colCodRepresentante);
		
		colNomeRepresentante.setColumnName("cliente.representante.nome");
		colNomeRepresentante.setPreferredWidth(120);
		colNomeRepresentante.setColumnFilterable(true);
		colNomeRepresentante.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeRepresentante);
		
		colTicketMedio.setColumnName("cliente.ticketmedio");
		colTicketMedio.setPreferredWidth(90);
		colTicketMedio.setColumnFilterable(true);
		colTicketMedio.setColumnSortable(true);
		colTicketMedio.setDecimals(2);
		colTicketMedio.setGrouping(true);
		grid.getColumnContainer().add(colTicketMedio);
		
		colPrimeiraCompra.setColumnName("cliente.data_primeira_compra");
		colPrimeiraCompra.setPreferredWidth(60);
		colPrimeiraCompra.setColumnFilterable(true);
		colPrimeiraCompra.setColumnSortable(true);
		grid.getColumnContainer().add(colPrimeiraCompra);
		
		colUltimaCompra.setColumnName("cliente.data_ultima_compra");
		colUltimaCompra.setPreferredWidth(60);
		colUltimaCompra.setColumnFilterable(true);
		colUltimaCompra.setColumnSortable(true);
		grid.getColumnContainer().add(colUltimaCompra);
		
		colEstado.setColumnName("cliente.endereco.estado");
		colEstado.setPreferredWidth(35);
		colEstado.setColumnFilterable(true);
		colEstado.setColumnSortable(true);
		grid.getColumnContainer().add(colEstado);
		
		colCidade.setColumnName("cliente.endereco.cidade");
		colCidade.setMaxCharacters(80);
		colCidade.setPreferredWidth(160);
		colCidade.setColumnFilterable(true);
		colCidade.setColumnSortable(true);
		grid.getColumnContainer().add(colCidade);

		ComboColumn cbTabProdEstoque = GridColumnFactory.getYesNoColumn("cliente.tab_prod_estoque");
		grid.getColumnContainer().add(cbTabProdEstoque);
		
		ComboColumn cbTabResistenciaMarca = GridColumnFactory.getYesNoColumn("cliente.tab_resistencia_marca");
		grid.getColumnContainer().add(cbTabResistenciaMarca);
		
		ComboColumn cbTabCompraAtualmente = GridColumnFactory.getYesNoColumn("cliente.tab_compra_atualmente");
		grid.getColumnContainer().add(cbTabCompraAtualmente);
		
		ComboColumn cbTabCompraAtacado = GridColumnFactory.getYesNoColumn("cliente.tab_compra_atacado");
		grid.getColumnContainer().add(cbTabCompraAtacado);
		
		ComboColumn cbTabCompraRede = GridColumnFactory.getYesNoColumn("cliente.tab_compra_rede");
		grid.getColumnContainer().add(cbTabCompraRede);
		
		ComboColumn cbTabVisitaRepresentante = GridColumnFactory.getYesNoColumn("cliente.tab_visita_representante");
		grid.getColumnContainer().add(cbTabVisitaRepresentante);
		
		ComboColumn cbTabComprariaNovamente = GridColumnFactory.getYesNoColumn("cliente.tab_compraria_novamente");
		grid.getColumnContainer().add(cbTabComprariaNovamente);
		
		ComboColumn cbTabComprariaCall = GridColumnFactory.getYesNoColumn("cliente.tab_compraria_call");
		grid.getColumnContainer().add(cbTabComprariaCall);
		
//		colDataUltimaLigacao.setColumnName("data_ultima_ligacao");
		colDataUltimaLigacao.setColumnName("dataregistro");
		colDataUltimaLigacao.setPreferredWidth(90);
		colDataUltimaLigacao.setColumnFilterable(true);
		colDataUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(colDataUltimaLigacao);

//		statusUltimaLigacao.setColumnName("status_ultima_ligacao");
		statusUltimaLigacao.setColumnName("status_ligacao");
		statusUltimaLigacao.setPreferredWidth(200);
		statusUltimaLigacao.setColumnFilterable(true);
		statusUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(statusUltimaLigacao);
		
//		motivoUltimaLigacao.setColumnName("motivo_ultima_ligacao");
		motivoUltimaLigacao.setColumnName("motivo_ligacao");
		motivoUltimaLigacao.setPreferredWidth(200);
		motivoUltimaLigacao.setColumnFilterable(true);
		motivoUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(motivoUltimaLigacao);
		
//		anotacaoUltimaLigacao.setColumnName("anotacao");
		anotacaoUltimaLigacao.setColumnName("anotacao");
		anotacaoUltimaLigacao.setPreferredWidth(200);
		anotacaoUltimaLigacao.setColumnFilterable(true);
		anotacaoUltimaLigacao.setColumnSortable(true);
		grid.getColumnContainer().add(anotacaoUltimaLigacao);
		
		grid.setShowPageNumber(false);

		setSize(800, 600);
		
		setUniqueInstance(true);
	}

	public GridControl getGrid() {
		return grid;
	}

}