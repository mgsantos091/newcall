package org.callcenter.frontend.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.panel.IndicadorGenericoPanel;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;

public class IndicadoresFiltroLigacaoGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private GridControl grid;
	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	
	private String baseSQL;

	private IndicadorGenericoPanel indicadorPainel;
	
	public IndicadoresFiltroLigacaoGFController(String baseSQL, IndicadorGenericoPanel indicadorPainel) {
		this.baseSQL = baseSQL;
		this.indicadorPainel = indicadorPainel;
	}

//	@Override
//	public void afterReloadGrid() {
//		this.indicadorPainel.refresh();
//	}
//	
	public void setGrid(GridControl grid) {
		this.grid = grid;
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			Session session = pdao.getSession();
//			Session session = pdao.getFrontEndSession();
			
//			HashMap<String,String> mapeamentoDeCampos = new HashMap<String,String>();
//			mapeamentoDeCampos.put("registro.dataregistro", "id");
//			mapeamentoDeCampos.put("registro.status_ligacao", "status_ligacao");
//			mapeamentoDeCampos.put("registro.motivo_ligacao", "motivo_ligacao");
//			mapeamentoDeCampos.put("registro.anotacao", "anotacao");
			
			Response res = HibernateUtils.getAllFromQuery(
//					mapeamentoDeCampos,
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "registro",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			if(res instanceof VOListResponse) {
				if(((VOListResponse)res).getRows().size()>0) {
					this.indicadorPainel.atualizaDadosLigacoes(((VOListResponse)res).getRows());
					if(filteredColumns != null&&filteredColumns.size()>0) {
						this.indicadorPainel.enableFlagFiltro();
						this.indicadorPainel.refresh();
					} else {
						this.indicadorPainel.disableFlagFiltro();
						this.indicadorPainel.refresh();
					}
				} else JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Filtro n�o localizou nenhum registro, dados no Indicador n�o ser�o atualizados.");
			}

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		RegistroLigacaoModel registro = (RegistroLigacaoModel) this.grid
				.getVOListTableModel().getObjectForRow(row);
		if (!registro.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

	public GridControl getGrid( ) {
		return this.grid;
	}

}