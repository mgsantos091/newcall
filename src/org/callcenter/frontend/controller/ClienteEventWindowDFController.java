package org.callcenter.frontend.controller;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.model.ClienteModel;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;

public class ClienteEventWindowDFController extends FormController {

	private ClienteModel cliente;

	public ClienteEventWindowDFController() {
	}

	public ClienteEventWindowDFController(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public Response loadData(Class valueObjectClass) {
		try {
			return new VOResponse(cliente);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}

}