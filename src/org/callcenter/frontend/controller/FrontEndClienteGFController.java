package org.callcenter.frontend.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.SessaoUsuario;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.AgendaFrontEndTempModel;
import org.callcenter.frontend.model.ClienteFrontEndTempModel;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.DadosSessaoUsuario;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Repository;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Event;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.message.send.java.FilterWhereClause;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.java.Consts;
import org.openswing.swing.util.server.HibernateUtils;

public class FrontEndClienteGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private DadosSessaoUsuario dadosSessaoUsuario = SessaoUsuario.getInstance().getDadosSessaoUsuario();
	
	private FrontEndLigacaoGFController operacoesController;
	private FrontEndTabulacaoFormController tabulacaoController;
	
	private ClienteModel primeiroCliente;

	private HashMap<String, FilterWhereClause[]> filtroMap = new HashMap<>();
	private Boolean temFiltroClienteSemLigacaoMap = false;
	private Boolean temFiltroOperadorMap = false;
	
//	private Boolean primeiraCarga = true;
	
	public FrontEndClienteGFController(FrontEndLigacaoGFController operacoesController, FrontEndTabulacaoFormController tabulacaoController) {
		this.operacoesController = operacoesController;
		this.tabulacaoController = tabulacaoController;
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		ClienteFrontEndTempModel vo = (ClienteFrontEndTempModel) persistentObject;
		ClienteModel cliente = (ClienteModel) PrincipalDAO.getInstance().getObjectByID(ClienteModel.class, "id", vo.getCliente().getId());
//		dadosSessaoUsuario.setUltimaLinhaGridSelecionado(rowNumber);
		operacoesController.setCliente(cliente);
		operacoesController.getGrid().reloadData();
		tabulacaoController.setCliente(cliente);
		tabulacaoController.setFormMode(Consts.READONLY);
		tabulacaoController.reloadForm();
//		tabulacaoController.setFormMode(Consts.EDIT);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
//			String baseSQL = "from org.callcenter.frontend.model.ClienteModel as cliente where ativo = true";
//			String baseSQL = "select registro as registro , cliente as cliente , representante as representante , endereco as endereco "
			String baseSQL = "";
					
			if(this.temFiltroClienteSemLigacaoMap)
//				baseSQL = "select null as registro , cliente as cliente "
//						+ "from org.callcenter.frontend.model.ClienteModel as cliente "
//						+ "where cliente.ativo = true and cliente.id not in ( select distinct idcliente from org.callcenter.frontend.model.RegistroLigacaoModel where ativo = true )";
				baseSQL = "select registro as registro , cliente as cliente "
						+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro right join "
						+ "registro.cliente as cliente left join fetch "
						+ "cliente.endereco as endereco "
						+ "where registro.id is null and cliente.ativo = true";
			else if(this.temFiltroOperadorMap)
				baseSQL = "select registro as registro , cliente as cliente "
						+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro join "
						+ "registro.cliente as cliente left join fetch "
						+ "cliente.endereco as endereco "
						+ "where cliente.operador.id = '" + dadosSessaoUsuario.getUsuario().getId() + "' and cliente.ativo = true";
			else
				baseSQL = "select registro as registro , cliente as cliente "
					+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro right join "
					+ "registro.cliente as cliente left join fetch "
					+ "cliente.endereco as endereco "
					+ "where (registro.dataregistro = ( "
					  + "select max(dataregistro) "
					  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
					  + "where regmax.cliente = registro.cliente and regmax.ativo = true "
					+ ") or registro.id is null) and cliente.ativo = true";
			Session session = pdao.getSession();
//			Session session = pdao.getFrontEndSession();

//			for(Object o : currentSortedColumns) {
//				String column = (String) o;
//				column = column.replace("cliente.","");
//				currentSortedColumns.set(currentSortedColumns.indexOf(o),column);
//			}
			
//			HashMap<String, FilterWhereClause[]> map = (HashMap<String, FilterWhereClause[]>) filteredColumns;
//			for(Entry<String, FilterWhereClause[]> entry : map.entrySet()) {
//			    String key = entry.getKey();
//			    FilterWhereClause[] value = entry.getValue();
//			    map.remove(key);
//			    map.put(key.replace("cliente.",""), value);
//			}
			
//			HashMap m = ((HashMap) filteredColumns);
//			for(Entry<String, FilterWhereClause> entry : m.entrySet()) {
//			    String key = entry.getKey();
//			    HashMap value = entry.getValue();
//
//			}
			
			HashMap<String,String> mapeamentoDeCampos = new HashMap<String,String>();
			mapeamentoDeCampos.put("data_registro", "registro.dataregistro");
//			mapeamentoDeCampos.put("cliente.razaosocial", "razaosocial");
//			mapeamentoDeCampos.put("cliente.nomefantasia", "nomefantasia");
//			mapeamentoDeCampos.put("cliente.cod_microsiga", "cod_microsiga");
//			mapeamentoDeCampos.put("cliente.loja_microsiga", "loja_microsiga");
//			mapeamentoDeCampos.put("cliente.ddd1", "ddd1");
//			mapeamentoDeCampos.put("cliente.telefone1", "telefone1");
//			mapeamentoDeCampos.put("cliente.ticketmedio", "ticketmedio");
//			mapeamentoDeCampos.put("cliente.endereco.cidade", "endereco.cidade");
//			mapeamentoDeCampos.put("cliente.endereco.estado", "endereco.estado");
//			mapeamentoDeCampos.put("cliente.data_primeira_compra", "data_primeira_compra");
//			mapeamentoDeCampos.put("cliente.data_ultima_compra", "data_ultima_compra");
//			mapeamentoDeCampos.put("cliente.tab_prod_estoque","tab_prod_estoque");
//			mapeamentoDeCampos.put("cliente.tab_resistencia_marca","tab_resistencia_marca");
//			mapeamentoDeCampos.put("cliente.tab_compra_atualmente","tab_compra_atualmente");
//			mapeamentoDeCampos.put("cliente.tab_compra_atacado","tab_compra_atacado");
//			mapeamentoDeCampos.put("cliente.tab_compra_rede","tab_compra_rede");
//			mapeamentoDeCampos.put("cliente.tab_visita_representante","tab_visita_representante");
//			mapeamentoDeCampos.put("cliente.tab_compraria_novamente","tab_compraria_novamente");
//			mapeamentoDeCampos.put("cliente.tab_compraria_call","tab_compraria_novamente");
			
//			VOListResponse res = (VOListResponse) HibernateUtils.getBlockFromQuery(
//					mapeamentoDeCampos,
//			        action,
//			        startIndex,
//			        50, // block size...
//			        filteredColumns,
//			        currentSortedColumns,
//			        currentSortedVersusColumns,
//			        valueObjectType,
//			        baseSQL,
//			        new Object[0],
////			        new Type[]{org.hibernate.type.DoubleType.INSTANCE},
//			        new Type[0],
//			        "Cliente",
//			        pdao.getSessionFactory()	,
//			        session
//			      );
			
//			HibernateUtils.applyFiltersAndSorter(filteredColumns, currentSortedColumns, currentSortedVersusColumns, valueObjectType, select, from, where, group, having, order, paramValues, paramTypes, tableName, sessions)
			
			if(filtroMap!=null && filtroMap.size()>0) filteredColumns = filtroMap;
			
//			if( primeiraCarga ) {
//				primeiraCarga = false;
//				startIndex = dadosSessaoUsuario.getUltimaLinhaGridSelecionado() == null ? startIndex : dadosSessaoUsuario.getUltimaLinhaGridSelecionado();
//			}
			
			VOListResponse res = (VOListResponse) HibernateUtils.getBlockFromQuery(
					mapeamentoDeCampos,
					action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			Repository repo = Util.getRepositorioPadrao();
			TreeMap<ClienteModel, AgendaFrontEndTempModel> mapValores = new TreeMap<ClienteModel, AgendaFrontEndTempModel>();
			for (Event event : repo.getAllEntries()) {
				java.util.Calendar c1 = event.getStartDate().toCalendar();
				java.util.Calendar c2 = java.util.Calendar.getInstance();
				boolean mesmoDia = c1.get(java.util.Calendar.YEAR) == c2.get(java.util.Calendar.YEAR) &&
		                  c1.get(java.util.Calendar.DAY_OF_YEAR) == c2.get(java.util.Calendar.DAY_OF_YEAR);
				if(mesmoDia||c2.before(c1)) { // mesmo dia ou evento no futuro
					String value = event.getNewCallData().getValue();
					Integer id;
					if(value.trim().equals("")) 
						continue;
					else
						id = Integer.parseInt(value);
					
					ClienteModel cliente = (ClienteModel) PrincipalDAO.getInstance().getObjectByID(ClienteModel.class, "id", id);
					if(cliente!=null) {
						AgendaFrontEndTempModel agendaTempModel = new AgendaFrontEndTempModel();
						agendaTempModel.setComentario(event.getDescription()==null?"":event.getDescription().getValue());
						agendaTempModel.setStartDate(c1.getTime());
						agendaTempModel.setTitulo(event.getSummary()==null?"":event.getSummary().getValue());
						AgendaFrontEndTempModel agendaAtual = mapValores.get(cliente);
						if(agendaAtual!=null) {
							if(agendaAtual.getStartDate().after(agendaTempModel.getStartDate()))
								mapValores.put(cliente, agendaTempModel);
						}
						 else
							mapValores.put(cliente, agendaTempModel);
					}
				}
			}
			
			for(ClienteFrontEndTempModel vo : (List<ClienteFrontEndTempModel>) ((VOListResponse) res).getRows()) {
				ClienteModel cliente = vo.getCliente();
				AgendaFrontEndTempModel agendaTempModel = mapValores.get(cliente);
				vo.setAgenda(agendaTempModel);
			}
			
//			VOListResponse newResp = new VOListResponse();
			
//			List voList = ((VOListResponse) res).getRows();
//			List newList = newResp.getRows();
//			for(ClienteModel cliente : (List<ClienteModel>) voList) {
//			for(Object o : voList) {
//				String cidade = (cliente.getEndereco()!=null?cliente.getEndereco().getCidade():"");
//				ClienteFrontEndTempModel vo = new ClienteFrontEndTempModel();
//				vo.setCliente((ClienteModel) o[1]);
//				vo.setRegistro((RegistroLigacaoModel) o[0]);
//				vo.setCliente(cliente);
//				vo.setId_cliente(cliente.getId());
//				vo.setNomefantasia(cliente.getNomefantasia());
//				vo.setRazaosocial(cliCente.getRazaosocial());
//				vo.setCidade(cidade);
				
//				baseSQL = "from org.callcenter.frontend.model.RegistroLigacaoModel " 
//						+ "where idcliente = '" + cliente.getId() + "' "
//						+ "and ativo = true "
//						+ "order by dataregistro desc";
//				
//				RegistroLigacaoModel ultimoRegistroLigacao = (RegistroLigacaoModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
//				String status = "SEM REGISTRO DE LIGA��ES";
//				if(ultimoRegistroLigacao!=null) {
//					String motivo = "";
//					if(ultimoRegistroLigacao.isTarget_fora()) status = "TARGET FORA";
//					else if(ultimoRegistroLigacao.getStatus_ligacao_nao_atendida()>0) {
//						status = "LIGA��O N�O ATENDIDA";
//						motivo = ClientSettings.getInstance().getDomain("LIGACAO_NAO_ATENDIDA").getDomainPair(ultimoRegistroLigacao.getStatus_ligacao_nao_atendida()).getDescription();
//					}
//					else if(ultimoRegistroLigacao.getStatus_ligacao_atendida_nao_efetivado()>0) { 
//						status = "LIGA��O N�O EFETIVADO";
//						motivo = ClientSettings.getInstance().getDomain("LIGACAO_ATENDIDA_NAO_EFETIVADO").getDomainPair(ultimoRegistroLigacao.getStatus_ligacao_atendida_nao_efetivado()).getDescription();
//					}
//					else if(ultimoRegistroLigacao.getStatus_ligacao_atendida_efetivado()>0) { 
//						status = "LIGA��O EFETIVADO";
//						motivo = ClientSettings.getInstance().getDomain("LIGACAO_ATENDIDA_EFETIVADO").getDomainPair(ultimoRegistroLigacao.getStatus_ligacao_atendida_efetivado()).getDescription();
//					}
//					vo.setData_ultima_ligacao(ultimoRegistroLigacao.getDataregistro());
//					vo.setStatus_ultima_ligacao(status);
//					vo.setMotivo_ultima_ligacao(motivo);
//					vo.setAnotacao(ultimoRegistroLigacao.getAnotacao());
//				}

//				newList.add(vo);
//			}
			
			if(primeiroCliente==null) {
				if(((VOListResponse) res).getRows().size()>0) {
					primeiroCliente = (ClienteModel) ((ClienteFrontEndTempModel)((VOListResponse) res).getRows().get(0)).getCliente();
//					primeiroCliente = (ClienteModel) voList.get(0)[1];
					operacoesController.setCliente(primeiroCliente);
					tabulacaoController.setCliente(primeiroCliente);
				}
				operacoesController.getGrid().reloadData();
				tabulacaoController.setFormMode(Consts.READONLY);
				tabulacaoController.reloadForm();
			}
			
//			newResp.setMoreRows(res.isMoreRows());
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void setGrid(GridControl grid) {
		tabulacaoController.setGrid(grid);
	}

	public void setAgendaFiltro(HashMap<String, FilterWhereClause[]> filtroMap) {
		this.filtroMap = filtroMap;
	}

	public void removeFiltroClienteSemLigacao() {
		this.temFiltroClienteSemLigacaoMap = false;
	}

	public void adicionaFiltroClienteSemLigacao() {
		this.temFiltroClienteSemLigacaoMap = true;
	}
	
	public void removeFiltroClientesDoOperador() {
		this.temFiltroOperadorMap = false;
	}

	public void adicionaFiltroClientesDoOperador() {
		this.temFiltroOperadorMap = true;
	}

	public void setTabulacoesCliente(ClienteModel cliente) {
		operacoesController.setCliente(cliente);
		operacoesController.getGrid().reloadData();
		tabulacaoController.setCliente(cliente);
		tabulacaoController.setFormMode(Consts.READONLY);
		tabulacaoController.reloadForm();
	}

}