package org.callcenter.frontend.controller;

import java.util.List;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.view.panel.TelaInicialItemFormSQLGenerico;
import org.hibernate.Session;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class TelInicialItemFormSQLController extends FormController {

	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private TelaInicialItemFormSQLGenerico form;

	public TelInicialItemFormSQLController(TelaInicialItemFormSQLGenerico form) {
		this.form = form;
	}

	public TelInicialItemFormSQLController() {
	}
	
	public Response loadData(Class valueObjectClass) {
		try {
			LogFileMngr.getInstance().info("Tela Inicial - item dados controller loadData() iniciado");
			Session session = pdao.getSession();
			String baseSQL = form.getBaseSQL();
			List<ValueObjectImpl> list = session.createQuery(baseSQL).list();
			Response r = form.processaDados(list);
			session.close();
			LogFileMngr.getInstance().info("Tela Inicial - item dados controller loadData() finalizado");
			return r;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

}