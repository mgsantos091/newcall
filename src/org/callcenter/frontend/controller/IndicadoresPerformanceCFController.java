package org.callcenter.frontend.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.panel.IndicadorContatosRealizadosPanel;
import org.callcenter.frontend.view.panel.IndicadorMotivoNaoCompraPanel;
import org.hibernate.Session;

public class IndicadoresPerformanceCFController {

	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	
	private IndicadorContatosRealizadosPanel contatosRealizadosPanel;
	private IndicadorMotivoNaoCompraPanel motivoNaoCompraPanel;

	private Session session;
	
	public IndicadoresPerformanceCFController( ) {

		session = pdao.getSession();
//		session = pdao.getIndicadoresPerformanceSession();
		List<RegistroLigacaoModel> list = getUltimaLigacaoPorCliente();

		contatosRealizadosPanel = new IndicadorContatosRealizadosPanel(new Dimension(450,350),list);
		motivoNaoCompraPanel = new IndicadorMotivoNaoCompraPanel(new Dimension(450, 350), list);
		
		session.close();
	}
	
	private List<RegistroLigacaoModel> getUltimaLigacaoPorCliente( ) {
		String baseSQL = "select registro as registro "
				+ "from org.callcenter.frontend.model.RegistroLigacaoModel as registro join fetch "
				+ "registro.cliente as cliente join fetch "
				+ "cliente.endereco as endereco "
				+ "where registro.dataregistro = ( "
				  + "select max(dataregistro) "
				  + "from org.callcenter.frontend.model.RegistroLigacaoModel as regmax "
				  + "where regmax.cliente = registro.cliente and regmax.ativo = true "
				+ ") and cliente.ativo = true";
		ArrayList<RegistroLigacaoModel> list = (ArrayList<RegistroLigacaoModel>) session.createQuery(baseSQL).list();
		return list;
	}

	public IndicadorContatosRealizadosPanel getContatosRealizadosPanel() {
		return contatosRealizadosPanel;
	}

	public IndicadorMotivoNaoCompraPanel getMotivoNaoCompraPanel() {
		return motivoNaoCompraPanel;
	}
}
