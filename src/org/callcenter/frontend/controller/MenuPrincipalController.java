package org.callcenter.frontend.controller;

import java.awt.Font;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.callcenter.frontend.lib.AvisoAgendaThread;
import org.callcenter.frontend.lib.ConfGlobal;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.SessaoUsuario;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.EnderecoModel;
import org.callcenter.frontend.model.StatusLigacaoModel;
import org.callcenter.frontend.model.UsuarioModel;
import org.callcenter.frontend.start.LoginDialog;
import org.callcenter.frontend.start.SplashScreenFrame;
import org.callcenter.frontend.view.TelaInicialCFView;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.internationalization.java.BrazilianPortugueseOnlyResourceFactory;
import org.openswing.swing.internationalization.java.Language;
import org.openswing.swing.internationalization.java.ResourcesFactory;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.mdi.client.ClientFacade;
import org.openswing.swing.mdi.client.Clock;
import org.openswing.swing.mdi.client.MDIController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.mdi.java.ApplicationFunction;
import org.openswing.swing.permissions.client.LoginController;
import org.openswing.swing.table.profiles.client.FileGridProfileManager;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.java.Consts;

public class MenuPrincipalController implements MDIController, LoginController {

	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private MenuPrincipalFacade menufacade = new MenuPrincipalFacade();

	private ConfGlobal globalSettings = ConfGlobal.getInstance();

	private LogFileMngr logMngr = LogFileMngr.getInstance();
	
	private SplashScreenFrame frameSplash;
	private AvisoAgendaThread avisoAgendaThread;

	public MenuPrincipalController( SplashScreenFrame frameSplash ) {
		this.frameSplash = frameSplash;
		this.frameSplash.setBarraDeProgressoVal(60);
		configClientSettings();
	}

	@Override
	public ArrayList<Language> getLanguages() {
		ArrayList list = new ArrayList();
		list.add(new Language("BR", "Brazilian Portuguese"));
		return list;
	}

	private void configClientSettings( ) {

		this.frameSplash.setBarraDeProgressoVal(70);
		
		frameSplash.addText("Configurando o Call Center...");
		System.out.println("Configurando o Call Center...");
		
		Hashtable domains = new Hashtable();

		Domain simOuNaoDomain = new Domain("SIM_OU_NAO");
		simOuNaoDomain.addDomainPair(0, "");
		simOuNaoDomain.addDomainPair(1, "1 - Sim");
		simOuNaoDomain.addDomainPair(2, "2 - N�o");
		domains.put(simOuNaoDomain.getDomainId(), simOuNaoDomain);
		
		// CARREGA STATUS LIGA��O
		
		Session s = pdao.getSession();
		
		Domain ligacaoNaoAtendidaDomain = new Domain("LIGACAO_NAO_ATENDIDA");
		ligacaoNaoAtendidaDomain.addDomainPair(0, "");
		List<StatusLigacaoModel> ligacaoNaoAtendidaList = s.createCriteria(StatusLigacaoModel.class).add(Restrictions.eq("tipo_status", 1)).addOrder(Order.asc("cod_status")).list();
		for(StatusLigacaoModel statusLigacao : ligacaoNaoAtendidaList)
			ligacaoNaoAtendidaDomain.addDomainPair(statusLigacao.getCod_status(), statusLigacao.getCod_status().toString().trim() + " - " + statusLigacao.getStatus_desc());
		domains.put(ligacaoNaoAtendidaDomain.getDomainId(), ligacaoNaoAtendidaDomain);
		
		Domain ligacaoAtendidaNaoEfetivado = new Domain("LIGACAO_ATENDIDA_NAO_EFETIVADO");
		ligacaoAtendidaNaoEfetivado.addDomainPair(0, "");
		List<StatusLigacaoModel> ligacaoNaoEfetivadoList = s.createCriteria(StatusLigacaoModel.class).add(Restrictions.eq("tipo_status", 2)).addOrder(Order.asc("cod_status")).list();
		for(StatusLigacaoModel statusLigacao : ligacaoNaoEfetivadoList)
			ligacaoAtendidaNaoEfetivado.addDomainPair(statusLigacao.getCod_status(), statusLigacao.getCod_status().toString().trim() + " - " + statusLigacao.getStatus_desc());
		domains.put(ligacaoAtendidaNaoEfetivado.getDomainId(), ligacaoAtendidaNaoEfetivado);
		
		Domain ligacaoAtendidaEfetivado = new Domain("LIGACAO_ATENDIDA_EFETIVADO");
		ligacaoAtendidaEfetivado.addDomainPair(0, "");
		List<StatusLigacaoModel> ligacaoEfetivadoList = s.createCriteria(StatusLigacaoModel.class).add(Restrictions.eq("tipo_status", 3)).addOrder(Order.asc("cod_status")).list();
		for(StatusLigacaoModel statusLigacao : ligacaoEfetivadoList)
			ligacaoAtendidaEfetivado.addDomainPair(statusLigacao.getCod_status(), statusLigacao.getCod_status().toString().trim() + " - " + statusLigacao.getStatus_desc());
		domains.put(ligacaoAtendidaEfetivado.getDomainId(), ligacaoAtendidaEfetivado);
		
		s.close();
		
		// FIM CARREGA STATUS LIGA��O
		
		Domain diaDaSemanaDomain = new Domain("DIAS_DA_SEMANA");
		diaDaSemanaDomain.addDomainPair(Calendar.SUNDAY, "Domingo");
		diaDaSemanaDomain.addDomainPair(Calendar.MONDAY, "Segunda-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.TUESDAY, "Ter�a-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.WEDNESDAY, "Quarta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.THURSDAY, "Quinta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.FRIDAY, "Sexta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.SATURDAY, "Sab�do");

		domains.put(diaDaSemanaDomain.getDomainId(), diaDaSemanaDomain);

		Domain paisDomain = new Domain("PAIS");
		paisDomain.addDomainPair("BRASIL", "BRASIL");
		
		domains.put(paisDomain.getDomainId(), paisDomain);

		Domain estadoDomain = new Domain("ESTADO");
		estadoDomain.addDomainPair("AC", "Acre");
		estadoDomain.addDomainPair("AL", "Alagoas");
		estadoDomain.addDomainPair("AP", "Amap�");
		estadoDomain.addDomainPair("AM", "Amazonas");
		estadoDomain.addDomainPair("BA", "Bahia");
		estadoDomain.addDomainPair("CE", "Cear�");
		estadoDomain.addDomainPair("DF", "Distrito Federal");
		estadoDomain.addDomainPair("ES", "Esp�rito Santo");
		estadoDomain.addDomainPair("GO", "Goi�s");
		estadoDomain.addDomainPair("MA", "Maranh�o");
		estadoDomain.addDomainPair("MT", "Mato Grosso");
		estadoDomain.addDomainPair("MS", "Mato Grosso do Sul");
		estadoDomain.addDomainPair("MG", "Minas Gerais");
		estadoDomain.addDomainPair("PA", "Par�");
		estadoDomain.addDomainPair("PB", "Para�ba");
		estadoDomain.addDomainPair("PR", "Paran�");
		estadoDomain.addDomainPair("PE", "Pernambuco");
		estadoDomain.addDomainPair("PI", "Piau�");
		estadoDomain.addDomainPair("RJ", "Rio de Janeiro");
		estadoDomain.addDomainPair("RN", "Rio Grande do Norte");
		estadoDomain.addDomainPair("RS", "Rio Grande do Sul");
		estadoDomain.addDomainPair("RO", "Rond�nia");
		estadoDomain.addDomainPair("RR", "Roraima");
		estadoDomain.addDomainPair("SC", "Santa Catarina");
		estadoDomain.addDomainPair("SP", "S�o Paulo");
		estadoDomain.addDomainPair("SE", "Sergipe");
		estadoDomain.addDomainPair("TO", "Tocantis");

		domains.put(estadoDomain.getDomainId(), estadoDomain);

		Properties props = new Properties();

		// 'Universal'
		props.setProperty("id", "C�d.");
		props.setProperty("dataregistro", "Data de inclus�o");
		props.setProperty("data_registro", "Data de inclus�o");
		props.setProperty("ativo", "Ativo ?");
		props.setProperty("cep", "Cep");
		props.setProperty("pais", "Pais");
		props.setProperty("cidade", "Cidade");
		props.setProperty("estado", "estado");
		props.setProperty("bairro", "Bairro");
		props.setProperty("logradouro", "Logradouro");
		props.setProperty("endnumero", "Numero");
		props.setProperty("endreferencia", "Refer�ncia");
		props.setProperty("endcomplemento", "Complemento");
		props.setProperty("cnpjcpf", "CPNJ/CPF");
		props.setProperty("pjuridica", "P. Juridica?");
		props.setProperty("razaosocial", "Raz�o social");
		props.setProperty("nomefantasia", "Nome fantasia");
		props.setProperty("email", "E-mail");
		props.setProperty("website", "Website");
		props.setProperty("nomeimagem", "Nome imagem");
		props.setProperty("nome", "Nome");
		props.setProperty("telefone1", "Telefone 1");
		props.setProperty("telefone2", "Telefone 2");
		props.setProperty("celular", "Celular");
		props.setProperty("site", "Website");
		props.setProperty("email", "Email");
		props.setProperty("funcao", "Fun��o");
		props.setProperty("responsavel", "Respons�vel");
		props.setProperty("descricao", "Descri��o");
		props.setProperty("valcompra", "Valor de compra");
		props.setProperty("valvenda", "Valor de venda");
		props.setProperty("tag", "Tag");
		props.setProperty("login", "Login");
		props.setProperty("senha", "Senha");
		props.setProperty("usuariovendedor", "Vendedor?");
		props.setProperty("anotacao", "Anota��o");
		props.setProperty("nome_arquivo_voz", "Nome do arquivo de voz");
		props.setProperty("cnpj", "Cnpj");
		props.setProperty("diadasemana", "Dia da semana");
		props.setProperty("horariomanhainicio", "Turno da manh� in�cio");
		props.setProperty("horariomanhafim", "Turno da manh� fim");
		props.setProperty("horariotardeinicio", "Turno da tarde in�cio");
		props.setProperty("horariotardefim", "Turno da tarde fim");
		props.setProperty("horarionoiteinicio", "Turno da noite in�cio");
		props.setProperty("horarionoitefim", "Turno da noite fim");
		props.setProperty("idnivelcomercial", "Nivel comercial");
		props.setProperty("valvenda","Valor de venda");
		props.setProperty("valdesconto","(R$) Desconto");
		props.setProperty("porcdesconto","(%) Desconto");
		props.setProperty("qtditens","Qtd. de itens");
		props.setProperty("valtotal","Valor total");
		props.setProperty("valortotal","Valor total");
		props.setProperty("vendaefetivada","Situa��o");
		props.setProperty("publico_alvo","P�blico alvo");
		props.setProperty("patrocinador","Patrocinador");
		props.setProperty("status","Status");
		props.setProperty("nome_campanha","Nome da campanha");
		props.setProperty("descricao_campanha","Descri��o");
		props.setProperty("item","Item");
		props.setProperty("valsubtotal", "Valor sub-total");
		
		// Tabela - Endere�o
		props.setProperty("endereco.cep", "Cep");
		props.setProperty("endereco.pais", "Pais");
		props.setProperty("endereco.cidade", "Cidade");
		props.setProperty("endereco.estado", "estado");
		props.setProperty("endereco.bairro", "Bairro");
		props.setProperty("endereco.logradouro", "Logradouro");

		// Tabela - Cliente
		props.setProperty("cliente.id", "C�d. Cliente");
		props.setProperty("cliente.cod_microsiga", "C�d. Microsiga");
		props.setProperty("cliente.loja_microsiga", "Loja Microsiga");
		props.setProperty("cliente.cep", "Cep");
		props.setProperty("cliente.endnumero", "Numero");
		props.setProperty("cliente.endreferencia", "Refer�ncia");
		props.setProperty("cliente.endcomplemento", "Complemento");
		props.setProperty("cliente.cnpjcpf", "CPNJ/CPF");
		props.setProperty("cliente.pjuridica", "P. juridica?");
		props.setProperty("cliente.razaosocial", "Raz�o social");
		props.setProperty("cliente.nomefantasia", "Nome fantasia");
		props.setProperty("cliente.ddd1", "DDD1");
		props.setProperty("cliente.telefone1", "Telefone1");
		props.setProperty("cliente.email", "E-mail");
		props.setProperty("cliente.website", "Website");
		props.setProperty("cliente.nomeimagem", "Nome imagem");
		props.setProperty("cliente.representante.codigo", "Cod. R.");
		props.setProperty("cliente.representante.nome", "Representante");
		props.setProperty("cliente.ticketmedio", "Ticket M�dio");
		props.setProperty("cliente.data_primeira_compra", "Dt. Primeira Compra");
		props.setProperty("cliente.data_ultima_compra", "Dt. �ltima Compra");
		props.setProperty("cliente.endereco.cep", "CEP");
		props.setProperty("cliente.endereco.estado", "Estado");
		props.setProperty("cliente.endereco.cidade", "Cidade");
		props.setProperty("cliente.endereco.logradouro", "Logradouro");
		props.setProperty("cliente.endereco.bairro", "Bairro");
		props.setProperty("cliente.tab_prod_estoque", "Produto Estoque?");
		props.setProperty("cliente.tab_resistencia_marca", "Resistente Marca?");
		props.setProperty("cliente.tab_compra_atualmente", "Compra Atualmente?");
		props.setProperty("cliente.tab_compra_atacado", "Compra Atacado?");
		props.setProperty("cliente.tab_compra_rede", "Compra Rede?");
		props.setProperty("cliente.tab_compraria_novamente", "Compraria Novamente?");
		props.setProperty("cliente.tab_compraria_call", "Compraria Call?");
		props.setProperty("cliente.tab_visita_representante", "Visita Frequente Representante?");
		props.setProperty("cliente.dataregitro", "Data de registro");

		//MAILING GERAL
		props.setProperty("cliente.importacao.nome_arquivo","Mailing Nome");
		props.setProperty("cliente.importacao.descricao","Mailing Desc.");
		props.setProperty("cliente.importacao.dataregistro","Mailing Data");
		
		// Tabela - Contato
		props.setProperty("contato.id", "C�d. Contato");
		props.setProperty("contato.nome", "Nome");
		props.setProperty("contato.telefone", "Telefone");
		props.setProperty("contato.celular", "Celular");
		props.setProperty("contato.site", "Website");
		props.setProperty("contato.email", "Email");
		props.setProperty("contato.funcao", "Fun��o");
		props.setProperty("contato.responsavel", "Respons�vel?");
		props.setProperty("contato.dataregitro", "Data de registro");

		// Especial - Grid tempor�rio para Liga��o
		props.setProperty("tipo", "Tipo");
		props.setProperty("numero", "N�mero");
		props.setProperty("duracao", "Dura��o da Liga��o");
		props.setProperty("status_ligacao_nao_atendida", "Liga��o n�o Atendida");
		props.setProperty("ligacao_atendida", "Liga��o Atendida");
		props.setProperty("status_ligacao_atendida_nao_efetivado","Liga��o N�o Efetivado");
		props.setProperty("status_ligacao_atendida_efetivado","Liga��o Efetivado");
		props.setProperty("indicou_compra","Indicou Compra?");
		
		// Especial - Grid tempor�rio para Clientes
		props.setProperty("cliente.id", "C�d.");
		
//		props.setProperty("data_ultima_ligacao", "Dt. �lt. Liga��o");
		props.setProperty("registro.dataregistro", "Dt. �lt. Liga��o");
//		props.setProperty("status_ultima_ligacao", "Status �lt. Liga��o");
		props.setProperty("registro.status_ligacao", "Status �lt. Liga��o");
//		props.setProperty("motivo_ultima_ligacao", "Motivo �lt. Liga��o");
		props.setProperty("registro.motivo_ligacao", "Motivo �lt. Liga��o");
//		props.setProperty("anotacao", "Anota��o");
		props.setProperty("registro.anotacao", "Anota��o");
		
		props.setProperty("agenda.startDate", "Dt. Agenda");
		props.setProperty("agenda.titulo", "Titulo Agenda");
		props.setProperty("agenda.comentario", "Coment. Agenda");
		
		props.setProperty("dataregistro", "Dt. Liga��o");
		props.setProperty("status_ligacao", "Status Liga��o");
		props.setProperty("motivo_ligacao", "Motivo Liga��o");
		props.setProperty("anotacao", "Anota��o");
		
		// Tabela - Categoria
		props.setProperty("categoria.id", "C�d. categoria");
		props.setProperty("categoria.nome", "Nome");
		props.setProperty("categoria.descricao", "Descri��o");
		props.setProperty("categoria.dataregitro", "Data de registro");

		// Tabela - Produto
		props.setProperty("produto.id", "C�d. produto");
		props.setProperty("produto.nome", "Nome");
		props.setProperty("produto.descricao", "Descri��o");
		props.setProperty("produto.valcompra", "Valor de compra");
		props.setProperty("produto.valvenda", "Valor de venda");
		props.setProperty("produto.nomeimagem", "Nome imagem");
		props.setProperty("produto.dataregitro", "Data de registro");;

		// Tabela - Tag
		props.setProperty("tag.id", "C�d. Tag");
		props.setProperty("tag.nome", "Nome");
		props.setProperty("tag.descricao", "Descri��o");
		props.setProperty("tag.dataregistro", "Data de registro");

		// Tabela - Usuario
		props.setProperty("usuario.id", "C�d. usu�rio");
		props.setProperty("usuario.nome", "Nome");
		props.setProperty("usuario.login", "Login");
		props.setProperty("usuario.senha", "Senha");
		props.setProperty("usuario.usuariovendedor", "Vendedor?");
		props.setProperty("usuario.dataregistro", "Data de registro");

		// Tabela - Vendedor
		props.setProperty("vendedor.id", "C�d. Vendedor");
		props.setProperty("vendedor.nome", "Nome");
		props.setProperty("vendedor.cep", "Cep");
		props.setProperty("vendedor.endnumero", "N�mero");
		props.setProperty("vendedor.endcomplemento", "Complemento");
		props.setProperty("vendedor.dataregistro", "Data de registro");

		// Tabela - NivelComercialRelacionamento
		props.setProperty("relacionamento.idnivelcomercial", "Nivel comercial");
		props.setProperty("relacionamento.completado", "Completo?");
		props.setProperty("relacionamento.ativo", "Ativo?");
		props.setProperty("relacionamento.dataregistro", "Data de registro");

		// Tabela - GeoPosicionamentoCliente
		props.setProperty("geoposicionamento.id", "");
		props.setProperty("geoposicionamento.latitude", "Latitude");
		props.setProperty("geoposicionamento.longitude", "Longitude");

		// Tabela - AnotacaoDeDadosCliente
		props.setProperty("anotacao.id", "C�d. Anota��o");
		props.setProperty("anotacao.anotacao", "Anota��o");
		props.setProperty("anotacao.nome_arquivo_voz", "Nome do arquivo de voz");
		props.setProperty("anotacao.dataregistro", "Data de registro");

		// Tabela - Configura��es Locais
		props.setProperty("conflocal.id", "");
		props.setProperty("conflocal.razaosocial", "Raz�o Social");
		props.setProperty("conflocal.nomefantasia", "Nome Fantasia");
		props.setProperty("conflocal.email", "E-mail");
		props.setProperty("conflocal.website", "Website");
		props.setProperty("conflocal.cnpj", "Cnpj");
		props.setProperty("conflocal.nomeimagem", "Nome Imagem");
		props.setProperty("conflocal.cep", "Cep");
		props.setProperty("conflocal.endnumero", "Numero");
		props.setProperty("conflocal.endreferencia", "Refer�ncia");
		props.setProperty("conflocal.endcomplemento", "Complemento");
		props.setProperty("conflocal.cnpjcpf", "CPNJ/CPF");
		props.setProperty("conflocal.dataregitro", "Data de registro");
		props.setProperty("conflocal.latitude", "Latitude");
		props.setProperty("conflocal.longitude", "Longitude");

		// Tabela - HorarioFuncionario
		props.setProperty("turno.diadasemana", "Dia da semana");
		props.setProperty("turno.horariomanhainicio", "Turno da manh� in�cio");
		props.setProperty("turno.horariomanhafim", "Turno da manh� fim");
		props.setProperty("turno.horariotardeinicio", "Turno da tarde in�cio");
		props.setProperty("turno.horariotardefim", "Turno da tarde fim");
		props.setProperty("turno.horarionoiteinicio", "Turno da noite in�cio");
		props.setProperty("turno.horarionoitefim", "Turno da noite fim");

		// Tabela - VendasFilho
		props.setProperty("item.id","C�d. Item");
		props.setProperty("item.vendaPai","C�d. da venda");
		props.setProperty("item.produto.nome","Nome do produto");
		props.setProperty("item.valvenda","Valor de venda");
		props.setProperty("item.valdesconto","(%) desconto");
		props.setProperty("item.qtditens","Qtd. de itens");
		props.setProperty("item.valtotal","Valor total");

		// Tabela - TipoCampanha
		props.setProperty("tipo.id","C�d. Tipo de Campanha");
		props.setProperty("tipo.nome","Tipo");
		props.setProperty("tipo.ativo","Ativo?");
		props.setProperty("tipo.data_registro","Data de registro");
		
		// Tabela - CampanhaPai
		props.setProperty("campanha.id","C�d. Campanha");
		props.setProperty("campanha.nome_campanha","Nome");
		props.setProperty("campanha.descricao_campanha","Descri��o");
		props.setProperty("campanha.publico_alvo","P�blico alvo");
		props.setProperty("campanha.patrocinador","Patrocinador");
		props.setProperty("campanha.status","Status");
		props.setProperty("campanha.data_registro","Data de registro");
		
  		ResourcesFactory resource = new BrazilianPortugueseOnlyResourceFactory(props, false);
  		
		resource.getResources().getDictionary().setProperty(Consts.EQ,"Igual a");
		resource.getResources().getDictionary().setProperty(Consts.GE,"Maior ou igual que");
		resource.getResources().getDictionary().setProperty(Consts.GT,"Maior que");
		resource.getResources().getDictionary().setProperty(Consts.IS_NOT_NULL,"Esta preenchido");
		resource.getResources().getDictionary().setProperty(Consts.IS_NULL,"N�o esta preenchido");
		resource.getResources().getDictionary().setProperty(Consts.LE,"Menor ou igual que");
		resource.getResources().getDictionary().setProperty(Consts.LIKE,"Cont�m");
		resource.getResources().getDictionary().setProperty(Consts.LT,"Menor que");
		resource.getResources().getDictionary().setProperty(Consts.NEQ,"N�o igual �");
		resource.getResources().getDictionary().setProperty(Consts.IN,"Cont�m os valores");
		resource.getResources().getDictionary().setProperty(Consts.ASC_SORTED,"Crescente");
  		resource.getResources().getDictionary().setProperty(Consts.DESC_SORTED,"Decrescente");
  		resource.getResources().getDictionary().setProperty(Consts.NOT_IN,"N�o cont�m os valores");

		ClientSettings clientSettings = new ClientSettings(
				resource,
				domains);

//		ClientSettings.BACKGROUND = "background-logoplayiv_2560x1600.jpg";
//		ClientSettings.BACKGROUND = "azul-claro.png";
//		ClientSettings.BACKGROUND = "prototipo1.jpg";
//		ClientSettings.BACKGROUND = "frontend.jpg";
		ClientSettings.BACKGROUND = "oficial2_1920x1080.jpg";
		ClientSettings.BACK_IMAGE_DISPOSITION = Consts.BACK_IMAGE_STRETCHED;
		ClientSettings.HEADER_FONT = new Font("Arial", Font.PLAIN, 14);
		/*ClientSettings.VIEW_BACKGROUND_SEL_COLOR = true;*/
		ClientSettings.TREE_BACK = null;
		ClientSettings.VIEW_MANDATORY_SYMBOL = true;
		ClientSettings.FILTER_PANEL_ON_GRID = true;
		ClientSettings.SHOW_FILTER_SYMBOL = true;
		ClientSettings.ASK_BEFORE_CLOSE = true;
		ClientSettings.GRID_PROFILE_MANAGER = new FileGridProfileManager();
		ClientSettings.LOOKUP_FRAME_CONTENT = LookupController.GRID_AND_FILTER_FRAME;
		ClientSettings.STORE_INTERNAL_FRAME_PROFILE = true;
		ClientSettings.AUTO_EXPAND_TREE_MENU = true;
		ClientSettings.SEARCH_ADDITIONAL_ROWS = true;
		ClientSettings.SELECT_DATA_IN_EDITABLE_GRID = false;
		ClientSettings.SHOW_TREE_MENU_ROOT = false;
		ClientSettings.SHOW_EVENT_QUEUE_EXCEPTIONS = true;
		ClientSettings.RELOAD_LAST_VO_ON_FORM = true;
		ClientSettings.ALLOW_OR_OPERATOR = false;
//		ClientSettings.FILTER_PANEL_ON_GRID_POLICY = Consts.FILTER_PANEL_ON_GRID_USE_CLOSE_BUTTON;
		/*ClientSettings.AUTO_FIT_COLUMNS = true;*/

		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "net.infonode.gui.laf.InfoNodeLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.birosoft.liquid.LiquidLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "ch.randelshofer.quaqua.QuaquaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.l2fprod.gui.plaf.skin.SkinLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceMistSilverLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceModerateLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "net.sourceforge.mlf.metouia.MetouiaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.fife.plaf.Office2003.Office2003LookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.fife.plaf.OfficeXP.OfficeXPLookAndFeel";

		/*ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		"org.fife.plaf.VisualStudio2005.VisualStudio2005LookAndFeel";*/

		// Realiza a checagem das pastas padr�o a serem utilizadas
		// nas funcionalidades presentes no PlayIV

//		frameSplash.addText("Realizando a checagem de pastas internas do PlayIV...");
//		System.out.println("Realizando a checagem de pastas internas do PlayIV...");

//		gerenciarDiretorios.checkOrganizacaoPastas();

		// ativa objeto respons�vel por gerenciar os logs
		
//		frameSplash.addText("Inicializando o sistema de logs do NewcALL...");
//		System.out.println("Inicializando o sistema de logs do NewcALL...");
//		
//		logMngr = LogFileMngr.getInstance();
//		logMngr.iniciar();
//		
//		frameSplash.addText("Logs iniciados... verificando dados para importa��o...");
//		System.out.println("Opera��o completada, seja bem vindo :)");
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			LogFileMngr.getInstance().error(e.getMessage(),e);
//			JOptionPane.showMessageDialog(null, e.getMessage());
//			e.printStackTrace();
//		}
				
		// ---------------
		// AREA EDITADA PARA TESTES

		
//		LoginPanel d = new LoginPanel(this);
		try {
			HashMap login = new HashMap<>();
			login.put("username", "mateus");
			login.put("password", "531879");
			authenticateUser(login);
			loginSuccessful(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*MDIFrame mdi = new MDIFrame(this);
		PIVSalesStatsGFController controller = new PIVSalesStatsGFController(null);*/
		/*PIVSalesTargetGFController controller = new PIVSalesTargetGFController(null );*/
		
		/*String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = pdao.getSession();

		PIVLocalConfModel vo = (PIVLocalConfModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();

		if (vo == null) {
			vo = new PIVLocalConfModel();
			vo.setVoip_dominio("brvoz.net.br");
			vo.setVoip_proxy("brvoz.net.br");
			vo.setVoip_sipport(5060);
			vo.setEmail_server_smpt_host("smtp.gmail.com");
			vo.setEmail_server_smpt_port(465);
			vo.setEmail_server_ssl(true);
			pdao.save(vo);
		}*/
		
//		Session session = pdao.getSession();
//		String baseSQL = "from org.callcenter.frontend.model.ClienteModel";
//		ClienteModel cliente = (ClienteModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
		
//		frameSplash.dispose();
		
//		loginSuccessful(null);
		
	}

	@Override
	public int getMaxAttempts() {
		System.out.println("getMaxAttempts");
		return 5;
	}

	@Override
	public boolean authenticateUser(Map loginInfo) throws Exception {

		String usr = (String)loginInfo.get("username");
		String pwd = (String)loginInfo.get("password");
		
//		String baseSQL = "from org.callcenter.frontend.model.UsuarioModel where login = '" + usr + "' and senha = '" + pwd + "'";
		Session s = pdao.getSession();
		UsuarioModel usuario = (UsuarioModel) s.createCriteria(UsuarioModel.class).add(Restrictions.eq("login", usr)).add(Restrictions.eq("senha", pwd)).uniqueResult();
//		UsuarioModel usuario = (UsuarioModel) s.createQuery(baseSQL).uniqueResult();
		s.close();
		if(usuario!=null) {
			SessaoUsuario sessUsuario = SessaoUsuario.getInstance();
			sessUsuario.registraUsuarioSessao(usuario);
			return true;
		}
		else return false;
	}

	@Override
	public void loginSuccessful(Map loginInfo) {
		MDIFrame mdi = new MDIFrame(this);
		mdi.setIconImage(Toolkit.getDefaultToolkit().getImage(
				MenuPrincipalController.class.getResource("/images/icone_newcall_150x150.jpg")));
		
		this.frameSplash.setBarraDeProgressoVal(90);
		
		avisoAgendaThread = new AvisoAgendaThread();
		avisoAgendaThread.start();
		
		TelaInicialCFView telaInicialView = new TelaInicialCFView();
		MDIFrame.add(telaInicialView, true);
		
		frameSplash.setBarraDeProgressoVal(100);
		frameSplash.dispose();
	}

//	@Override
//	public void afterMDIcreation(MDIFrame frame) {
//		MDIFrame.addStatusComponent(new Clock());
//		PIVSipVoipFrame voipFrame = PIVSipVoipFrame.getInstance();
//		MDIFrame.addStatusComponent(voipFrame);
//	}

	@Override
	public String getMDIFrameTitle() {
		return "CALL CENTER - SIRIUS SOFTWARE";
	}

	@Override
	public int getExtendedState() {
		return JFrame.MAXIMIZED_BOTH;
	}

	@Override
	public String getAboutText() {
		return "";
	}

	@Override
	public String getAboutImage() {
		return null;
	}

	@Override
	public void stopApplication() {
		pdao.closeConnection();
		logMngr.encerrar();
		System.exit(0);
	}

	@Override
	public boolean viewChangeLanguageInMenuBar() {
		return false;
	}

	@Override
	public boolean viewLoginInMenuBar() {
		return true;
	}

	@Override
	public JDialog viewLoginDialog(JFrame parentFrame) {
		LoginDialog d = new LoginDialog(this);
		return d;
	}

	@Override
	public boolean viewFunctionsInTreePanel() {
		return false;
	}

	@Override
	public boolean viewFunctionsInMenuBar() {
		return true;
	}

	@Override
	public ClientFacade getClientFacade() {
		return this.menufacade;
	}

	@Override
	public DefaultTreeModel getApplicationFunctions() {
		
		DefaultMutableTreeNode root = new OpenSwingTreeNode();

		DefaultTreeModel model = new DefaultTreeModel(root);

//		ApplicationFunction inicio = new ApplicationFunction("Interface Inicial","iniciarInterfaceInicial", "", "iniciarInterfaceInicial");
		ApplicationFunction frontEnd = new ApplicationFunction("Front End", null);
//		ApplicationFunction operacoes = new ApplicationFunction("Opera��es", null);
		ApplicationFunction cadastro = new ApplicationFunction("Cadastros",null);
		ApplicationFunction relatorio = new ApplicationFunction("Relat�rios",null);		
		ApplicationFunction configuracao = new ApplicationFunction("Configura��es",null);
		
		ApplicationFunction telaInicial = new ApplicationFunction(
				"Inicio", "iniciarTelaApresentacao", "", "iniciarTelaApresentacao");
		
		ApplicationFunction moduloFrontEnd = new ApplicationFunction(
				"Principal", "iniciarFrontEnd", "", "iniciarFrontEnd"); // 3 parametro: localiza��o imagem
		
		ApplicationFunction moduloKPIs = new ApplicationFunction(
				"Indicadores de Performance", "iniciarInterfaceKPIs", "", "iniciarInterfaceKPIs");
		
		ApplicationFunction moduloAgenda = new ApplicationFunction(
				"Agenda", "iniciarAgenda", "", "iniciarAgenda");

		ApplicationFunction cadastroCliente = new ApplicationFunction(
				"Clientes", "gerenciarCadCliente", "", "gerenciarCadCliente");
		
//		ApplicationFunction operacaoLigacoes = new ApplicationFunction(
//				"Liga��es", "gerenciarLigacoes", "", "gerenciarLigacoes");

//		root.add(inicio);
		
		frontEnd.add(telaInicial);
		frontEnd.add(moduloFrontEnd);
		frontEnd.add(moduloKPIs);
		frontEnd.add(moduloAgenda);
		root.add(frontEnd);
		
//		operacoes.add(operacaoLigacoes);
//		root.add(operacoes);

		cadastro.add(cadastroCliente);
		root.add(cadastro);

		root.add(relatorio);
		
		root.add(configuracao);

		return model;
	}

	@Override
	public boolean viewOpenedWindowIcons() {
		return true;
	}

	@Override
	public boolean viewFileMenu() {
		return true;
	}

	@Override
	public void afterMDIcreation(MDIFrame frame) {
		MDIFrame.addStatusComponent(new Clock());
	}

}