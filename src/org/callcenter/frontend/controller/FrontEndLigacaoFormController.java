package org.callcenter.frontend.controller;

import java.sql.Timestamp;
import java.util.Date;

import javax.swing.JOptionPane;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.SessaoUsuario;
import org.callcenter.frontend.lib.ValidacaoController;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.DadosSessaoUsuario;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.model.UsuarioModel;
import org.callcenter.frontend.view.panel.FrontEndLigacaoFormPanel;
import org.callcenter.frontend.view.panel.FrontEndLigacaoFormPanel.MiniCronometro;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;

public class FrontEndLigacaoFormController extends FormController {

	private GridControl grid;
	private RegistroLigacaoModel registro;

	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private Form form;
	private ClienteModel cliente;
	
	private FrontEndLigacaoFormPanel.MiniCronometro cronometroPanel;
	
	private DadosSessaoUsuario dadosSessaoUsuario = SessaoUsuario.getInstance().getDadosSessaoUsuario();

	public FrontEndLigacaoFormController(RegistroLigacaoModel registro) {
		this.registro = registro;
	}
	
	public FrontEndLigacaoFormController(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public FrontEndLigacaoFormController(GridControl gridFrame) {
		this.grid = gridFrame;
	}
	
	public FrontEndLigacaoFormController(GridControl gridFrame, ClienteModel cliente) {
		this.grid = gridFrame;
		this.cliente = cliente;
	}

	public FrontEndLigacaoFormController(GridControl gridFrame,
			RegistroLigacaoModel registro) {
		this(registro);
		this.grid = gridFrame;
	}

	public FrontEndLigacaoFormController() {
	}

	public Response loadData(Class valueObjectClass) {
		try {
			if(registro!=null&&registro.getDuracao()!=null) {
				long t = registro.getDuracao().getTime();
				this.cronometroPanel.setTime(t);
				this.cronometroPanel.setInicio(registro.getInicio());
				this.cronometroPanel.setFim(registro.getFim());
				this.cronometroPanel.atualizaLabel();
			} else
				this.cronometroPanel.reset();
			return new VOResponse(registro);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	@Override
	public void afterEditData() {
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}
	
	@Override
	public void afterInsertData() {
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}
	
	@Override
	public void afterEditData(Form form) {
		this.cronometroPanel.setEnabled(false);
	}
	
	@Override
	public boolean beforeEditData(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		if(vo==null||vo.getId()==null) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), 
					"Nenhum registro selecionado.", 
					"A��o inv�lida", 
					JOptionPane.ERROR_MESSAGE);
			return false;
		} else return true;
	}
	
	@Override
	public void afterInsertData(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		if(vo!=null) {
			vo.setCliente(cliente);
			if(cliente!=null) {
				vo.setDdd(cliente.getDdd1());
				vo.setNumero(cliente.getTelefone1());
			}
		}
		
		if (cliente.getOperador() == null) {
			// Cliente se torna do Operador
			cliente.setOperador(dadosSessaoUsuario.getUsuario());
			pdao.update(cliente);
			// if (this.grid != null)
			// this.grid.reloadCurrentBlockOfData();
		}
		
		this.cronometroPanel.reset();
		this.cronometroPanel.start();
		form.pull();
	}
	
	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosLigacao(vo);
	}
	
	@Override
	public boolean beforeSaveDataInEdit(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosLigacao(vo);
	}
	
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
//			if(vo.getRepresentante_ausente()!=null&&vo.getRepresentante_ausente().getCodigo()==null) vo.setRepresentante_ausente(null);
			pdao.update(vo);
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {

			RegistroLigacaoModel vo = (RegistroLigacaoModel) newPersistentObject;
			vo.setAtivo(true);

			this.cronometroPanel.stop();
			
			Timestamp inicio = this.cronometroPanel.getInicio();
			Timestamp fim = this.cronometroPanel.getFim();
			if (inicio != null && fim == null) {
				fim = new Timestamp((new Date()).getTime());
			}
			vo.setInicio(inicio);
			vo.setFim(fim);
			
//			if(vo.getRepresentante_ausente()!=null&&vo.getRepresentante_ausente().getCodigo()==null) vo.setRepresentante_ausente(null);
			
			UsuarioModel usuario = SessaoUsuario.getInstance().getUsuarioSessao();
			
			vo.setCliente(cliente);
			vo.setUsuario(usuario);
			pdao.save(vo);

			dadosSessaoUsuario.setUltimoClienteLigacao(cliente);
			PrincipalDAO.getInstance().update(dadosSessaoUsuario);
			
			return new VOResponse(vo);

		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}


	public GridControl getGridFrame() {
		return this.grid;
	}

	public RegistroLigacaoModel getRegistro() {
		return registro;
	}

	public void setForm(Form form) {
		this.form = form;
	}
	
	public void setFormMode(int mode) {
		if(this.form!=null) this.form.setMode(mode);
	}

	public void setRegistro(RegistroLigacaoModel registro) {
		this.registro = registro;
		if(registro!=null)
			this.cliente = registro.getCliente();
	}

	public void reloadForm() {
		if(this.form!=null) this.form.reload();
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public void setGrid(GridControl grid) {
		this.grid = grid;
	}

	public void setCronometroPanel(MiniCronometro cronometro) {
		this.cronometroPanel = cronometro;
	}

}