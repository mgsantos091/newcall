package org.callcenter.frontend.controller;

import java.util.List;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.view.panel.TelaInicialItemFormListGenerico;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;

public class TelInicialItemFormListController extends FormController {

	private TelaInicialItemFormListGenerico form;

	public TelInicialItemFormListController(TelaInicialItemFormListGenerico form) {
		this.form = form;
	}

	public TelInicialItemFormListController() {
	}
	
	public Response loadData(Class valueObjectClass) {
		try {
			LogFileMngr.getInstance().info("Tela Inicial - item dados controller list loadData() iniciado");
			List dados = form.getDados();
			Response r = form.processaDados(dados);
			LogFileMngr.getInstance().info("Tela Inicial - item dados controller list loadData() finalizado");
			return r;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

}