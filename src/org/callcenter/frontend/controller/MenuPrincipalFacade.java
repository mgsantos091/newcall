package org.callcenter.frontend.controller;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.swing.k5n.k5ncal.AgendaCFView;
import org.callcenter.frontend.view.CadClienteGFView;
import org.callcenter.frontend.view.FrontEndCFView;
import org.callcenter.frontend.view.IndicadoresPerformanceCFView;
import org.callcenter.frontend.view.TelaInicialCFView;
import org.openswing.swing.mdi.client.ClientFacade;
import org.openswing.swing.mdi.client.MDIFrame;

public class MenuPrincipalFacade implements ClientFacade {

	public MenuPrincipalFacade( ) {

	}
	
	public void iniciarTelaApresentacao() {
		try {
			TelaInicialCFView telaInicialView = new TelaInicialCFView();
			MDIFrame.add(telaInicialView, true);
		} catch (Exception ex) {
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}
	
	public void iniciarInterfaceKPIs( ) {
		try {
			IndicadoresPerformanceCFView indicadoresPerformanceView = new IndicadoresPerformanceCFView();
			MDIFrame.add(indicadoresPerformanceView, true);
		} catch (Exception ex) {
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}
	
	public void iniciarFrontEnd( ) {
		try {
			FrontEndCFView frontEndView = new FrontEndCFView();
			MDIFrame.add(frontEndView, true);
		} catch (Exception ex) {
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}
	
	public void iniciarAgenda( ) {
		try {
			AgendaCFView calendarioView = new AgendaCFView();
			MDIFrame.add(calendarioView, true);
		} catch (Exception ex) {
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void gerenciarCadCliente( ) {
		try {
			CadClienteGFController cadClienteController = new CadClienteGFController();
			CadClienteGFView cadClienteView = new CadClienteGFView();
			cadClienteView.setController(cadClienteController);
			MDIFrame.add(cadClienteView);
		} catch (Exception ex) {
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void gerenciarCadRepresentante( ) {
//		new PIVUserMngtGFController( );
	}

}