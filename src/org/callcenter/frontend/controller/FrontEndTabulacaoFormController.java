package org.callcenter.frontend.controller;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.ValidacaoController;
import org.callcenter.frontend.model.ClienteModel;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;

public class FrontEndTabulacaoFormController extends FormController {

	private GridControl grid;
	private ClienteModel cliente;

	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private Form form;

	public FrontEndTabulacaoFormController(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public FrontEndTabulacaoFormController(GridControl gridFrame) {
		this.grid = gridFrame;
	}

	public FrontEndTabulacaoFormController(GridControl gridFrame,
			ClienteModel cliente) {
		this(cliente);
		this.grid = gridFrame;
	}

	public FrontEndTabulacaoFormController() {
	}

//	@Override
//	public boolean beforeEditData(Form form) {
//		ClienteModel vo = (ClienteModel) form.getVOModel()
//				.getValueObject();
//		if(vo!=null) {
//			if(vo.getTab_compra_atacado()==null) vo.setTab_compra_atacado(2);
//			if(vo.getTab_compra_atualmente()==null) vo.setTab_compra_atualmente(2);
//			if(vo.getTab_compra_rede()==null) vo.setTab_compra_rede(2);
//			if(vo.getTab_compraria_call()==null) vo.setTab_compraria_call(2);
//			if(vo.getTab_compraria_novamente()==null) vo.setTab_compraria_novamente(2);
//			if(vo.getTab_prod_estoque()==null) vo.setTab_prod_estoque(2);
//			if(vo.getTab_resistencia_marca()==null) vo.setTab_resistencia_marca(2);
//			if(vo.getTab_extra_prod_encarte()==null) vo.setTab_extra_prod_encarte(2);
//			if(vo.getTab_extra_fora_rede()==null) vo.setTab_extra_fora_rede(2);
//		}
//		form.pull();
//		return true;
//	}
	
	public Response loadData(Class valueObjectClass) {
		try {
			return new VOResponse(cliente);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		ClienteModel vo = (ClienteModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosCliente(vo);
	}
	
	@Override
	public boolean beforeSaveDataInEdit(Form form) {
		ClienteModel vo = (ClienteModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosCliente(vo);
	}

	@Override
	public void afterEditData() {
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}

	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			ClienteModel vo = (ClienteModel) persistentObject;
			if(vo.getEndereco().getId()==null) pdao.save(vo.getEndereco());
			else pdao.update(vo.getEndereco());
			pdao.update(vo);
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public GridControl getGridFrame() {
		return this.grid;
	}

	public ClienteModel getCliente() {
		return cliente;
	}

	public void setForm(Form form) {
		this.form = form;
	}
	
	public void setFormMode(int mode) {
		if(this.form!=null) this.form.setMode(mode);
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}

	public void reloadForm() {
		if(this.form!=null) this.form.reload();
	}

	public void setGrid(GridControl grid) {
		this.grid = grid;
	}

}