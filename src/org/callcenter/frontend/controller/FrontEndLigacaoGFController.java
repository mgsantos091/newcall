package org.callcenter.frontend.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.java.Consts;
import org.openswing.swing.util.server.HibernateUtils;

public class FrontEndLigacaoGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private GridControl grid;
	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private ClienteModel cliente;
	private RegistroLigacaoModel primeiroRegistro;
	
	private FrontEndLigacaoFormController ligacaoFormController;

	public FrontEndLigacaoGFController(FrontEndLigacaoFormController ligacaoFormController) {
		this.ligacaoFormController = ligacaoFormController;
	}

	public void setGrid(GridControl grid) {
		this.grid = grid;
		this.ligacaoFormController.setGrid(grid);
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
		ligacaoFormController.setCliente(cliente);
		ligacaoFormController.setRegistro(vo);
		ligacaoFormController.setFormMode(Consts.READONLY);
		ligacaoFormController.reloadForm();
//		RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
//		OpeLigacoesDFController opLigacaoController = new OpeLigacoesDFController(grid, vo);
//		OpeLigacoesDFView cadClienteView = opLigacaoController.getFrame();
//		MDIFrame.add(cadClienteView);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
//			if(cliente == null) return new ErrorResponse("Nenhum cliente selecionado");
			if(cliente == null) return new VOResponse();
			
			String baseSQL = "from org.callcenter.frontend.model.RegistroLigacaoModel as RegistroLigacao where idcliente = '" + cliente.getId() + "'";
			Session session = pdao.getSession();
//			Session session = pdao.getFrontEndSession();
			
			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "RegistroLigacao",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			if(primeiroRegistro==null) { // se o primeiro registro estiver vazio, apontar automaticamente o form pro primeiro registro encontrado no grid
				if(((VOListResponse) res).getRows().size()>0) {
					primeiroRegistro = (RegistroLigacaoModel) ((VOListResponse) res).getRows().get(0);
					ligacaoFormController.setRegistro(primeiroRegistro);
				} else ligacaoFormController.setRegistro(null);
				ligacaoFormController.setFormMode(Consts.READONLY);
				ligacaoFormController.reloadForm();
			}
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		RegistroLigacaoModel registro = (RegistroLigacaoModel) this.grid
				.getVOListTableModel().getObjectForRow(row);
		if (!registro.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

	public GridControl getGrid( ) {
		return this.grid;
	}

	public ClienteModel getCliente() {
		return cliente;
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
		this.primeiroRegistro = null; // null pois quando o grid recarregar, o form precisa apontar pro primeiro registro de liga��o (olhar loadData)
		this.ligacaoFormController.setCliente(cliente);
	}
	
	public void setFormCliente(ClienteModel cliente) {
		this.ligacaoFormController.setCliente(cliente);
	}
	
	public void setFormCliente() {
		this.ligacaoFormController.setCliente(cliente);
	}
	
	public void setFormRegistro(RegistroLigacaoModel registro) {
		this.ligacaoFormController.setRegistro(registro);
	}
	
	public void reloadForm() {
		this.ligacaoFormController.reloadForm();
	}

	public void setFormMode(int mode) {
		this.ligacaoFormController.setFormMode(mode);
	}

}