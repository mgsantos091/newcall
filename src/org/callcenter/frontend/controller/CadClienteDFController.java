package org.callcenter.frontend.controller;

import java.beans.PropertyVetoException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.ValidacaoController;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.EnderecoModel;
import org.callcenter.frontend.view.CadClienteDFView;
import org.hibernate.Session;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;

import br.com.caelum.stella.MessageProducer;
import br.com.caelum.stella.ResourceBundleMessageProducer;
import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import br.com.caelum.stella.validation.Validator;

public class CadClienteDFController extends FormController {

	private GridControl grid;
	private CadClienteDFView frame;
	private ClienteModel cliente;

	private PrincipalDAO pdao = PrincipalDAO.getInstance();

	public CadClienteDFController() {
		frame = new CadClienteDFView(this);
		frame.getPanel().setMode(Consts.INSERT);
	}

	public CadClienteDFController(ClienteModel cliente) {
		this.cliente = cliente;
		frame = new CadClienteDFView(this);
		frame.getPanel().setMode(Consts.EDIT);
		frame.getPanel().reload();
	}

	public CadClienteDFController(GridControl grid,
			ClienteModel cliente) {
		this(cliente);
		this.cliente = cliente;
		this.grid = grid;
	}

	public CadClienteDFController(GridControl grid) {
		this();
		this.grid = grid;
	}

	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		ClienteModel vo = (ClienteModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosCliente(vo);
	}
	
	@Override
	public boolean beforeSaveDataInEdit(Form form) {
		ClienteModel vo = (ClienteModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosCliente(vo);
	}

	public Response loadData(Class valueObjectClass) {
		try {
			if (cliente == null)
				return new ErrorResponse("Nenhum cliente foi selecionado");
			return new VOResponse(cliente);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	@Override
	public boolean validateControl(String attributeName, Object oldValue,
			Object newValue) {
		if (attributeName.equals("cnpjcpf")) {
			if (((String) newValue).equals(""))
				return true; // s� valida se existir algum valor
			Validator<String> validator;
			boolean pJuridica = false;
			try {
				// Valida CNPJ
				ResourceBundle resourceBundle = ResourceBundle.getBundle(
						"PlayIVValidationMessages", new Locale("pt", "BR"));
				MessageProducer messageProducer = new ResourceBundleMessageProducer(
						resourceBundle);
				boolean isFormatted = false;
				pJuridica = this.frame.getClientePanel().isPJuridica();
				if (pJuridica) {
					String cnpj = (String) newValue;
					validator = new CNPJValidator(messageProducer, isFormatted);
					validator.assertValid(cnpj);
				} else { // Valida CPF
					String cpf = (String) newValue;
					validator = new CPFValidator(messageProducer, isFormatted);
					validator.assertValid(cpf);
				}
			} catch (InvalidStateException e) {
				StringBuilder msgError = new StringBuilder("");
				for (ValidationMessage message : e.getInvalidMessages()) {
					msgError.append(message.getMessage() + "\n");
				}
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),
						"Houve um erro na valida��o do "
								+ (pJuridica ? "CNPJ" : "CPF") + " :" + "\n"
								+ msgError);
				LogFileMngr.getInstance().error(e.getMessage(), e);
				return false;
			}

		}
		return true;
	}

	@Override
	public void afterInsertData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}

	@Override
	public void afterEditData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}

	@Override
	public void afterDeleteData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadCurrentBlockOfData();
//			this.grid.reloadData();
	}

	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {

			ClienteModel vo = (ClienteModel) newPersistentObject;
			vo.setAtivo(true);
			vo.setPjuridica(this.frame.getClientePanel().isPJuridica());

			EnderecoModel endereco = vo.getEndereco();
			if(endereco.getId()!= null) {
				pdao.update(endereco);
			} else {
				pdao.save(endereco);
			}
			
//			if (!(endereco == null))
//				if (!(endereco.getCep() == null)) {
//					String baseSQL = "from Endereco in class org.callcenter.frontend.model.EnderecoModel where Endereco.cep = '"
//							+ endereco.getCep() + "'";
//					Session session = pdao.getSession(); // obtain a JDBC
//															// connection and
//															// instantiate a new
//															// Session
//					EnderecoModel findEndereco = (EnderecoModel) session
//							.createQuery(baseSQL).uniqueResult();
//					session.close();
//
//					if (findEndereco == null)
//						pdao.save(endereco);
//					else
//						pdao.update(endereco);
//				} else
//					vo.setEndereco(null);

			pdao.save(vo);

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			ClienteModel vo = (ClienteModel) persistentObject;
			vo.setAtivo(true);
			vo.setPjuridica(this.frame.getClientePanel().isPJuridica());

			EnderecoModel endereco = vo.getEndereco();
			if(endereco.getId()!= null) {
				pdao.update(endereco);
			} else {
				pdao.save(endereco);
			}
			
//			if (!(endereco == null))
//				if (!(endereco.getCep() == null)) {
//					String baseSQL = "from Endereco in class org.callcenter.frontend.model.EnderecoModel where Endereco.cep = '"
//							+ endereco.getCep() + "'";
//					Session session = pdao.getSession(); // obtain a JDBC
//															// connection and
//															// instantiate a new
//															// Session
//					EnderecoModel findEndereco = (EnderecoModel) session
//							.createQuery(baseSQL).uniqueResult();
//					session.close();
//
//					if (findEndereco == null)
//						pdao.save(endereco);
//					else
//						pdao.update(endereco);
//				} else
//					vo.setEndereco(null);

			pdao.update(vo);

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			ClienteModel vo = (ClienteModel) persistentObject;
			vo.setAtivo(false);
			pdao.update(vo);
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	public GridControl getGridFrame() {
		return this.grid;
	}

	public CadClienteDFView getFrame() {
		return this.frame;
	}

	public ClienteModel getCliente() {
		return cliente;
	}

}