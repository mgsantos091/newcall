package org.callcenter.frontend.controller;

import java.beans.PropertyVetoException;
import java.sql.Timestamp;
import java.util.Date;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.lib.ValidacaoController;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.OpeLigacoesDFView;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;

public class OpeLigacoesDFController extends FormController {

	private GridControl grid;
	private OpeLigacoesDFView frame;
	private RegistroLigacaoModel registro;
	private ClienteModel cliente;

	private PrincipalDAO pdao = PrincipalDAO.getInstance();

	public OpeLigacoesDFController(ClienteModel cliente) {
		this.cliente = cliente;
		frame = new OpeLigacoesDFView(this);
		frame.getPanel().setMode(Consts.INSERT);
	}

	public OpeLigacoesDFController(RegistroLigacaoModel registro) {
		this.registro = registro;
		frame = new OpeLigacoesDFView(this);
		frame.getPanel().setMode(Consts.EDIT);
		frame.getPanel().reload();
	}

	public OpeLigacoesDFController(GridControl gridFrame,
			RegistroLigacaoModel registro) {
		this(registro);
		this.registro = registro;
		this.grid = gridFrame;
	}

	public OpeLigacoesDFController(GridControl gridFrame,
			ClienteModel cliente) {
		this(cliente);
		this.grid = gridFrame;
	}

	public Response loadData(Class valueObjectClass) {
		try {
			if (registro == null)
				return new ErrorResponse("Nenhum registro foi selecionado");
			if (registro.getDuracao() != null) {
				long t = registro.getDuracao().getTime();
				this.frame.getJpRelogioPanel().setTime(t);
				this.frame.getJpRelogioPanel().setInicioTempo(
						registro.getInicio());
				this.frame.getJpRelogioPanel().setFimTempo(registro.getFim());
			}
			return new VOResponse(registro);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	@Override
	public boolean beforeInsertData(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		vo.setCliente(cliente);
		form.pull();
		return true;
	}

	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosLigacao(vo);
	}
	
	@Override
	public boolean beforeSaveDataInEdit(Form form) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) form.getVOModel()
				.getValueObject();
		return ValidacaoController.validaDadosLigacao(vo);
	}

	@Override
	public void afterInsertData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadData();
	}

	@Override
	public void afterEditData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadData();
	}

	@Override
	public void afterDeleteData() {
		try {
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			LogFileMngr.getInstance().error(e.getMessage(), e);
		}
		if (this.grid != null)
			this.grid.reloadData();
	}

	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {

			RegistroLigacaoModel vo = (RegistroLigacaoModel) newPersistentObject;
			vo.setAtivo(true);

			Timestamp inicio = this.frame.getJpRelogioPanel().getInicioTempo();
			Timestamp fim = this.frame.getJpRelogioPanel().getFimTempo();
			if (inicio != null && fim == null) {
				fim = new Timestamp((new Date()).getTime());
			}
			vo.setInicio(inicio);
			vo.setFim(fim);

			vo.setCliente(cliente);
			pdao.save(vo);

			return new VOResponse(vo);

		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
			pdao.update(vo);
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
			vo.setAtivo(false);
			pdao.update(vo);
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	public GridControl getGridFrame() {
		return this.grid;
	}

	public OpeLigacoesDFView getFrame() {
		return this.frame;
	}

	public RegistroLigacaoModel getCliente() {
		return registro;
	}

}