package org.callcenter.frontend.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.PrincipalDAO;
import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.callcenter.frontend.view.OpeLigacoesDFView;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;

public class OpeLigacoesGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private GridControl grid;
	private PrincipalDAO pdao = PrincipalDAO.getInstance();
	private ClienteModel cliente;

	public OpeLigacoesGFController( ) {
	}
	
	public OpeLigacoesGFController(ClienteModel cliente) {
		this();
		this.cliente = cliente;
	}
	
	public void setGrid(GridControl grid) {
		this.grid = grid;
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		RegistroLigacaoModel vo = (RegistroLigacaoModel) persistentObject;
		OpeLigacoesDFController opLigacaoController = new OpeLigacoesDFController(grid, vo);
		OpeLigacoesDFView cadClienteView = opLigacaoController.getFrame();
		MDIFrame.add(cadClienteView);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
//			if(cliente == null) return new ErrorResponse("Nenhum cliente selecionado");
			if(cliente == null) return new VOResponse();
			
			String baseSQL = "from org.callcenter.frontend.model.RegistroLigacaoModel where idcliente = '" + cliente.getId() + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "RegistroLigacaoModel",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogFileMngr.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		RegistroLigacaoModel registro = (RegistroLigacaoModel) this.grid
				.getVOListTableModel().getObjectForRow(row);
		if (!registro.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

	public GridControl getGrid( ) {
		return this.grid;
	}

	public ClienteModel getCliente() {
		return cliente;
	}

	public void setCliente(ClienteModel cliente) {
		this.cliente = cliente;
	}

}