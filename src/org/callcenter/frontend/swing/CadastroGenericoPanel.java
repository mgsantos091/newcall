package org.callcenter.frontend.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
//import java.awt.color.ColorSpace;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

public class CadastroGenericoPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	JPanel jpNomePainel;
	JPanel jpCentroPainel;
	
	public CadastroGenericoPanel(String nomePainel) {
		super.setLayout( new MigLayout( ) );
		
		jpNomePainel = new JPanel();
//		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpNomePainel.setBackground(Color.DARK_GRAY);
		
		JLabel lblTituloPainel = new JLabel(nomePainel);
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 14));
		jpNomePainel.add(lblTituloPainel);
		
		super.add(jpNomePainel, "h 25! , dock north , growx");
		
		jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
//		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setBackground(Color.WHITE);
		jpCentroPainel.setLayout(new MigLayout("",
				"[right][grow][right][grow]", ""));
		
		super.add(jpCentroPainel, "dock center , grow");
	}
	
	@Override
	public Component add(Component c) {
		return jpCentroPainel.add(c);
	}
	
	@Override
	public void add(Component comp, Object constraints) {
		jpCentroPainel.add(comp, constraints);
	}
	
	public void setCentroPainelLayout(LayoutManager lwtmngr) {
		jpCentroPainel.setLayout(lwtmngr);
	}
	
}
