/*
 * k5nCal - Java Swing Desktop Calendar App
 * Copyright (C) 2005-2007 Craig Knudsen, craig@k5n.us
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.callcenter.frontend.swing.k5n.k5ncal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.callcenter.frontend.lib.ConfGlobal;
import org.callcenter.frontend.lib.LogFileMngr;
import org.callcenter.frontend.lib.Util;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Calendar;
import org.callcenter.frontend.swing.k5n.k5ncal.data.HttpClient;
import org.callcenter.frontend.swing.k5n.k5ncal.data.HttpClientStatus;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Repository;
import org.callcenter.frontend.swing.k5n.k5ncal.data.RepositoryChangeListener;
import org.callcenter.frontend.swing.k5n.k5ncal.data.SingleEvent;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.BogusDataException;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Constants;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.DataStore;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Date;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Event;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.ICalendarParser;
import org.callcenter.frontend.swing.k5n.k5ncal.ui.CalendarPanel;
import org.callcenter.frontend.swing.k5n.k5ncal.ui.CalendarPanelSelectionListener;
import org.callcenter.frontend.swing.k5n.k5ncal.ui.EventInstance;
import org.openswing.swing.mdi.client.InternalFrame;



import org.openswing.swing.mdi.client.MDIFrame;

//import us.k5n.k5ncal.MacStuff;
import us.k5n.ui.AccordionPane;

/**
 * Main class for k5nCal application. This application makes use of the k5n
 * iCalendar library (part of Java Calendar Tools) as well as many other 3rd
 * party libraries and tools. See the README.txt file for details. Please see
 * the License.html file for licensing details.
 * 
 * @author Craig Knudsen, craig@k5n.us
 * @version $Id: Main.java,v 1.67 2008/01/19 00:50:45 cknudsen Exp $
 * 
 */
public class AgendaCFView extends InternalFrame implements Constants, ComponentListener,
    PropertyChangeListener, RepositoryChangeListener,
    CalendarPanelSelectionListener, CalendarRefresher {
	public static final String DEFAULT_DIR_NAME = ConfGlobal.getInstance().getDEFAULT_CALENDARIO_PATH();
	public String version = null;;
	public static final String CALENDARS_XML_FILE = "calendars.xml";
	static final String APP_ICON = "images/k5n/k5nCal-128x128.png";
	static final String APP_URL = "http://www.k5n.us/k5ncal.php";
	static final String DONATE_URL = "https://sourceforge.net/donate/index.php?group_id=195315";
	static final String REPORT_BUG_URL = "https://sourceforge.net/tracker/?group_id=195315&atid=952950";
	static final String REQUEST_FEATURE_URL = "https://sourceforge.net/tracker/?group_id=195315&atid=952953";
	static final String SUPPORT_REQUEST_URL = "https://sourceforge.net/tracker/?group_id=195315&atid=952951";
	static final String LICENSE_FILE = "License.html";
	static ClassLoader cl = null;
	EventViewPanel eventViewPanel;
	JButton newButton, editButton, deleteButton, largerButton, smallerButton;
	JLabel messageArea;
	Repository dataRepository;
	CalendarPanel calendarPanel;
	JSplitPane horizontalSplit = null, leftVerticalSplit = null;
	AccordionPane ap;
	JListWithCheckBoxes calendarJList;
	JList categoryJList;
	String searchText = null;
	private static File lastExportDirectory = null;
	AppPreferences prefs;
	File dataDir = null;
	static final String MENU_CALENDAR_NEW = "Adicionar Calend�rio";
	static final String MENU_CALENDAR_EDIT = "Editar Calend�rio";
	static final String MENU_CALENDAR_REFRESH = "Refresh Calend�rio";
	static final String MENU_CALENDAR_DELETE = "Deletar Calend�rio";
	static final String MENU_CALENDAR_ADD_EVENT = "Adicionar Evento";
	static final String MENU_CALENDAR_VIEW_ERRORS = "Visualizar Erros/Avisos";
	static final String ADD_EVENT_LABEL = "Novo";
	static final String EDIT_EVENT_LABEL = "Editar";
	static final String DELETE_EVENT_LABEL = "Deletar";
	static final String LARGER_FONT_LABEL = "Maior";
	static final String SMALLER_FONT_LABEL = "Menor";
	private boolean fontsInitialized = false;
	private boolean isMac = false;

	public AgendaCFView() {
//		super ( "k5nCal" );
		setTitle("FRONT END - AGENDA");
		this.isMac = ( System.getProperty ( "mrj.version" ) != null );

		// Get version from ChangeLog
		this.getVersionFromChangeLog ();

		prefs = AppPreferences.getInstance ();
		// prefs.clearAll ();

		setInitialLAF ();

		setSize ( prefs.getMainWindowWidth (), prefs.getMainWindowHeight () );
		this.setLocation ( prefs.getMainWindowX (), prefs.getMainWindowY () );

		setDefaultCloseOperation ( JFrame.EXIT_ON_CLOSE );
		Container contentPane = getContentPane ();

		// Load data
		dataDir = Util.getDiretorioDados ();
		dataRepository = Util.getRepositorioPadrao();
		// Ask to be notified when the repository changes (user adds/edits
		// an entry)
		dataRepository.addChangeListener ( this );
		
		saveCalendars ( dataDir );
			
//		for(Event e : dataRepository.getAllEntries()) {
//			System.out.println(e.getStartDate().getValue() + " " + e.getSummary().getValue() + " " + e.getNewCallData().getValue());
//		}

		contentPane.setLayout ( new BorderLayout () );

		// Add message/status bar at bottom
		JPanel messagePanel = new JPanel ();
		messagePanel.setLayout ( new BorderLayout () );
		messagePanel.setBorder ( BorderFactory.createEmptyBorder ( 2, 4, 2, 4 ) );
		messageArea = new JLabel ( "Bem vindo ao NewCALL - Agenda..." );
		messagePanel.add ( messageArea, BorderLayout.CENTER );
		contentPane.add ( messagePanel, BorderLayout.SOUTH );

		ap = new AccordionPane();
		ap.addPanel("Calend�rios",
				createCalendarSelectionPanel(dataRepository.getCalendars()));
		ap.addPanel("Categorias",
				createCategorySelectionPanel(dataRepository.getCategories()));
		ap.setTooltipTextAt(0, "Gerenciar Calend�rios");
		ap.setTooltipTextAt(1, "Filtrar eventos por categoria");

		eventViewPanel = new EventViewPanel ();
		eventViewPanel.setBorder ( BorderFactory
			.createTitledBorder ( "Detalhes do Evento" ) );
		leftVerticalSplit = new JSplitPane ( JSplitPane.VERTICAL_SPLIT, ap,
		    eventViewPanel );
		leftVerticalSplit.setOneTouchExpandable ( true );
		leftVerticalSplit.setDividerLocation ( prefs
		    .getMainWindowLeftVerticalSplitPosition () );
		leftVerticalSplit.addPropertyChangeListener ( this );

		JPanel rightPanel = new JPanel ();
		rightPanel.setLayout ( new BorderLayout () );
		rightPanel.add ( createToolBar (), BorderLayout.NORTH );

		calendarPanel = new MyCalendarPanel ( dataRepository );
		calendarPanel.addSelectionListener ( this );
		calendarPanel.setShowTime ( prefs.getDisplayHourInMonthView () );
//		calendarPanel.setLocale(getDefaultLocale());
		rightPanel.add ( calendarPanel, BorderLayout.CENTER );

		horizontalSplit = new JSplitPane ( JSplitPane.HORIZONTAL_SPLIT,
		    leftVerticalSplit, rightPanel );
		horizontalSplit.setOneTouchExpandable ( true );
		horizontalSplit.setDividerLocation ( prefs
		    .getMainWindowHorizontalSplitPosition () );
		horizontalSplit.addPropertyChangeListener ( this );

		this.add ( horizontalSplit, BorderLayout.CENTER );

		this.addComponentListener ( this );
		updateToolbar ();
		this.setVisible ( true );

		// Start a thread to update the remote calendars as needed.
		RemoteCalendarUpdater updater = new RemoteCalendarUpdater (
		    this.dataRepository, this );
		updater.start ();
		
		setUniqueInstance(true);
	}

	public void paint ( Graphics g ) {
		if ( !this.fontsInitialized ) {
			this.fontsInitialized = true;
			Font currentFont = g.getFont ();
			Font newFont = new Font ( currentFont.getFamily (), currentFont
			    .getStyle (), currentFont.getSize () + prefs.getDisplayFontSize () );
			eventViewPanel.setAllFonts ( newFont );
			calendarPanel.setFont ( newFont );
		}
		super.paint ( g );
	}

	public void saveCalendars ( File dir ) {
		File f = new File ( dir, CALENDARS_XML_FILE );
		try {
			Calendar.writeCalendars ( f, dataRepository.getCalendars () );
		} catch ( IOException e1 ) {
			this.showError ( "Erro ao salvar os dados do calend�rio" + ":\n" + e1.getMessage () );
			e1.printStackTrace ();
		}
	}

	public void setMessage ( String msg ) {
		this.messageArea.setText ( msg );
	}

	/**
	 * Quit the k5nCal application (and save the current calendar states).
	 */
//	public void quit () {
//		saveCalendars ( getDataDirectory () );
//		System.exit ( 0 );
//	}

	/**
	 * Show the application preferences dialog window.
	 */
	public void showPreferences () {
		// Note: we can re-use this same JDialog window. However, the hidden window
		// will not receive the L&F updates while the app is running. So, we just
		// create a new one to avoid this issue.
		new PreferencesWindow ( dataRepository );
	}

	/**
	 * Show the k5nCal About dialog.
	 */
//	public void showAbout () {
//		// Get application icon
//		URL url = getResource ( APP_ICON );
//		ImageIcon icon = new ImageIcon ( url, "k5nCal" );
//		// Get java version
//		String javaVersion = System.getProperty ( "java.version" );
//		if ( javaVersion == null )
//			javaVersion = "Unknown";
//		JOptionPane.showMessageDialog ( parent, "k5nCal "
//		    + ( version == null ? "Unknown Version" : version ) + "\n\n"
//		    + "Java Version" + ": " + javaVersion + "\n\n" + "Developed by k5n.us"
//		    + "\n\nhttp://www.k5n.us", "About k5nCal",
//		    JOptionPane.INFORMATION_MESSAGE, icon );
//	}

	JToolBar createToolBar () {
		JToolBar toolbar = new JToolBar ();
		newButton = makeNavigationButton ( "k5n/New24.gif", "new", "Adicionar novo evento",
			    ADD_EVENT_LABEL );
		newButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				// Make sure there is at least one local calendar.
				boolean foundLocal = false;
				for ( int i = 0; i < dataRepository.getCalendars ().size (); i++ ) {
					Calendar c = dataRepository.getCalendars ().elementAt ( i );
					if ( c.getType () == Calendar.LOCAL_CALENDAR )
						foundLocal = true;
				}
				if ( !foundLocal ) {
					showError ( "Voc� deve criar um novo calend�rio local para adicionar um novo evento." );
				} else {
					// See if they have selected a local calendar from the
					// calendar list
					Calendar selectedCalendar = null;
					int selCalInd = calendarJList.getSelectedIndex ();
					if ( selCalInd >= 0 ) {
						selectedCalendar = dataRepository.getCalendars ().elementAt (
						    selCalInd );
						if ( selectedCalendar.getType () != Calendar.LOCAL_CALENDAR )
							selectedCalendar = null; // don't allow adding to
						// remote cals
					}
					Date now = Date.getCurrentDateTime ( "DTSTART" );
					now.setMinute ( 0 );
//					new EditEventWindow ( parent, dataRepository, now, selectedCalendar );
					new EditEventWindow ( dataRepository, now, selectedCalendar );
				}
			}
		} );
		toolbar.add ( newButton );

		editButton = makeNavigationButton ( "k5n/Edit24.gif", "edit",
			"Editar o evento selecionado", EDIT_EVENT_LABEL );
		toolbar.add ( editButton );
		editButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				// Get selected item and open edit window
				EventInstance eventInstance = calendarPanel.getSelectedEvent ();
				if ( eventInstance != null ) {
					// NOTE: edit window does not yet support complicated
					// recurrence
					// rules.
					SingleEvent se = (SingleEvent) eventInstance;
					if ( se.getEvent ().getRrule () != null ) {
//						new EditEventWindow ( parent, dataRepository, se.getEvent (), se
						new EditEventWindow ( dataRepository, se.getEvent (), se
						    .getCalendar () );
					} else {
//						new EditEventWindow ( parent, dataRepository, se.getEvent (), se
						new EditEventWindow ( dataRepository, se.getEvent (), se
						    .getCalendar () );
					}
				}
			}
		} );

		deleteButton = makeNavigationButton ( "k5n/Delete24.gif", "delete",
			"Deletar o evento selecionado", DELETE_EVENT_LABEL );
		toolbar.add ( deleteButton );
		deleteButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				// Get selected item and open edit window
				EventInstance eventInstance = calendarPanel.getSelectedEvent ();
				if ( eventInstance != null ) {
					SingleEvent se = (SingleEvent) eventInstance;
					if ( se.getEvent ().getRrule () != null ) {
						// TODO: support deleting single occurrence, which will
						// add an
						// exception to the RRULE in the event.
//						if ( JOptionPane.showConfirmDialog ( parent,
						if ( JOptionPane.showConfirmDialog ( null,
							"Voc� tem certeza que quer deletar todas as ocorr�ncias do\n"
							        + "evento com ocorr�ncias repetidas registradas?" + "\n\n" + se.getTitle ()
							        + "\n\n" + "Isto vai deletar TODOS os eventos desta s�rie.",
							    "Confirmar", JOptionPane.YES_NO_OPTION ) == 0 ) {
							try {
								dataRepository.deleteEvent ( se.getCalendar (), se.getEvent () );
							} catch ( IOException e1 ) {
								showError ( "Erro ao deletar." );
								e1.printStackTrace ();
							}
						}
					} else {
//						if ( JOptionPane.showConfirmDialog ( parent,
						if ( JOptionPane.showConfirmDialog ( null,
							    "Voc� tem certeza que quer deletar o evento?"
							        + "\n\n" + se.getTitle (), "Confirmar",
						    JOptionPane.YES_NO_OPTION ) == 0 ) {
							try {
								dataRepository.deleteEvent ( se.getCalendar (), se.getEvent () );
							} catch ( IOException e1 ) {
								showError ( "Erro ao deletar." );
								e1.printStackTrace ();
							}
						}
					}
				}
			}
		} );

		toolbar.addSeparator ();

		largerButton = makeNavigationButton ( "k5n/LargerFont24.png",
				"Aumentar o tamanho da fonte", "Aumentar o tamanho da fonte", LARGER_FONT_LABEL );
		toolbar.add ( largerButton );
		largerButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				int oldOffset = prefs.getDisplayFontSize ();
				if ( oldOffset < 4 )
					prefs.setDisplayFontSize ( oldOffset + 2 );
				displaySettingsChanged ();
			}
		} );

		smallerButton = makeNavigationButton ( "k5n/SmallerFont24.png",
				"Diminuir o tamanho da fonte", "Diminuir o tamanho da fonte", SMALLER_FONT_LABEL );
		toolbar.add ( smallerButton );
		smallerButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				int oldOffset = prefs.getDisplayFontSize ();
				if ( oldOffset > -4 )
					prefs.setDisplayFontSize ( oldOffset - 2 );
				displaySettingsChanged ();
			}
		} );

		return toolbar;
	}

	/**
	 * Update the toolbar. You can only modify local calendars (for now, at
	 * least), so unless an event from a local calendar was selected, the edit and
	 * delete buttons should be disabled.
	 */
	void updateToolbar () {
		boolean canEdit = false;
		boolean selected = false;
		EventInstance eventInstance = calendarPanel.getSelectedEvent ();
		selected = ( eventInstance != null );
		if ( selected && eventInstance instanceof SingleEvent ) {
			SingleEvent se = (SingleEvent) eventInstance;
			canEdit = ( se.getCalendar ().getType () == Calendar.LOCAL_CALENDAR )
			    || ( se.getCalendar ().getType () == Calendar.REMOTE_ICAL_CALENDAR && se
			        .getCalendar ().getCanWrite () );
		}
		editButton.setEnabled ( selected && canEdit );
		deleteButton.setEnabled ( selected && canEdit );
		smallerButton.setEnabled ( prefs.getDisplayFontSize () > -4 );
		largerButton.setEnabled ( prefs.getDisplayFontSize () < 4 );
		// Show text?
		if ( prefs.getToolbarIconText () ) {
			this.newButton.setText ( ADD_EVENT_LABEL );
			this.editButton.setText ( EDIT_EVENT_LABEL );
			this.deleteButton.setText ( DELETE_EVENT_LABEL );
			this.largerButton.setText ( LARGER_FONT_LABEL );
			this.smallerButton.setText ( SMALLER_FONT_LABEL );
		} else {
			this.newButton.setText ( null );
			this.editButton.setText ( null );
			this.deleteButton.setText ( null );
			this.largerButton.setText ( null );
			this.smallerButton.setText ( null );
		}
	}

	/**
	 * Create the file selection area on the top side of the window. This will
	 * include a split pane where the left will allow navigation and selection of
	 * dates and the right will allow the selection of a specific entry.
	 * 
	 * @return
	 */
	protected JPanel createCalendarSelectionPanel ( Vector calendars ) {
		JPanel topPanel = new JPanel ();
		topPanel.setLayout ( new BorderLayout () );

		this.calendarJList = new JListWithCheckBoxes ( new Vector<Object> () );
		updateCalendarJList ();
		this.calendarJList
		    .addListItemChangeListener ( new ListItemChangeListener () {
			    public void itemSelected ( int ind ) {
				    dataRepository.getCalendars ().elementAt ( ind )
				        .setSelected ( true );
				    handleCalendarFilterSelection ();
			    }

			    public void itemUnselected ( int ind ) {
				    dataRepository.getCalendars ().elementAt ( ind )
				        .setSelected ( true );
				    handleCalendarFilterSelection ();
			    }

			    public Vector<ListItemMenuItem> getMenuChoicesForIndex ( int ind ) {
				    Vector<ListItemMenuItem> ret = new Vector<ListItemMenuItem> ();
				    Calendar c = dataRepository.getCalendars ().elementAt ( ind );
				    ret.addElement ( new ListItemMenuItem ( MENU_CALENDAR_NEW ) );
				    ret.addElement ( new ListItemMenuItem ( MENU_CALENDAR_EDIT ) );
				    ret.addElement ( new ListItemMenuItem ( MENU_CALENDAR_REFRESH, c
				        .getType () != Calendar.LOCAL_CALENDAR ) );
				    ret.addElement ( new ListItemMenuItem ( MENU_CALENDAR_DELETE ) );
				    ret.addElement ( new ListItemMenuItem ( MENU_CALENDAR_ADD_EVENT, c
				        .getType () == Calendar.LOCAL_CALENDAR ) );
				    if ( c.getType () == Calendar.REMOTE_ICAL_CALENDAR ) {
					    // Does this calendar have errors?
					    // TODO: implement error viewer...
					    // Vector<ParseError> errors =
					    // dataRepository.getErrorsAt ( ind );
					    // if ( errors != null && errors.size () > 0 )
					    // ret.addElement ( MENU_CALENDAR_VIEW_ERRORS + " "
					    // + "("
					    // + errors.size () + ")" );
				    }
				    return ret;
			    }

			    public void menuChoice ( int ind, String actionCommand ) {
				    Calendar c = dataRepository.getCalendars ().elementAt ( ind );
				    if ( MENU_CALENDAR_NEW.equals ( actionCommand ) ) {
				    	editLocalCalendar ( null );
				    }else if ( MENU_CALENDAR_EDIT.equals ( actionCommand ) ) {
					    editCalendar ( c );
				    } else if ( MENU_CALENDAR_REFRESH.equals ( actionCommand ) ) {
					    if ( c.getType () == Calendar.LOCAL_CALENDAR ) {
					    	showError ( "Voc� pode apenas atualizar\ncalend�rio remoto/inscrito." );
					    } else {
						    refreshCalendar ( c );
					    }
				    } else if ( MENU_CALENDAR_DELETE.equals ( actionCommand ) ) {
//					    if ( JOptionPane.showConfirmDialog ( parent,
				    	if ( JOptionPane.showConfirmDialog ( null,
					    		"Voc� tem certeza que quer deletar o calend�rio a seguir?"
							            + "\n\n" + c.toString (), "Confirmar",
					        JOptionPane.YES_NO_OPTION ) == 0 ) {
						    deleteCalendar ( c );
					    }
				    } else if ( MENU_CALENDAR_ADD_EVENT.equals ( actionCommand ) ) {
					    Date now = Date.getCurrentDateTime ( "DTSTART" );
					    now.setMinute ( 0 );
//					    new EditEventWindow ( parent, dataRepository, now, c );
					    new EditEventWindow ( dataRepository, now, c );
				    } else {
					    System.err.println ( "Unknown menu command: " + actionCommand );
				    }
			    }
		    } );

		for ( int i = 0; i < dataRepository.getCalendars ().size (); i++ ) {
			Calendar c = dataRepository.getCalendars ().elementAt ( i );
			ListItem item = this.calendarJList.getListItemAt ( i );
			item
			    .setState ( c.isSelected () ? ListItem.STATE_YES : ListItem.STATE_OFF );
		}

		topPanel
		    .add ( new MyScrollPane ( this.calendarJList ), BorderLayout.CENTER );

		return topPanel;
	}

	// Handle user selecting a calendar checkbox to display/hide a calendar
	void handleCalendarFilterSelection () {
		// Repaint the calendar view, which will reload the data
		for ( int i = 0; i < dataRepository.getCalendars ().size (); i++ ) {
			dataRepository
			    .getCalendars ()
			    .elementAt ( i )
			    .setSelected (
			        this.calendarJList.getListItemAt ( i ).getState () == ListItem.STATE_YES );
		}
		this.calendarPanel.clearSelection ();
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
	}

	public void editCalendar ( Calendar c ) {
		if ( c.getType () == Calendar.LOCAL_CALENDAR ) {
			editLocalCalendar ( c );
		} else {
//			new EditRemoteCalendarWindow ( parent, dataRepository, c,
			new EditRemoteCalendarWindow ( dataRepository, c,
			    Util.getDiretorioDados () );
		}
	}

	/**
	 * Refresh the specified calendar by reloading it from its URL. Because this
	 * is likely to take a second or more in ideal circumstances (and much longer
	 * in many cases), we will use the SwingWorker class to execute this in a
	 * separate thread so we don't lock up the UI.
	 * 
	 * @param cal
	 *          The Calendar to refresh
	 */
	public void refreshCalendar ( final Calendar cal ) {
		// Before we get started, update the status bar to indicate we are
		// loading
		// the calendar.
		showStatusMessage ( "Atualizando calend�rio" + ": " + cal.getName () );

		SwingWorker refreshWorker = new SwingWorker () {
			private String error = null;
			private String statusMsg = null;
			private File outputFile = null;

			public Object construct () {
				// Execute time-consuming task...
				// For now, we only support HTTP/HTTPS since 99.99% of all users
				// will
				// use it
				// instead of something like FTP.
				outputFile = new File ( dataDir, cal.getFilename () + ".new" );
				String username = null, password = null;
				if ( cal.getAuthType () == Calendar.AUTH_BASIC ) {
					username = cal.getAuthUsername ();
					password = cal.getAuthPassword ();
				}
				HttpClientStatus result = HttpClient.getRemoteCalendar ( cal.getUrl (),
				    username, password, outputFile );
				// We're not supposed to make UI calls from this thread. So,
				// when
				// we get an error, save it in the error variable for use in the
				// finished method.
				// TODO: implement a way to show these errors to the user.
				switch (result.getStatus()) {
				case HttpClientStatus.HTTP_STATUS_SUCCESS:
					statusMsg = "Calend�rio atualizado com sucesso" + ": "
							+ cal.getName();
					break;
				case HttpClientStatus.HTTP_STATUS_AUTH_REQUIRED:
					error = "Autoriza��o requerida.\nPor favor providenciar o login\n"
							+ "e a senha."
							+ "\n\n"
							+ "Calendario"
							+ ": "
							+ cal.getName()
							+ "\n"
							+ "URL"
							+ ": "
							+ cal.getUrl();
					return null;
				case HttpClientStatus.HTTP_STATUS_NOT_FOUND:
					error = "URL do calend�rio inv�lido (n�o encontrado).\n\nSem resposta do servidor"
							+ ": " + result.getMessage();
					return null;
				default:
				case HttpClientStatus.HTTP_STATUS_OTHER_ERROR:
					error = "Erro ao realizar o donwload do calend�rio.\n\nResposta do servidor: "
							+ result.getMessage()
							+ "\n\n"
							+ "Calend�rio"
							+ ": "
							+ cal.getName()
							+ "\n"
							+ "URL"
							+ ": "
							+ cal.getUrl();
					return null;
				}
				return null;
			}

			public void finished () {
				// Update UI
				if ( error != null )
					showError ( error );
				if ( this.statusMsg != null )
					showStatusMessage ( statusMsg );
				if ( error == null ) {
					// TODO: validate what we downloaded was ICS data rather
					// than an HTML
					// page
					// Rename file from ".ics.new" to ".ics"
					File file = new File ( dataDir, cal.getFilename () );
					// Delete old file first since renameTo may file if file
					// already
					// exists
					file.delete ();
					// Now rename
					if ( !outputFile.renameTo ( file ) ) {
						// Error renaming
						showError ( "Erro ao renomear o arquivo" );
					} else {
						// System.out.println ( "Renamed " + outputFile + " to "
						// + file );
						// If no error, then save calendar update
						cal.setLastUpdatedAsNow ();
						saveCalendars ( dataDir );
						dataRepository.updateCalendar ( Util.getDiretorioDados (), cal );
					}
				}
			}
		};
		refreshWorker.start ();
	}

	public void deleteCalendar ( Calendar c ) {
		boolean found = false;
		for ( int i = 0; i < dataRepository.getCalendars ().size () && !found; i++ ) {
			Calendar c1 = (Calendar) dataRepository.getCalendars ().elementAt ( i );
			if ( c1.equals ( c ) ) {
				dataRepository.removeCalendar ( Util.getDiretorioDados (), c );
				found = true;
			}
		}
		if ( found ) {
			updateCalendarJList ();
			updateCategoryJList ();
			this.dataRepository.rebuild ();
			this.calendarPanel.repaint ();
			saveCalendars ( Util.getDiretorioDados () );
		} else {
			System.err.println ( "deleteCalendar: could not find calendar!" );
		}
	}

	/**
	 * Update the list of Calendars shown to the user
	 */
	public void updateCalendarJList () {
		this.calendarJList.setChoices ( dataRepository.getCalendars () );
		for ( int i = 0; i < dataRepository.getCalendars ().size (); i++ ) {
			Calendar cal = (Calendar) dataRepository.getCalendars ().elementAt ( i );
			ListItem item = this.calendarJList.getListItemAt ( i );
			item.setBackground ( cal.getBackgroundColor () );
			item.setForeground ( cal.getForegroundColor () );
			item.setState ( cal.isSelected () ? ListItem.STATE_YES
			    : ListItem.STATE_OFF );
		}
		this.calendarJList.validate ();
	}

	protected JPanel createCategorySelectionPanel ( Vector categories ) {
		JPanel panel = new JPanel ();
		panel.setLayout ( new BorderLayout () );
		this.categoryJList = new JList ( categories );
		final JList list = this.categoryJList;
		updateCategoryJList ();

		JPanel buttonPanel = new JPanel ();
		buttonPanel.setLayout ( new FlowLayout () );
		JButton allButton = new JButton ( "Selecionar Tudo" );
		allButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent e ) {
				int len = list.getModel ().getSize ();
				if ( len > 0 )
					list.getSelectionModel ().setSelectionInterval ( 0, len - 1 );
			}
		} );
		buttonPanel.add ( allButton );
		JButton noneButton = new JButton ( "Limpar" );
		noneButton.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent e ) {
				list.clearSelection ();
			}
		} );
		buttonPanel.add ( noneButton );
		panel.add ( buttonPanel, BorderLayout.SOUTH );

		// Add handler for when user changes category selectios
		list.addListSelectionListener ( new ListSelectionListener () {
			public void valueChanged ( ListSelectionEvent e ) {
				handleCategoryFilterSelection ();
			}
		} );

		JScrollPane sp = new MyScrollPane ( this.categoryJList );
		panel.add ( sp, BorderLayout.CENTER );
		return panel;
	}

	// Handle user selecting a category to filter by
	void handleCategoryFilterSelection () {
		boolean uncategorized = this.categoryJList.getSelectionModel ()
		    .isSelectedIndex ( 0 );
		int[] selected = this.categoryJList.getSelectedIndices ();
		Vector selectedCats = new Vector<String> ();
		for ( int i = 0; i < selected.length; i++ ) {
			if ( selected[i] > 0 ) {
				String cat = (String) this.categoryJList.getModel ().getElementAt (
				    selected[i] );
				selectedCats.addElement ( cat );
			}
		}
		if ( selected == null || selected.length == 0 ) {
			ap.setTitleAt ( 1, "Categorias" );
			this.dataRepository.clearCategoryFilter ();
		} else {
			ap.setTitleAt ( 1, "Categorias" + " [" + selected.length + "]" );
			this.dataRepository.setCategoryFilter ( uncategorized, selectedCats );
		}
		this.calendarPanel.clearSelection ();
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
	}

	/**
	 * Update the list of Calendars shown to the user
	 */
	public void updateCategoryJList () {
		// Get current selections so we can preserve
		Object[] oldSelections = this.categoryJList.getSelectedValues ();
		HashMap<String, String> old = new HashMap<String, String> ();
		for ( int i = 0; i < oldSelections.length; i++ ) {
			old.put ( oldSelections[i].toString (), oldSelections[i].toString () );
		}
		Vector cats = dataRepository.getCategories ();
		// Sort categories alphabetically
		Collections.sort ( cats );
		cats.insertElementAt ( "Sem categoria", 0 );
		this.categoryJList.setListData ( cats );
		int[] newSelections = new int[oldSelections.length];
		int j = 0;
		for ( int i = 0; i < dataRepository.getCategories ().size (); i++ ) {
			String cat = (String) dataRepository.getCategories ().elementAt ( i );
			if ( old.containsKey ( cat ) )
				newSelections[j++] = i + 1; // skip over "uncategorized"
		}
		int[] indices = new int[j];
		for ( int i = 0; i < j; i++ ) {
			indices[i] = newSelections[i];
		}
		this.categoryJList.setSelectedIndices ( indices );
		this.categoryJList.validate ();
	}

	/**
	 * Get the data directory that data files for this application will be stored
	 * in.
	 * 
	 * @return
	 */

	void showStatusMessage ( String string ) {
		this.messageArea.setText ( string );
	}

	void showMessage ( String message ) {
//		JOptionPane.showMessageDialog ( parent, message, "Informativo",
		JOptionPane.showMessageDialog ( null, message, "Informativo",
		    JOptionPane.INFORMATION_MESSAGE );
		LogFileMngr.getInstance().info(message);
	}

	void showError ( String message ) {
//		System.err.println ( "Error" + ": " + message );
//		JOptionPane.showMessageDialog ( parent, message, "Erro",
		JOptionPane.showMessageDialog ( null, message, "Erro",
		    JOptionPane.ERROR_MESSAGE );
		LogFileMngr.getInstance().error(message);
	}

	void fatalError ( String message ) {
//		System.err.println ( "Fatal error" + ": " + message );
//		JOptionPane.showMessageDialog ( parent, message, "Erro fatal",
		JOptionPane.showMessageDialog ( null, message, "Erro fatal",
		    JOptionPane.ERROR_MESSAGE );
//		System.exit ( 1 );
		LogFileMngr.getInstance().error(message);
	}

	protected JButton makeNavigationButton ( String imageName,
	    String actionCommand, String toolTipText, String altText ) {
		JButton button;
		String imgLocation = null;
		URL imageURL = null;

		// Look for the image.
		imgLocation = "images/" + imageName;
		if ( imageName != null ) {
			imageURL = getResource ( imgLocation );
		}

		if ( imageURL != null ) { // image found
			button = new JButton ( altText );
			button.setIcon ( new ImageIcon ( imageURL, altText ) );
		} else {
			// no image found
			button = new JButton ( altText );
			if ( imageName != null )
				System.err.println ( "Resource not found" + ": " + imgLocation );
		}

		button.setVerticalTextPosition ( JButton.BOTTOM );
		button.setHorizontalTextPosition ( JButton.CENTER );
		button.setActionCommand ( actionCommand );
		button.setToolTipText ( toolTipText );

		// Decrease font size by 2 if we have an icon
		if ( imageURL != null ) {
			Font f = button.getFont ();
			Font newFont = new Font ( f.getFamily (), Font.PLAIN, f.getSize () - 2 );
			button.setFont ( newFont );
		}

		return button;
	}

	/**
	 * Set the Look and Feel to be Windows.
	 */
	public void setInitialLAF () {
		String laf = prefs.getAppearanceLookAndFeel ();
		try {
			if ( laf != null )
				UIManager.setLookAndFeel ( laf );
		} catch ( Exception e ) {
			System.out.println ( "Unable to L&F " + laf + ": " + e.toString () );
		}
	}

	public void selectLookAndFeel ( Component toplevel, Frame dialogParent ) {
		LookAndFeel lafCurrent = UIManager.getLookAndFeel ();
		// System.out.println ( "Current L&F: " + lafCurrent );
		UIManager.LookAndFeelInfo[] info = UIManager.getInstalledLookAndFeels ();
		String[] choices = new String[info.length];
		int sel = 0;
		for ( int i = 0; i < info.length; i++ ) {
			System.out.println ( "  " + info[i].toString () );
			choices[i] = info[i].getClassName ();
			if ( info[i].getClassName ().equals ( lafCurrent.getClass ().getName () ) )
				sel = i;
		}
		Object uiSelection = JOptionPane.showInputDialog ( dialogParent,
		    "Select Look and Feel", "Look and Feel",
		    JOptionPane.INFORMATION_MESSAGE, null, choices, choices[sel] );
		UIManager.LookAndFeelInfo selectedLAFInfo = null;
		for ( int i = 0; i < info.length; i++ ) {
			if ( uiSelection.equals ( choices[i] ) )
				selectedLAFInfo = info[i];
		}
		if ( selectedLAFInfo != null ) {
			try {
				// System.out.println ( "Changing L&F: " + selectedLAFInfo );
				UIManager.setLookAndFeel ( selectedLAFInfo.getClassName () );
				// SwingUtilities.updateComponentTreeUI ( parent );
				// parent.pack ();
			} catch ( Exception e ) {
				System.err.println ( "Unabled to load L&F: " + e.toString () );
			}
		} else {
			System.err.println ( "No L&F selected" );
		}
	}

	private Color getForegroundColorForBackground ( Color bg ) {
		Color ret = Color.white;
		if ( bg.getRed () > 128 && bg.getGreen () > 128 && bg.getRed () > 128 )
			ret = Color.black;
		return ret;
	}

	protected void editLocalCalendar ( final Calendar c ) {
//		final JDialog addLocal = new JDialog ( this );
		final InternalFrame addLocal = new InternalFrame();
//		addLocal.setParentFrame(parent);
//		addLocal.setLocationRelativeTo ( null );
		final JTextField nameField = new JTextField ( 30 );
		final ColorButton colorField = new ColorButton ();
		int[] props = { 1, 2 };

		if ( c != null ) {
			nameField.setText ( c.getName () );
		}

		addLocal.setTitle ( c != null ? "Editar Calend�rio Local"
			    : "Adicionar Calend�rio Local" );
		addLocal.setModal ( true );
		Container content = addLocal.getContentPane ();
		content.setLayout ( new BorderLayout () );
		JPanel buttonPanel = new JPanel ();
		buttonPanel.setLayout ( new FlowLayout () );
		JButton cancel = new JButton ( "Cancelar" );
		cancel.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				addLocal.dispose ();
			}
		} );
		buttonPanel.add ( cancel );
		JButton ok = new JButton ( c == null ? "Adicionar" : "Salvar" );
		ok.addActionListener ( new ActionListener () {
			public void actionPerformed ( ActionEvent event ) {
				try {
					String name = nameField.getText ();
					if ( name == null || name.trim ().length () == 0 ) {
						showError ( "� necess�rio que o calend�rio possua um nome" );
						return;
					}
					Color color = colorField.getSelectedColor ();
					showStatusMessage ( "Criando calend�rio..." );
					Calendar cal = null;
					if ( c == null ) {
						cal = new Calendar ( Util.getDiretorioDados (), name );
					} else {
						cal = c;
					}
					cal.setBackgroundColor ( color );
					cal.setBorderColor ( getForegroundColorForBackground ( color ) );
					cal.setForegroundColor ( getForegroundColorForBackground ( color ) );
					cal.setLastUpdatedAsNow ();
					File file = new File ( Util.getDiretorioDados (), cal.getFilename () );
					if ( c == null ) {
						// Create empty iCalendar file
						FileWriter writer = new FileWriter ( file );
						ICalendarParser icalParser = new ICalendarParser (
						    ICalendarParser.PARSE_STRICT );
						icalParser.toICalendar ();
						writer.write ( icalParser.toICalendar () );
						writer.close ();
					}
					if ( c == null ) {
						showStatusMessage ( "Novo calend�rio adicionado" + ": " + name );
						dataRepository.addCalendar ( Util.getDiretorioDados (), cal, false );
						// This will call us back with calendarAdded (below)
					} else {
						// updating calendar...
						dataRepository.updateCalendar ( Util.getDiretorioDados (), cal );
						showStatusMessage ( "Calend�rio local atualizado" + ": " + name );
					}
					// System.out.println ( "Created cal file: " + file );
				} catch ( Exception e1 ) {
					showError ( "Erro no processo de cria��o do calend�rio" + ": " + e1.getMessage () );
					return;
				}
				addLocal.dispose ();
			}
		} );
		buttonPanel.add ( ok );
		content.add ( buttonPanel, BorderLayout.SOUTH );

		JPanel main = new JPanel ();
		main.setBorder ( BorderFactory
				.createTitledBorder ( c == null ? "Novo Calend�rio Local"
				        : "Editar Calend�rio Local" ) );
		main.setLayout ( new GridLayout ( 2, 1 ) );
		JPanel namePanel = new JPanel ();
		namePanel.setLayout ( new ProportionalLayout ( props,
		    ProportionalLayout.HORIZONTAL_LAYOUT ) );
		namePanel.add ( new JLabel ( "Nome do calend�rio" + ": " ) );
		namePanel.add ( nameField );
		main.add ( namePanel );

		JPanel colorPanel = new JPanel ();
		colorPanel.setLayout ( new ProportionalLayout ( props,
		    ProportionalLayout.HORIZONTAL_LAYOUT ) );
		colorPanel.add ( new JLabel ( "Cor de fundo" + ": " ) );
		JPanel colorSub = new JPanel ();
		colorSub.setLayout ( new BorderLayout () );
		colorField.setSelectedColor ( c == null ? Color.blue : c
		    .getBackgroundColor () );
		colorSub.add ( colorField, BorderLayout.WEST );
		colorPanel.add ( colorSub );
		main.add ( colorPanel );

		content.add ( main, BorderLayout.CENTER );

		addLocal.setUniqueInstance(true);
		addLocal.pack ();
		MDIFrame.add(addLocal);
//		addLocal.setVisible ( true );
	}

//	protected void importCSV () {
//		new ImportDialog ( this, ImportDialog.IMPORT_CSV, getDataDirectory (),
//		    dataRepository );
//	}
//
//	protected void importICalendar () {
//		new ImportDialog ( this, ImportDialog.IMPORT_ICS, getDataDirectory (),
//		    dataRepository );
//	}

	protected void exportAll () {
		export ( "Export All", dataRepository.getAllEntries () );
	}

	protected void exportVisible () {
		export ( "Export Visible", dataRepository.getVisibleEntries () );
	}

	private void export ( String title, Vector eventEntries ) {
		JFileChooser fileChooser;
		File outFile = null;

		if ( lastExportDirectory == null )
			fileChooser = new JFileChooser ();
		else
			fileChooser = new JFileChooser ( lastExportDirectory );
		fileChooser.setFileSelectionMode ( JFileChooser.FILES_ONLY );
		fileChooser.setFileFilter ( new ICSFileChooserFilter () );
		fileChooser.setDialogTitle ( "Select Output File" );
		fileChooser.setApproveButtonText ( "Save as ICS File" );
		fileChooser
		    .setApproveButtonToolTipText ( "Export entries to iCalendar file" );
		int ret = fileChooser.showSaveDialog ( this );
		if ( ret == JFileChooser.APPROVE_OPTION ) {
			outFile = fileChooser.getSelectedFile ();
		} else {
			// Cancel
			return;
		}
		// If no file extension provided, use ".ics"
		String basename = outFile.getName ();
		if ( basename.indexOf ( '.' ) < 0 ) {
			// No filename extension provided, so add ".csv" to it
			outFile = new File ( outFile.getParent (), basename + ".ics" );
		}
		// System.out.println ( "Selected File: " + outFile.toString () );
		lastExportDirectory = outFile.getParentFile ();
		if ( outFile.exists () && !outFile.canWrite () ) {
//			JOptionPane.showMessageDialog ( parent,
			JOptionPane.showMessageDialog ( null,
			    "You do not have the proper\npermissions to write to" + ":\n\n"
			        + outFile.toString () + "\n\n" + "Please select another file.",
			    "Save Error", JOptionPane.PLAIN_MESSAGE );
			return;
		}
		if ( outFile.exists () ) {
//			if ( JOptionPane.showConfirmDialog ( parent, "Overwrite existing file?"
			if ( JOptionPane.showConfirmDialog ( null, "Overwrite existing file?"
			    + "\n\n" + outFile.toString (), "Overwrite Confirm",
			    JOptionPane.YES_NO_OPTION ) != 0 ) {
//				JOptionPane.showMessageDialog ( parent, "Export canceled.",
				JOptionPane.showMessageDialog ( null, "Export canceled.",
				    "Export canceled", JOptionPane.PLAIN_MESSAGE );
				return;
			}
		}
		try {
			PrintWriter writer = new PrintWriter ( new FileWriter ( outFile ) );
			// Now write!
			ICalendarParser p = new ICalendarParser ( PARSE_LOOSE );
			DataStore dataStore = p.getDataStoreAt ( 0 );
			for ( int i = 0; i < eventEntries.size (); i++ ) {
				Event j = (Event) eventEntries.elementAt ( i );
				dataStore.storeEvent ( j );
			}
			writer.write ( p.toICalendar () );
			writer.close ();
//			JOptionPane.showMessageDialog ( parent, "Exported to" + ":\n\n"
			JOptionPane.showMessageDialog ( null, "Exported to" + ":\n\n"
			    + outFile.toString (), "Export", JOptionPane.PLAIN_MESSAGE );
		} catch ( IOException e ) {
//			JOptionPane.showMessageDialog ( parent,
			JOptionPane.showMessageDialog ( null,
			    "An error was encountered\nwriting to the file" + ":\n\n"
			        + e.getMessage (), "Save Error", JOptionPane.PLAIN_MESSAGE );
			e.printStackTrace ();
		}
	}

	public void componentHidden ( ComponentEvent ce ) {
	}

	public void componentShown ( ComponentEvent ce ) {
	}

	// Handle moving of main window
	public void componentMoved ( ComponentEvent ce ) {
		saveWindowPreferences ();
	}

	public void componentResized ( ComponentEvent ce ) {
		saveWindowPreferences ();
	}

	public void propertyChange ( PropertyChangeEvent pce ) {
		// System.out.println ( "property Change: " + pce );
		if ( pce.getPropertyName ().equals ( JSplitPane.DIVIDER_LOCATION_PROPERTY ) ) {
			saveWindowPreferences ();
		}
	}

	/**
	 * Save current window width, height so we can restore on next run.
	 */
	public void saveWindowPreferences () {
		prefs.setMainWindowX ( this.getX () );
		prefs.setMainWindowY ( this.getY () );
		prefs.setMainWindowWidth ( this.getWidth () );
		prefs.setMainWindowHeight ( this.getHeight () );
		prefs.setMainWindowLeftVerticalSplitPosition ( leftVerticalSplit
		    .getDividerLocation () );
		prefs.setMainWindowHorizontalSplitPosition ( horizontalSplit
		    .getDividerLocation () );
	}

	public void eventAdded ( Event event ) {
		this.updateCategoryJList ();
		handleCalendarFilterSelection ();
		this.eventViewPanel.clear ();
	}

	public void eventUpdated ( Event event ) {
		this.updateCategoryJList ();
		handleCalendarFilterSelection ();
		this.eventViewPanel.clear ();
	}

	public void eventDeleted ( Event event ) {
		this.updateCategoryJList ();
		handleCalendarFilterSelection ();
		this.eventViewPanel.clear ();
	}

	public void eventSelected ( EventInstance eventInstance ) {
		SingleEvent se = (SingleEvent) eventInstance;
		Date eventDate = null;
		try {
			eventDate = new Date ( "DTSTART", eventInstance.getYear (), eventInstance
			    .getMonth (), eventInstance.getDayOfMonth () );
		} catch ( BogusDataException e1 ) {
			e1.printStackTrace ();
			return;
		}
		if ( eventInstance.hasTime () ) {
			eventDate.setDateOnly ( false );
			eventDate.setHour ( eventInstance.getHour () );
			eventDate.setMinute ( eventInstance.getMinute () );
			eventDate.setSecond ( eventInstance.getSecond () );
		} else {
			eventDate.setDateOnly ( true );
		}
		updateToolbar ();
		this.eventViewPanel.update ( eventDate, se.getEvent (), se.getCalendar () );
		// Select the calendar on the left that the selected event belongs to.
		if ( se.getCalendar () != null ) {
			int ind = -1;
			for ( int i = 0; i < this.dataRepository.getCalendars ().size ()
			    && ind < 0; i++ ) {
				Calendar c = this.dataRepository.getCalendars ().elementAt ( i );
				if ( c.equals ( se.getCalendar () ) )
					ind = i;
			}
			if ( ind >= 0 )
				this.calendarJList.setSelectedIndex ( ind );
		}
	}

	public void calendarAdded ( Calendar c ) {
		updateCalendarJList ();
		updateCategoryJList ();
		saveCalendars ( Util.getDiretorioDados () );
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
	}

	public void calendarUpdated ( Calendar c ) {
		updateCalendarJList ();
		updateCategoryJList ();
		saveCalendars ( Util.getDiretorioDados () );
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
	}

	public void calendarDeleted ( Calendar c ) {
		updateCalendarJList ();
		updateCategoryJList ();
		saveCalendars ( Util.getDiretorioDados () );
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
	}

	public void displaySettingsChanged () {
		this.calendarPanel.setShowTime ( prefs.getDisplayHourInMonthView () );
		Font oldFont = this.calendarPanel.getFont ();
		Font defaultFont = this.getFont ();
		Font newFont = new Font ( oldFont.getFamily (), oldFont.getStyle (),
		    defaultFont.getSize () + prefs.getDisplayFontSize () );
		this.calendarPanel.setFont ( newFont );
		this.dataRepository.rebuild ();
		this.calendarPanel.repaint ();
		this.eventViewPanel.setAllFonts ( newFont );
		updateToolbar ();
//		updateLookAndFeel ();
	}

//	public void updateLookAndFeel () {
//		String laf = prefs.getAppearanceLookAndFeel ();
//		try {
//			if ( laf != null ) {
//				UIManager.setLookAndFeel ( laf );
////				SwingUtilities.updateComponentTreeUI ( parent );
//			}
//		} catch ( Exception e ) {
//			System.out.println ( "Unable to L&F " + laf + ": " + e.toString () );
//		}
//	}

	public void eventDoubleClicked ( EventInstance eventInstance ) {
		if ( eventInstance != null ) {
			SingleEvent se = (SingleEvent) eventInstance;
			boolean canEdit = ( se.getCalendar ().getType () == Calendar.LOCAL_CALENDAR )
			    || ( se.getCalendar ().getType () == Calendar.REMOTE_ICAL_CALENDAR && se
			        .getCalendar ().getCanWrite () );
			if ( !canEdit ) {
				showError ( "You cannot edit events\non the following calendar" + ": "
				    + "\n\n" + se.getCalendar ().getName () );
			} else {
//				new EditEventWindow ( parent, dataRepository, se.getEvent (), se
				new EditEventWindow ( dataRepository, se.getEvent (), se
				    .getCalendar () );
			}
		}
	}

	public void dateDoubleClicked ( int year, int month, int dayOfMonth ) {
		try {
			Date d = new Date ( "DTSTART", year, month, dayOfMonth );
//			new EditEventWindow ( parent, dataRepository, d, null );
			new EditEventWindow ( dataRepository, d, null );
		} catch ( BogusDataException e1 ) {
			e1.printStackTrace ();
		}
	}

	public void eventUnselected () {
		updateToolbar ();
		this.eventViewPanel.clear ();
	}

	URL getResource ( String name ) {
		return this.getClass ().getClassLoader ().getResource ( name );
	}

	void getVersionFromChangeLog () {
		if ( this.version != null )
			return;

		URL url = getResource ( "ChangeLog" );
		if ( url == null ) {
			System.err.println ( "Error: could not find ChangeLog in your CLASSPATH" );
			return;
		}
		try {
			InputStream is = url.openStream ();
			BufferedReader reader = new BufferedReader ( new InputStreamReader ( is ) );
			while ( this.version == null ) {
				String line = reader.readLine ();
				if ( line == null )
					break; // not found
				line = line.trim ();
				if ( line.toUpperCase ().startsWith ( "VERSION" ) ) {
					String[] args = line.split ( "-" );
					this.version = args[0].trim ();
				}
			}
			is.close ();
		} catch ( IOException e1 ) {
			e1.printStackTrace ();
			this.version = "Unknown Version" + ": " + e1.getMessage ();
		}
		return;
	}

	public void viewChangeLog () {
		URL url = getResource ( "ChangeLog" );
		try {
			InputStream is = url.openStream ();
			BufferedReader reader = new BufferedReader ( new InputStreamReader ( is ) );
			String line;
			StringBuffer sb = new StringBuffer ();
			while ( ( line = reader.readLine () ) != null ) {
				sb.append ( line );
				sb.append ( "\n" );
			}
			is.close ();
			final JDialog d = new JDialog ();
			d.getContentPane ().setLayout ( new BorderLayout () );
			d.setTitle ( "ChangeLog" );
			d.setSize ( 500, 400 );
			d.setLocationByPlatform ( true );
			JPanel buttonPanel = new JPanel ();
			buttonPanel.setLayout ( new FlowLayout () );
			JButton b = new JButton ( "Close" );
			b.addActionListener ( // Anonymous class as a listener.
			    new ActionListener () {
				    public void actionPerformed ( ActionEvent e ) {
					    d.dispose ();
				    }
			    } );
			buttonPanel.add ( b );
			d.getContentPane ().add ( buttonPanel, BorderLayout.SOUTH );
			JTextArea te = new JTextArea ( sb.toString () );
			Font f = new Font ( te.getFont ().getFamily (), Font.PLAIN, 10 );
			te.setFont ( f );
			te.setEditable ( false );
			JScrollPane sp = new MyScrollPane ( te );
			sp.getVerticalScrollBar ().setValue ( 0 );
			JPanel p = new JPanel ();
			p.setLayout ( new BorderLayout () );
			p.setBorder ( BorderFactory.createTitledBorder ( "ChangeLog" ) );
			p.add ( sp, BorderLayout.CENTER );
			d.getContentPane ().add ( p, BorderLayout.CENTER );
			d.show ();
		} catch ( Exception e1 ) {
			e1.printStackTrace ();
			showMessage ( "Error" + ":\n" + e1.getMessage () );
		}
	}

	public void viewLicense () {
		URL url = getResource ( LICENSE_FILE );
		if ( url == null ) {
			System.err.println ( "Unable to find license file" + ": " + LICENSE_FILE );
			return;
		}
		try {
			final JDialog d = new JDialog ();
			d.getContentPane ().setLayout ( new BorderLayout () );
			d.setTitle ( "k5nCal License" );
			d.setSize ( 500, 400 );
			d.setLocationByPlatform ( true );
			JPanel buttonPanel = new JPanel ();
			buttonPanel.setLayout ( new FlowLayout () );
			JButton b = new JButton ( "Close" );
			b.addActionListener ( // Anonymous class as a listener.
			    new ActionListener () {
				    public void actionPerformed ( ActionEvent e ) {
					    d.dispose ();
				    }
			    } );
			buttonPanel.add ( b );
			d.getContentPane ().add ( buttonPanel, BorderLayout.SOUTH );
			HelpPanel licenseText = new HelpPanel ( url );
			d.getContentPane ().add ( licenseText, BorderLayout.CENTER );
			d.show ();
		} catch ( Exception e1 ) {
			e1.printStackTrace ();
			showMessage ( "Error" + ":\n" + e1.getMessage () );
		}
	}

	/**
	 * @param args
	 */
	public static void main ( String[] args ) {
		Vector<String> remoteNames = new Vector<String> ();
		Vector<String> remoteURLs = new Vector<String> ();

		// If running on a Mac, change some system properties.
		if ( System.getProperty ( "mrj.version" ) != null ) {
			System.setProperty ( "com.apple.mrj.application.apple.menu.about.name",
			    "k5nCal" );
			System.setProperty ( "apple.laf.useScreenMenuBar", "true" );
			System.setProperty ( "com.apple.mrj.application.growbox.intrudes",
			    "false" );
			System.setProperty ( "apple.awt.antialiasing", "true" );
		}

		// Check for command line options
		for ( int i = 0; i < args.length; i++ ) {
			if ( args[i].equals ( "-addcalendar" ) ) {
				if ( args.length >= i + 1 ) {
					String name = args[++i].trim ();
					String url = args[++i].trim ();
					if ( url.startsWith ( "http://" ) || url.startsWith ( "https://" ) ) {
						remoteNames.addElement ( name );
						remoteURLs.addElement ( url );
					} else {
						System.err.println ( "Ignoring invalid url" + ": " + url );
					}
				} else {
					System.err
					    .println ( "Error: -addcalendar requires name and URL parameter" );
					System.exit ( 1 );
				}
			} else {
				System.err.println ( "Unknown command line option" + ": " + args[i] );
			}

		}
		AgendaCFView app = new AgendaCFView ();
//		if ( System.getProperty ( "mrj.version" ) != null ) {
//			MacStuff mac = new MacStuff ( app );
//		}
		// Add calendars if not there...
		for ( int i = 0; i < remoteURLs.size (); i++ ) {
			String name = remoteNames.elementAt ( i );
			String url = remoteURLs.elementAt ( i );
			if ( app.dataRepository.hasCalendarWithURL ( url ) ) {
				System.out.println ( "Ignoring add calendar from duplicate URL" + ": "
				    + url );
			} else {
				// auto-add the calendar....
				app.addCalendarFromCommandLine ( name, url );
			}
		}
	}

	private void addCalendarFromCommandLine ( String name, String urlStr ) {
		URL url = null;
		try {
			url = new URL ( urlStr );
		} catch ( Exception e1 ) {
			showError ( "Invalid URL" + ":\n" + e1.getMessage () );
			return;
		}
		showStatusMessage ( "Downloading calendar" + ": " + name );
		final Calendar cal = new Calendar ( Util.getDiretorioDados (), name, url, 30 );
		cal.setBackgroundColor ( Color.blue );
		cal.setForegroundColor ( getForegroundColorForBackground ( Color.blue ) );
		cal.setBorderColor ( getForegroundColorForBackground ( Color.blue ) );
		cal.setLastUpdatedAsNow ();
		cal.setUrl ( url );

		SwingWorker addWorker = new SwingWorker () {
			private String error = null;
			private String statusMsg = null;

			public Object construct () {
				File file = new File ( Util.getDiretorioDados (), cal.getFilename () );
				HttpClientStatus result = HttpClient.getRemoteCalendar ( cal.getUrl (),
				    null, null, file );
				switch ( result.getStatus () ) {
					case HttpClientStatus.HTTP_STATUS_SUCCESS:
						break;
					case HttpClientStatus.HTTP_STATUS_AUTH_REQUIRED:
						showError ( "Authorization required.\nPlease provide a username\nand password." );
						return null;
					case HttpClientStatus.HTTP_STATUS_NOT_FOUND:
						showError ( "Invalid calendar URL (not found).\n\nServer response"
						    + ": " + result.getMessage () );
						return null;
					default:
					case HttpClientStatus.HTTP_STATUS_OTHER_ERROR:
						showError ( "Error downloading calendar.\n\nServer response" + ": "
						    + result.getMessage () );
						return null;
				}
				return null;
			}

			public void finished () {
				if ( error == null )
					dataRepository.addCalendar ( Util.getDiretorioDados (), cal, false );
				// Update UI
				if ( error != null )
					showError ( error );
				if ( this.statusMsg != null )
					showStatusMessage ( statusMsg );
				if ( error == null ) {
					// If no error, then save calendar update
					cal.setLastUpdatedAsNow ();
					saveCalendars ( dataDir );
					dataRepository.updateCalendar ( Util.getDiretorioDados (), cal );
				}
			}
		};
		addWorker.start ();
		// updating calendar...
		dataRepository.updateCalendar ( Util.getDiretorioDados (), cal );
		showStatusMessage ( "Calendar added" + ": " + name );
	}
}

/**
 * Create a class to use as a file filter for exporting to ics.
 */

class ICSFileChooserFilter extends javax.swing.filechooser.FileFilter {
	public boolean accept ( File f ) {
		if ( f.isDirectory () )
			return true;
		String name = f.getName ();
		if ( name.toLowerCase ().endsWith ( ".ics" ) )
			return true;
		return false;
	}

	public String getDescription () {
		return "*.ics (iCalendar Files)";
	}
}
