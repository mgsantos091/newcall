package org.callcenter.frontend.lib;

import java.awt.Color;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Repository;
import org.callcenter.frontend.swing.k5n.k5ncal.ical.Event;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.miscellaneous.client.AlertWindow;

public class AvisoAgendaThread implements Runnable {

	public Thread t;
	private String threadName = "AVISO AGENDA";
	
	private Repository repoDadosAgenda = Util.getRepositorioPadrao();
	
	private ArrayList<Event> eventosDaAgenda = new ArrayList<Event>();
	private ArrayList<Event> mostrarEventosAgenda = new ArrayList<Event>();
	
	private LogFileMngr lfm = LogFileMngr.getInstance();
	
	static final long ONE_MINUTE_IN_MILLIS=60000;
	
//	private TreeMap<ClienteModel, Event> mapEventosDiaTodo = new TreeMap<ClienteModel, Event>();
	
	public AvisoAgendaThread( ) {
		lfm.info("criando T " +  threadName );
	}
	
	public void start() {
		lfm.info("iniciando " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}

	@Override
	public void run() {
		lfm.info("executando " + threadName);
		
		Date dUltDtCheck = new Date();
		Date dPeriodcheck = new Date(dUltDtCheck.getTime() - ONE_MINUTE_IN_MILLIS);
		while(true) {
			try {
				lfm.info("processando... " + threadName);
				atualizaEventosAtuais();
	
				for (Event event : eventosDaAgenda) {
//					event.getStartDate().isDateOnly();
					
					Calendar cEvent = event.getStartDate().toCalendar();
					Date dEvent = cEvent.getTime();
	
					if (dEvent.after(dPeriodcheck) && // entre um min. atr�s
							dEvent.before(dUltDtCheck)) { // e 'agora'
						mostrarEventosAgenda.add(event);
					} 
	
				}
				mostrarEventosAtuais();
				
				dPeriodcheck = dUltDtCheck;
				
				Thread.sleep(60000);
				
				dUltDtCheck = new Date();
				
			} catch (InterruptedException e) {
				lfm.info("Thread " +  threadName + " interrompido.");
			} catch (Exception ex) {
				lfm.error("Erro na thread " + threadName, ex);
				t.interrupt();
			}
			
		}
		//lfm.info("Thread " +  threadName + " finalizando.");
	}
	
	private synchronized void mostrarEventosAtuais( ) {
		for(Event e : mostrarEventosAgenda) {
			
			String telefoneCliente = "N/D";
			String nomeCliente = "SEM CLIENTE DEFINIDO";
			Integer idCliente;
			if(e.getNewCallData()!=null && e.getNewCallData().getValue().trim().equals("")) {
				String clienteEventoCod = e.getNewCallData().getValue();
				idCliente = Integer.parseInt(clienteEventoCod);
				ClienteModel cliente = (ClienteModel) PrincipalDAO.getInstance().getObjectByID(ClienteModel.class, "id", idCliente);
				if(cliente!=null)
					nomeCliente = cliente.getNomefantasia() != null ? cliente.getNomefantasia() : cliente.getRazaosocial() != null ? cliente.getRazaosocial() : nomeCliente;
					telefoneCliente = cliente.getDdd1() != null && !cliente.getDdd1().equals("") ? "(" + cliente.getDdd1() + ") " : "";
					telefoneCliente += cliente.getTelefone1();
					if(nomeCliente.length()>20) nomeCliente = nomeCliente.substring(0,20) + "...";
			}
			
			AlertWindow alertWindow = new AlertWindow();
	        alertWindow.setBackground(new Color(240,240,240));
	        alertWindow.anchorWindow(MDIFrame.getStatusBar(),alertWindow.TOP);
//	        alertWindow.setShowCloseButton(true);
//	        alertWindow.setShowReduceToIconButton(true);
	        alertWindow.setMainText("Existe um evento \"" + e.getSummary().getValue() + "\" marcado �s " + formatarDataHora(e.getStartDate().toCalendar().getTime()) + ".\n"
	        		+ "Telefone: " + telefoneCliente + ".\n"
	        		+ "Descri��o: " + e.getDescription().getValue());
	        alertWindow.setTitle("ATEN��O - REALIZAR LIGA��O [" + nomeCliente + "]");
//	        alertWindow.setImageName("mail.gif");
	        alertWindow.setWindowMaximumSize(new Dimension(350,120));
	        alertWindow.setFadeInTime(1000);
//	        a.setTimeout(2000);
	        alertWindow.setReduceToIconOnTimeout(true);
	        alertWindow.showWindow();
		}
	}
	
	private synchronized void atualizaEventosAtuais( ) {
		eventosDaAgenda = new ArrayList<Event>(repoDadosAgenda.getAllEntries());
		mostrarEventosAgenda = new ArrayList<Event>();
	}
	
	private String formatarDataHora( Date d ) {
		SimpleDateFormat sdf = new SimpleDateFormat("hh' horas e 'mm' minutos'");
		String formatado = sdf.format(d);
		return formatado;
	}

}
