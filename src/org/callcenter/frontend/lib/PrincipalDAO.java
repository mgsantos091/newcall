package org.callcenter.frontend.lib;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

// Método de Bill Pugh para o Singleton pattern
public class PrincipalDAO {

	private SessionFactory sessionFactory;
	private Configuration configuration;

	// Construtor com o modificador de acesso private não permite a
	// possibilidade de instanciação por outra classe
	private PrincipalDAO() {
		/*File dblogfile = new File(settings.getDefaultDatabasePath()+"playiv.log");*/
		configuration = new Configuration().configure();
		/*if(dblogfile.exists()) {
			configuration.setProperty("hibernate.hbm2ddl.auto", "validate");
		}
		else {
			configuration.setProperty("hibernate.hbm2ddl.auto", "create");
			configuration.setProperty("hibernate.archive.autodetecion","class,hbm");
		}*/
		this.sessionFactory = configuration.buildSessionFactory();
				
	}
	
//	public Connection getJdbcConnection( ) {
//		
//		/*String driver_class = configuration.getProperty("hibernate.connection.driver_class");*/
//		String conn_url = configuration.getProperty("hibernate.connection.url");
//		String user = configuration.getProperty("hibernate.connection.username");
//		String pwd = configuration.getProperty("hibernate.connection.password");
//		
//		Connection jdbc = null;
//		try {
//			jdbc = DriverManager.getConnection(conn_url, user, pwd);
//		} catch (SQLException e) {
//			LogFileMngr.getInstance().error(e.getMessage(),e);
//			e.printStackTrace();
//		}
//		
//		return jdbc;
//		
//	}
	
	public synchronized Object getObjectByID(Class _class, String campo, Object id) {
		Session s = getInstance().getSession();
		Criteria c = s.createCriteria(_class);
		c.add(Restrictions.eq(campo, id));
		Object o = c.uniqueResult();
		s.close();
		return o;
	}

	// PIVDaoHolder é carregado na primeira execução de PIVDao.getInstance()
	// ou no primeiro acesso ao PIVDaoHolder.PDAO, não antes.
	private static class PIVDaoHolder {
		public static final PrincipalDAO pdao = new PrincipalDAO();
	}

	public static PrincipalDAO getInstance() {
//		if(!isExpirado( )) {
			return PIVDaoHolder.pdao;
//		} else return null;
	}

	public synchronized Session getSession() {
		return this.sessionFactory.openSession();
	}

	public synchronized void delete( Object obj ) {
		Session session = getSession();
		session.beginTransaction();
		session.delete( obj );
		session.flush();
		session.getTransaction().commit();
		session.close();
	}
	
	public synchronized void save( Object obj ) {
		Session session = getSession();
		session.beginTransaction();
		session.save( obj );
		session.flush();
		session.getTransaction().commit();
		session.close();
	}
	
	public synchronized void update( Object obj ) {
		Session session = getSession();
		session.beginTransaction();
		session.update( obj );
		session.flush();
		session.getTransaction().commit();
		session.close();
	}
	
	public synchronized SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	public synchronized void closeConnection( ) {
		this.sessionFactory.close();
	}

}