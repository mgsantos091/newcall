package org.callcenter.frontend.lib;

import org.openswing.swing.table.columns.client.ComboColumn;

public class GridColumnFactory {
	
	public static ComboColumn getYesNoColumn(String attributeName) {
		ComboColumn c = new ComboColumn();
		c.setDomainId("SIM_OU_NAO");
		c.setColumnName(attributeName);
		c.setColumnFilterable(true);
		c.setColumnSortable(true);
		c.setPreferredWidth(130);
		return c;
	}
	
}