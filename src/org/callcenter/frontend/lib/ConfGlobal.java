package org.callcenter.frontend.lib;


public class ConfGlobal {
	
	private static ConfGlobal instance;
	
	private final String DEFAULT_FRONTEND_PATH;
	private final String DEFAULT_ARQ_PATH;
	private final String DEFAULT_LOG_PATH;
	private final String DEFAULT_CALENDARIO_PATH;

	private ConfGlobal( ) {
		DEFAULT_FRONTEND_PATH = System.getProperty("user.dir") + java.io.File.separator;
		DEFAULT_ARQ_PATH = DEFAULT_FRONTEND_PATH + "arquivos" + java.io.File.separator;
		DEFAULT_LOG_PATH = DEFAULT_FRONTEND_PATH + "log" + java.io.File.separator;
		DEFAULT_CALENDARIO_PATH = DEFAULT_FRONTEND_PATH + "calendario" + java.io.File.separator;
//		if(isWindows()) {
//			DEFAULT_FRONTEND_PATH = System.getProperty("user.dir") + java.io.File.separator;
//			DEFAULT_ARQ_PATH = DEFAULT_FRONTEND_PATH + "arquivos\\";
//			DEFAULT_LOG_PATH = DEFAULT_FRONTEND_PATH + "log\\";
//			DEFAULT_CALENDARIO_PATH = DEFAULT_FRONTEND_PATH + "calendario\\";
//		} else {
//			DEFAULT_FRONTEND_PATH = System.getProperty("user.dir") + "//";
//			DEFAULT_ARQ_PATH = DEFAULT_FRONTEND_PATH + "arquivos//";
//			DEFAULT_LOG_PATH = DEFAULT_FRONTEND_PATH + "log//";
//			DEFAULT_CALENDARIO_PATH = DEFAULT_FRONTEND_PATH + "calendario//";
//		}
	}
	
	public static ConfGlobal getInstance( ) {
		if( instance == null ) instance = new ConfGlobal();
		return instance;
	}
	
	public String getDefaultArqsFile( ) {
		return this.DEFAULT_ARQ_PATH;
	}
	
	public String getDefaultPlayIVPath( ) {
		return this.DEFAULT_FRONTEND_PATH;
	}
	
	public String getDEFAULT_LOG_PATH() {
		return DEFAULT_LOG_PATH;
	}
	
	// Trecho de c�digo do site Mkyong.com
	// How to detect OS in Java � System.getProperty(�os.name�)
	// http://www.mkyong.com/java/how-to-detect-os-in-java-systemgetpropertyosname/

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}
 
	public static boolean isMac() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("mac") >= 0);
	}
 
	public static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
	}
 
	public static boolean isSolaris() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("sunos") >= 0);
	}
	
	public String getDEFAULT_CALENDARIO_PATH() {
		return DEFAULT_CALENDARIO_PATH;
	}

}