package org.callcenter.frontend.lib;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.openswing.swing.mdi.client.MDIFrame;

public class LogFileMngr {
	
	private ConfGlobal globalSettings = ConfGlobal.getInstance();
	
	private Logger logger = Logger.getLogger(LogFileMngr.class);
	
	private final String LOG_FILE = "callnew-error.log";
	private String ip;
	
	private static LogFileMngr logsettings;
	
	private LogFileMngr( ) {
		
		ip = "DESCONHECIDO";
		try {
			ip = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		BasicConfigurator.configure();
		logger.setLevel(Level.ALL);
		
		Appender fileAppender = null;
		try {
			File dir = new File(globalSettings.getDEFAULT_LOG_PATH());
			if(!dir.exists()) {
				if(!dir.mkdirs()) {
					JOptionPane.showMessageDialog(null, "O caminho do log da aplica��o: " + dir.getAbsolutePath() + " n�o pode ser criado.");
				}
			}
			File f = new File(globalSettings.getDEFAULT_LOG_PATH() + LOG_FILE);
			f.createNewFile();
			fileAppender = new FileAppender(new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN), f.getAbsolutePath());
			logger.addAppender(fileAppender);
			
			// configure output e err
//			System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(globalSettings.getDEFAULT_LOG_PATH()+"SystemOut.log")))));
//			System.setErr(new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(globalSettings.getDEFAULT_LOG_PATH()+"SystemErr.log")))));
			
			System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
			System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), "N�o foi poss�vel criar o arquivo " + LOG_FILE + " em: '" + globalSettings.getDEFAULT_LOG_PATH() + "'.\n" +
					" N�o vai ser poss�vel realizar o log dos erros no sistema.\n" +
					"Entre em contato com o administrador.\n" + 
					e.getMessage());
			e.printStackTrace();
		}

	}
	
	public static LogFileMngr getInstance( ) {
		if( logsettings == null ) logsettings = new LogFileMngr();
		return logsettings;
	}
	
	public void iniciar( ) {
		Format formatter = new SimpleDateFormat("dd/mm/aaaa �s hh:mm:ss");
		logger.info("DEBUG NEWCALL - " + ip + " - " + formatter.format(new Date()) + " - INICIALIZANDO APLICA��O...");
	}
	
	public void encerrar( ) {
		Format formatter = new SimpleDateFormat("dd/mm/aaaa �s hh:mm:ss");
		logger.info("DEBUG NEWCALL - " + ip + " - " + formatter.format(new Date()) + " - ENCERRANDO APLICA��O...");
	}
	
	public void error(Object message , Throwable t) {
		System.err.println("[ERROR] " + message);
		if(this.logger!=null) {
			this.logger.error(message, t);
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");
	}
	
	public void error( Object message ) {
		System.err.println("[ERROR] " + message);
		if(this.logger!=null) {
			this.logger.error(message);
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");
	}
	
	public void info( Object message ) {
		System.out.println("[INFO] " + message);
		if(this.logger!=null) {
			this.logger.info(message);
		}/* else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");*/
	}
	
	public void debug( Object message ) {
		System.out.println("[DEBUG] " + message);
		if(this.logger!=null) {
			this.logger.debug(message);
		}
	}

}