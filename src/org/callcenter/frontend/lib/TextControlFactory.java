package org.callcenter.frontend.lib;

import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.table.columns.client.FormattedTextColumn;

public class TextControlFactory {

	private static MaskFormatter criaFormatter( String mask ) throws ParseException {
		MaskFormatter formatter;
		formatter = new MaskFormatter(mask);
		formatter.setAllowsInvalid(false);
		formatter.setValueContainsLiteralCharacters(false);
		formatter.setPlaceholderCharacter('_');
		formatter.setCommitsOnValidEdit(true);
		return formatter;
	}

	private static MaskFormatter criaFormatter( MaskResources.mask maskenum ) throws ParseException {
		return criaFormatter(MaskResources.getMask(maskenum));
	}

	private static FormattedTextControl criaFormattedTextControl( String mask ) throws ParseException {
		MaskFormatter formatter = criaFormatter(mask);

		FormattedTextControl formattedTextControl = new FormattedTextControl();
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(
				formatter));

		return formattedTextControl;
	}

	public static JFormattedTextField criaCpfCnpjTextControl( MaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(maskenum);
		JFormattedTextField formattedTextControl = new JFormattedTextField();
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(
				formatter));
		return formattedTextControl;
	}

	public static JFormattedTextField criaFormattedTextField ( MaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(MaskResources.getMask(maskenum));
		JFormattedTextField formattedTextField = new JFormattedTextField(formatter);
		return formattedTextField;
	}
	
	public static FormattedTextControl criaFormattedTextControl( MaskResources.mask maskenum ) throws ParseException {
		return criaFormattedTextControl(MaskResources.getMask(maskenum));
	}

	public static FormattedTextColumn criaFormaattedTextColumn( MaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(maskenum);
		formatter.setValidCharacters("0123456789");
		FormattedTextColumn formattedTextColumn = new FormattedTextColumn();
		formattedTextColumn.setFormatterFactory(new DefaultFormatterFactory(formatter));
//		formattedTextColumn.setFormatter(formatter);
		return formattedTextColumn;
	}

	public static void mudaMascara( FormattedTextControl formattedTextControl , MaskResources.mask maskenum) throws ParseException  {
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(criaFormatter(maskenum)));
	}

}