package org.callcenter.frontend.lib;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.callcenter.frontend.swing.k5n.k5ncal.AgendaCFView;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Calendar;
import org.callcenter.frontend.swing.k5n.k5ncal.data.Repository;
import org.openswing.swing.table.profiles.java.GridProfile;
import org.openswing.swing.util.client.ClientSettings;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Util {

	private static File dataDir;
	private static Repository dataRepository;

	public static Timestamp getCurrentTime() {
		return new Timestamp((new Date()).getTime());
	}

	public static Vector<Calendar> carregarAgendaCalendarios(File dir) {
		Vector<Calendar> ret = new Vector<Calendar>();
		File f = new File(dir, AgendaCFView.CALENDARS_XML_FILE);
		if (!f.exists()) {
			String name = (String) System.getProperty("user.name");
			if (name == null)
				name = "Unnamed Calendar";
			Calendar def = new Calendar(dir, name);
			// this.showMessage ( "A new calendar named was created for you" +
			// ": "
			// + name );
			ret.addElement(def);
		} else {
			try {
				ret = Calendar.readCalendars(f);
				if(ret.size()==0) {
					String name = (String) System.getProperty("user.name");
					if (name == null)
						name = "Unnamed Calendar";
					Calendar def = new Calendar(dir, name);
					// this.showMessage ( "A new calendar named was created for you" +
					// ": "
					// + name );
					ret = new Vector<Calendar>(); 
					ret.addElement(def);
				}
			} catch (Exception e1) {
				LogFileMngr.getInstance().error(e1);
			}
		}
		return ret;
	}

	public static File getDiretorioDados() {
		if (dataDir != null)
			return dataDir;
		File dir = new File(AgendaCFView.DEFAULT_DIR_NAME);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				// fatalError ( "N�o foi poss�vel criar um diret�rio de dados" +
				// ": " + dir );
				// showMessage ( "O diret�rio a seguir foi criado\npara
				// armazenar arquivos de dados"
				// + ":\n\n" + dir );
				LogFileMngr.getInstance().error("N�o foi poss�vel criar um diret�rio de dados" + ": " + dir);
			}
		}
		if (!dir.isDirectory()) {
			// fatalError ( "Diret�rio inv�lido: " + dir );
			LogFileMngr.getInstance().error("Diret�rio inv�lido: " + dir);
		}
		dataDir = dir;
		return dir;
	}

	public static Repository getRepositorioPadrao() {
		if (dataRepository != null)
			return dataRepository;
		dataRepository = new Repository(getDiretorioDados(), Util.carregarAgendaCalendarios(getDiretorioDados()),
				false);
		return dataRepository;
	}

	public static GridProfile getGridProfile(String filtroGridFunctionID) {
		GridProfile profile = null;
		Object id = ClientSettings.getInstance().getLastUserGridProfileIds().get(filtroGridFunctionID);
		try {
			if (id == null) {
				id = ClientSettings.getInstance().GRID_PROFILE_MANAGER.getLastGridProfileId(filtroGridFunctionID);
				if (id != null)
					ClientSettings.getInstance().getLastUserGridProfileIds().put(filtroGridFunctionID, id);
			}
		} catch (Throwable ex1) {
		}

		if (id != null) {
			// retrieve a previously stored profile...
			try {
				profile = (GridProfile) ClientSettings.getInstance().getUserGridProfiles(filtroGridFunctionID).get(id);
				if (profile == null) {
					profile = ClientSettings.getInstance().GRID_PROFILE_MANAGER.getUserProfile(filtroGridFunctionID,
							id);
					if (profile != null) {
						ClientSettings.getInstance().getUserGridProfiles(filtroGridFunctionID).put(id, profile);
					}
				}
			} catch (Throwable ex1) {
			}
		}

		return profile;
	}

	public static Integer getNumberOfLines(File f) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(f));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

	public static String getHostName() throws UnknownHostException {
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();
		return hostname;
	}
	
	public static String getSerialNumber() {
		
		String sn = null;

		OutputStream os = null;
		InputStream is = null;

		Runtime runtime = Runtime.getRuntime();
		Process process = null;
		try {
			process = runtime.exec(new String[] { "wmic", "bios", "get", "serialnumber" });
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		os = process.getOutputStream();
		is = process.getInputStream();

		try {
			os.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Scanner sc = new Scanner(is);
		try {
			while (sc.hasNext()) {
				String next = sc.next();
				if ("SerialNumber".equals(next)) {
					sn = sc.next().trim();
					break;
				}
			}
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		if (sn == null) {
			throw new RuntimeException("N�o foi poss�vel localizar SN");
		}

		return sn;
		
	}
	
	public static String getOSName( ) {
		return System.getProperty("os.name");
	}
	
	public static String getOsArch( ) {
		return System.getProperty("os.arch");
	}

}