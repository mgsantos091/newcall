package org.callcenter.frontend.lib;

import org.callcenter.frontend.model.DadosSessaoUsuario;
import org.callcenter.frontend.model.UsuarioModel;

//M�todo de Bill Pugh para Singleton Pattern
public class SessaoUsuario {

	private UsuarioModel usuarioSessao;
	private DadosSessaoUsuario dadosSessaoUsuario;

	// Construtor com o modificador de acesso private n�o permite a
	// possibilidade de instancia��o por outra classe
	private SessaoUsuario() {
//		PrincipalDAO pdao = PrincipalDAO.getInstance();
//		Session session = pdao.getSession();
		
		// Busca usu�rio
//		String baseSQL = "from org.callcenter.frontend.model.UsuarioModel as Usuario order by Usuario.id asc";
//		this.usuarioSessao = (UsuarioModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
		

		
//		session.close();
		
	}

	// PIVSessaoUsuarioHolder � carregada na primeira execu��o de
	// PIVSessaoUsuario.getInstance()
	// ou no primeiro acesso ao PIVSessaoUsuarioHolder.psessao, n�o antes
	private static class PIVSessaoUsuarioHolder {
		public static final SessaoUsuario psessao = new SessaoUsuario();
	}

	public static SessaoUsuario getInstance() {
		return PIVSessaoUsuarioHolder.psessao;
	}

	public UsuarioModel getUsuarioSessao() {
		return this.usuarioSessao;
	}
	
	public void registraUsuarioSessao(UsuarioModel usuario) {
		if(this.usuarioSessao==null) {
			this.usuarioSessao = usuario;
			DadosSessaoUsuario dadosSessaoUsuario = (DadosSessaoUsuario) PrincipalDAO.getInstance().getObjectByID(DadosSessaoUsuario.class, "usuario.id", usuario.getId());
			if(dadosSessaoUsuario!=null) this.dadosSessaoUsuario = dadosSessaoUsuario;
			else {
				dadosSessaoUsuario = new DadosSessaoUsuario();
				dadosSessaoUsuario.setUsuario(usuario);
				PrincipalDAO.getInstance().save(dadosSessaoUsuario);
				this.dadosSessaoUsuario = dadosSessaoUsuario;
			}
		}
	}

	public DadosSessaoUsuario getDadosSessaoUsuario() {
		return dadosSessaoUsuario;
	}
	
}