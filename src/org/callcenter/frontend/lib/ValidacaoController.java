package org.callcenter.frontend.lib;

import javax.swing.JOptionPane;

import org.callcenter.frontend.model.ClienteModel;
import org.callcenter.frontend.model.RegistroLigacaoModel;
import org.openswing.swing.mdi.client.MDIFrame;

public class ValidacaoController {

	public static Boolean validaDadosLigacao(RegistroLigacaoModel registro) {
		Integer status_ligacao_nao_atendida = registro.getStatus_ligacao_nao_atendida()==null?0:registro.getStatus_ligacao_nao_atendida();
		Integer status_ligacao_nao_efetivado = registro.getStatus_ligacao_atendida_nao_efetivado()==null?0:registro.getStatus_ligacao_atendida_nao_efetivado();
		Integer status_ligacao_efetivado = registro.getStatus_ligacao_atendida_efetivado()==null?0:registro.getStatus_ligacao_atendida_efetivado();
			
		if((status_ligacao_nao_atendida>0 && status_ligacao_nao_efetivado>0)||
				(status_ligacao_nao_atendida>0 && status_ligacao_efetivado>0)||
				(status_ligacao_nao_efetivado>0 && status_ligacao_efetivado>0))
		{
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), "S� � poss�vel selecionar um status para a liga��o, por favor realize a corre��o.");
			return false;
		} else if(((status_ligacao_nao_atendida>0)||
				(status_ligacao_nao_efetivado>0)||
				(status_ligacao_efetivado>0)) && registro.isTarget_fora()) { 
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Voc� selecionou a op��o Target Fora, por�m, tamb�m selecionou um status para a liga��o.\n"
					+ "S� � poss�vel marcar Target Fora se nenhum status de liga��o for selecionado.");
			return false;
		} else if(status_ligacao_nao_atendida==0&&
				status_ligacao_nao_efetivado==0 && 
				status_ligacao_efetivado==0 && 
				!registro.isTarget_fora()) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Voc� n�o selecionou nenhum status para esta liga��o.\n"
					+ "S� � poss�vel registrar liga��o com algum status.");
			return false;
		}
			return true;
	}
	
	public static Boolean validaDadosCliente(ClienteModel cliente) {
		return true;
	}
	
}
