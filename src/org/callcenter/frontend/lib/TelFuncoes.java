package org.callcenter.frontend.lib;

public class TelFuncoes {
	public static String trataDDD(String ddd) {
		String dddRet = ddd == null || ddd.equals("") ? "" : ddd.trim().length()==2 ? "0" + ddd.trim() : ddd.trim();
		return dddRet;
	}
}
