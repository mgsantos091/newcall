# LEIA-ME #

Este repositório serve para ser um local centralizado do código fonte do NewCall, aonde pessoas do time podem ver o código, realizar modificações, realizar pulls/commits, documentar bugs e workarounds, entre outros.

### INTEGRAÇÃO NO ECLIPSE ###

Como configurar este BitBucket repository no ambiente Eclipse através do eGit?

Você precisará:

1. Link do Projeto que você quer sincronizar. Neste caso: https://bitbucket.org/newcall/projeto-newcall
2. Uma conta com acesso ao projeto.
3. Ter o Eclipse instalado e configurado + eGit. Para configurar o eGit:

* Abra o Eclipse -> Help -> Install New Software.
* Cole a url: http://download.eclipse.org/egit/updates e clique em Add. Nome: eGit.
* Selecione Eclipse Git Team Provider e JGit. Avance, complete a instalação e reinicie o Eclipse.

Instalação:

1. Clique em Open Perspective e selecione a perspectiva Git.
2. Selecione "Clone a Git Repository".
3. Cole o BitBucket url da lista acima. Na parte de autenticação, preencha os dados de seu acesso a este projeto.
4. O download do projeto sera realizado e aparecera no Eclipse.
5. Para trabalhar no Source Code:

* Vá em File -> Import.
* Selecione Git/Projects from Git.
* Selecione Existing local repository.
* Selecione projeto-newcall.
* Selecione existing Eclipse projects
* Finish.


Fonte: http://crunchify.com/how-to-configure-bitbucket-git-repository-in-you-eclipse/

### EXECUTANDO A APLICAÇÃO ###

* Após baixar o repositório localmente, e ter um projeto associado, você deve ser capaz de iniciar a aplicação *as is*. Todas as libs já estão configuradas corretamente no projeto e portanto estas são as unicas dependencias, fora a instalação do Java.
* Você precisa apenas verificar o arquivo **hibernate.cfg.xml**, ele deve estar apontando para um database local (hsql) ou remoto (mysql).
* Para iniciar a aplicação no Eclipse: execute org.callcenter.frontend.start.Launcher
* Para realizar a build do projeto, eu estou selecionado o projeto e gerando um jar executável, criando uma pasta para as dependencias (libs).

### CONTATO ###

* Programador: mtsgomessantos@gmail.com
* Coordenador: m.eljalis@gmail.com